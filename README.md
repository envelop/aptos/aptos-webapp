## Docker Dev Environment with local NGINX
```bash
docker run --rm  -v $PWD:/app node:19 /bin/bash -c 'cd /app && npm install && chmod -R 777 node_modules'
```
```bash
docker run --rm  --env PUBLIC_URL=/wrap -v $PWD:/app node:19 /bin/bash -c 'cd /app && npm run build'
```


```bash
# set app URL 
export PUBLIC_URL=/wrap
```

```bash
docker-compose -f docker-compose-local.yaml up
```
http://localhost:8080/wrap

### Node build info

```
> node --version
v19.4.0

> npm -V
9.2.0
```

```
> npm install
> npm run build
```

For start or build with node version >16 use environment parameter: `NODE_OPTIONS=--openssl-legacy-provider`.

To turn off strict warning check use environment parameter `CI=false`.

In prodution use web-server (nginx, etc) to distribute static files from /build folder.