import React, { ReactNode, useContext, useEffect, useState } from 'react';
import { ERC20Type, chainTypeToERC20, getERC20Params } from '../../core-src';
import { Web3Context } from '../Web3Dispatcher';

import config from '../../app.config.json';

export type ERC20ContextType = {
	erc20List: Array<ERC20Type>;
	requestERC20Token: (
		contractAddress: string,
		userAddress?: string,
		allowanceTo?: string,
	) => Promise<ERC20Type | undefined>;
};

export const ERC20Context = React.createContext<ERC20ContextType>({
	erc20List: [],
	requestERC20Token: async () => undefined,
});

export function ERC20Dispatcher(props: { children: ReactNode }) {
	const [erc20List, setErc20List] = useState<Array<ERC20Type>>([]);

	const { currentChain } = useContext(Web3Context);

	useEffect(() => {
		if (!currentChain) {
			return;
		}

		const foundChain: any = config.CHAIN_SPECIFIC_DATA.find((item) => {
			return item.chainId === currentChain.chainId;
		});
		if (!foundChain || !foundChain.supportedERC20Tokens) {
			setErc20List([]);
			return;
		}

		Promise.all(
			foundChain.supportedERC20Tokens.map(async (item: string) => {
				return await getERC20Params(currentChain.chainId, item);
			}),
		).then((data) => {
			setErc20List(data);
		});
	}, [currentChain]);

	const requestERC20Token = async (
		contractAddress: string,
		userAddress?: string,
		allowanceTo?: string,
	) => {
		if (!currentChain) {
			return;
		}

		if (contractAddress === '0x0000000000000000000000000000000000000000') {
			return chainTypeToERC20(currentChain);
		}

		if (!userAddress) {
			const foundToken = erc20List.find((item) => {
				return (
					contractAddress.toLowerCase() === item.contractAddress.toLowerCase()
				);
			});
			if (foundToken) {
				return foundToken;
			}
		}

		const data = await getERC20Params(
			currentChain.chainId,
			contractAddress,
			userAddress,
			allowanceTo,
		);

		setErc20List((prevState) => {
			return [
				...prevState.filter((item) => {
					return (
						contractAddress.toLowerCase() !== item.contractAddress.toLowerCase()
					);
				}),
				data,
			];
		});
	};

	return (
		<ERC20Context.Provider
			value={{
				erc20List,
				requestERC20Token,
			}}
		>
			{props.children}
		</ERC20Context.Provider>
	);
}
