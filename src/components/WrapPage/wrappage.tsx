import React, { useContext, useEffect, useState } from 'react';
import {
	matchRoutes,
	useLocation,
	useMatch,
	useMatches,
	useSearchParams,
} from 'react-router-dom';
import {
	BigNumber,
	ChainType,
	CollateralItem,
	ERC20Type,
	Fee,
	Lock,
	LockType,
	Royalty,
	Rules,
	Web3,
	_AssetType,
	addThousandSeparator,
	checkApprovalERC1155,
	checkApprovalERC721,
	checkApprovalForAllERC721,
	combineURLs,
	compactString,
	decodeRules,
	getChainId,
	getContractNameERC1155,
	getContractNameERC721,
	getERC20BalanceFromChain,
	getERC721ByIdFromChain,
	getNFTById,
	getNullERC20,
	getWNFTById,
	getWrapperTechToken,
	localStorageGet,
	makeERC20Allowance,
	makeERC20AllowanceMultisig,
	mintERC721,
	removeThousandSeparator,
	setApprovalERC1155,
	setApprovalERC1155Multisig,
	setApprovalERC721,
	setApprovalERC721Multisig,
	setApprovalForAllERC721,
	tokenToFloat,
	tokenToInt,
	waitUntilAsync,
	
} from '../../core-src';

import TippyWrapper from '../TippyWrapper';
import NumberInput from '../NumberInput';
import CoinSelector from '../CoinSelector';
import CollateralViewer, { CollateralError } from '../CollateralViewer';

import { getDAFromChain, isAptosAddress, mintDA } from '../../aptos';

import {
	AdvancedLoaderStageType,
	ERC20Context,
	InfoModalContext,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes,
} from '../../dispatchers';

import {
	getBlacklist,
	getMaxCollaterals,
	getWhitelist,
	getWrapperWhitelist,
	isEnabledForCollateral,
	isEnabledForFee,
	isEnabledRemoveFromCollateral,
} from '../../models';
import { useWallet, WalletName } from '@aptos-labs/wallet-adapter-react';

import default_token_preview from '../../core-pics/_default_nft.svg';
import icon_i_del from '../../static/pics/i-del.svg';
import default_icon from '../../static/pics/coins/_default.svg';
import icon_external from '../../static/pics/icons/i-external.svg';
import attention_sm from '../../static/pics/mascot/attention-sm.png';

import config from '../../app.config.json';
import {
	addDays,
	dateToStrWithMonth,
	dateToUnixtime,
	dateToUnixtimeNumber
} from '../../models/utils';
import { aptosWrapToken, aptosWrapTokenWithMint, wrapTokenMultisig } from '../../models/wrap';
import { getNativeCollateralNumber } from '../../core-src/_types';
import { Address } from '@web3-onboard/core/dist/types';
import { log } from 'console';

export type RoyaltyInput = {
	address: string | undefined;
	percent: string; // fraction: 100.00
	timeAdded?: number; // needs for ui purposes
};

type OriginalToken = {
	tokenName?: string;
	collectionImg?: string;
	badContract?: boolean;
	originalTokenStandart?: _AssetType;
	isApproved?: boolean;
	owner?: string;
	balance1155?: BigNumber;
};

export default function WrapPage() {
	const focusableTime = React.useRef<HTMLInputElement>(null);
	const focusableFees = React.useRef<HTMLInputElement>(null);
	let scrollable: any = {};
	const niftsyTokenIcon = 'https://envelop.is/assets/img/niftsy.svg';

	let inputsOriginalTokenTimeout: ReturnType<typeof setTimeout> = setTimeout(
		() => {},
	);

	const {
		userAddress,
		currentChain,
		currentChainId,
		balanceNative,
		web3,
		getWeb3Force,
		silentConnect
	} = useContext(Web3Context);
	const { erc20List, requestERC20Token } = useContext(ERC20Context);
	const {
		setModal,
		setError,
		setInfo,
		unsetModal,
		setLoading,
		createAdvancedLoader,
		updateStepAdvancedLoader,
	} = useContext(InfoModalContext);

	const {
		connect,
		account,
		network,
		connected,
		//		disconnect,
		wallet,
		wallets,
		signAndSubmitTransaction,
//		signAndSubmitBCSTransaction,
		signTransaction,
		signMessage,
		signMessageAndVerify,
	} = useWallet();
		

	const [pageMode, setPageMode] = useState<'simple' | 'advanced'>('advanced');
	/*	const [wrapWith, setWrapWith] = useState<'own' | 'predefined' | 'empty'>(
		'empty',
	);
*/
	const [wrapWith, setWrapWith] = useState<'own' | 'predefined'>('predefined');

	const [templates, setTemplates] = useState<Array<string>>([
		'templ 1',
		'templ 2',
		'templ 3',
		'templ 4',
	]);
	const [selectedTemplate, setSelectedTemplate] = useState(templates[0]);

	const [wrapperContract, setWrapperContract] = useState<string>('');
	const [wrapperCollection, setWrapperCollection] = useState<string>('');
	const [minterContract, setMinterContract] = useState<string>('');
	const [storageContracts, setStorageContracts] = useState<Array<string>>([]);
	const [whitelistContract, setWhitelistContract] = useState<
		string | undefined
	>('');
	const [techToken, setTechToken] = useState('');

	const [collateralPopupShown, setCollateralPopupShown] =
		useState<boolean>(false);
	const [collateralWhitelist, setCollateralWhitelist] = useState<Array<string>>(
		[],
	);
	const [originalTokensBlacklist, setOriginalTokensBlacklist] = useState<
		Array<string>
	>([]);
	const [enabledForFee, setEnabledForFee] = useState<Array<string>>([]);
	const [enabledForCollateral, setEnabledForCollateral] = useState<
		Array<string>
	>([]);
	const [enabledRemoveFromCollateral, setEnabledRemoveFromCollateral] =
		useState<Array<string>>([]);

	const [inputOriginalTokenAddress, setInputOriginalTokenAddress] =
		useState('');
	const [inputOriginalTokenId, setInputOriginalTokenId] = useState('');
	const [inputOriginalCopies, setInputOriginalCopies] = useState('');
	const [inputApproveAll, setInputApproveAll] = useState(false);

	const [requestOriginalTokenAddress, setRequestOriginalTokenAddress] =
		useState<string | undefined>(undefined);
	const [requestOriginalTokenId, setRequestOriginalTokenId] = useState<
		string | undefined
	>(undefined);

	const [originalTokenCache, setOriginalTokenCache] = useState<
		Array<{
			contractAddress: string;
			tokenId: string;
			savedToken: OriginalToken;
		}>
	>([]);
	const [originalContractNamesCache, setOriginalContractNamesCache] = useState<
		Array<{ contractAddress: string; name: string }>
	>([]);

	const [showOutAssetTypeBlock, setShowOutAssetTypeBlock] =
		useState<boolean>(true);
	const [inputOutAssetType, setInputOutAssetType] = useState<_AssetType>(
		_AssetType.ERC721,
	);
	const [inputCopies, setInputCopies] = useState(2);

	const [showOutAdvancedOptionsBlock, setShowOutAdvancedOptionsBlock] =
		useState<boolean>(true);
	const [advancedOptionsOpened, setAdvancedOptionsOpened] = useState(false);
	const [inputCollateralRecipientAddress, setInputCollateralRecipientAddress] =
		useState('');
	const [inputWNFTRecipientAddress, setInputWNFTRecipientAddress] =
		useState('');
	const [inputRules, setInputRules] = useState<Rules>(decodeRules('0000'));

	const [showCollateralBlock, setShowCollateralBlock] = useState<boolean>(true);
	const [collaterals, setCollaterals] = useState<Array<CollateralItem>>([]);
	const [inputCollateralAmount, setInputCollateralAmount] = useState('');
	const [inputCollateralAssetType, setInputCollateralAssetType] =
		useState<_AssetType>(_AssetType.ERC721);
	const [inputCollateralAddress, setInputCollateralAddress] = useState('');
	const [inputCollateralTokenId, setInputCollateralTokenId] = useState('');
	const [collateralErrors, setCollateralErrors] = useState<
		Array<CollateralError>
	>([]);
	const [inputCollateralSlots, setInputCollateralSlots] = useState<
		number | undefined
	>(undefined);
	//web3	const [maxCollaterals, setMaxCollaterals] = useState<number>(25);
	const [maxCollaterals, setMaxCollaterals] = useState<number>(1);

	const [showTransferFeeBlock, setShowTransferFeeBlock] =
		useState<boolean>(true);
	const [inputTransferFeeAddress, setInputTransferFeeAddress] = useState('');
	const [inputTransferFeeAmount, setInputTransferFeeAmount] = useState('');

	const [showRoyaltyBlock, setShowRoyaltyBlock] = useState<boolean>(true);
	const [inputRecipientAddress, setInputRecipientAddress] = useState('');
	const [recipients, setRecipients] = useState<Array<RoyaltyInput>>([]);
	const [inputAddWNFTChecked, setInputAddWNFTChecked] = useState(false);

	const [showLockBlock, setShowLockBlock] = useState<boolean>(true);
	const [inputTimeLockChecked, setInputTimeLockChecked] = useState(false);
	const [inputTimeLockDays, setInputTimeLockDays] = useState('');
	const [inputFeeLockChecked, setInputFeeLockChecked] = useState(false);
	const [inputFeeLockAmount, setInputFeeLockAmount] = useState('');

	const [showErrors, setShowErrors] = useState<Array<string>>([]);

	const location = useLocation();

	useEffect(() => {
		const sourceParams = [
//			{ path: '/:chainId/:contractAddress/:tokenId' },
			{ path: '/:chainId/:tokenId' },
			{ path: '/:chainId' },
		];
		const matches = matchRoutes(sourceParams, location);

		if (
			matches &&
			matches[0] &&
			matches[0].params &&
			matches[0].params.chainId &&
			parseInt(matches[0].params.chainId)
		) {
			if (matches[0].params.tokenId) {
				setPageMode('advanced');
				setWrapWith('own');
//				setInputOriginalTokenAddress(matches[0].params.contractAddress);
//				setRequestOriginalTokenAddress(matches[0].params.contractAddress);
				setInputOriginalTokenId(matches[0].params.tokenId);
				setRequestOriginalTokenId(matches[0].params.tokenId);
				/*				if (matches[0].params.tokenId) {
					setInputOriginalTokenId(matches[0].params.tokenId);
					setRequestOriginalTokenId(matches[0].params.tokenId);
				}
				*/
			}
		} else {
			setInputOriginalTokenAddress('');
			setInputOriginalTokenId('');
		}
	}, [location]);

	useEffect(() => {
		const getWrapperParams = async () => {
			if (!currentChain) {
				return;
			}

			let networkID = network
			? network.chainId
				? parseInt(network.chainId)
				: 1
			: 1;

			const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => {
				return item.chainId === networkID;
			});
			console.log('setWrapperContract currentChain.chainId', networkID);
			
			if (!foundChain) {
				setError('Unsupported chain');
				return;
			}

			const _wrapperContract = foundChain.wrapperContract;
			const _wrapperCollection = foundChain.wrapperCollection;
//			console.log('config.CHAIN_SPECIFIC_DATA',config.CHAIN_SPECIFIC_DATA);
			
//			console.log('currentChain',currentChain);
			
			console.log('foundChain',foundChain);
			
			setWrapperContract(_wrapperContract);
			setWrapperCollection(_wrapperCollection);
//			console.log('wrapperContractAddress', _wrapperContract);
			const _minterContract = foundChain.minterContract;
			if (_minterContract) {
				setMinterContract(_minterContract);
			} else {
				setMinterContract('');
			}
/*			const _techToken = await getWrapperTechToken(
				currentChain.chainId,
				_wrapperContract,
			);
			setTechToken(_techToken);
*/
			/*web3
			const _storageContracts = foundChain.WNFTStorageContracts.map((item) => {
				return item.contractAddress;
			});
			if (_storageContracts && _storageContracts.length) {
				setStorageContracts(_storageContracts);
			} else {
				setStorageContracts([]);
			}
			*/
			const _storageContracts = foundChain.WNFTStorageContracts.map((item) => {
				return item.contractAddress;
			});
			console.log('_storageContracts', _storageContracts);
			
			if (_storageContracts && _storageContracts.length) {
				setStorageContracts(_storageContracts);
			} else {
				setStorageContracts([]);
			}

			/*web3
			const _maxCollaterals = await getMaxCollaterals(
				currentChain.chainId,
				_wrapperContract,
			);
			
			setMaxCollaterals(_maxCollaterals);
			*/

			/*
			const _whitelistContract = await getWrapperWhitelist(
				currentChain.chainId,
				_wrapperContract,
			);
			if (!_whitelistContract) {
				setWhitelistContract(undefined);
				setCollateralWhitelist([]);
				setEnabledForCollateral([]);
				setEnabledForFee([]);
				setEnabledRemoveFromCollateral([]);
				setOriginalTokensBlacklist([]);
				return;
			}

			const _whitelist = (
				await getWhitelist(currentChain.chainId, _whitelistContract)
			).filter((item) => {
				return (
					item.contractAddress.toLowerCase() !==
					'0x376e8EA664c2E770E1C45ED423F62495cB63392D'.toLowerCase()
				);
			});
			const _whitelistParsed = await Promise.all(
				_whitelist.map(async (item) => {
					if (item.assetType === _AssetType.ERC20) {
						requestERC20Token(item.contractAddress, userAddress);
					}
					return item.contractAddress;
				}),
			);
			setCollateralWhitelist(_whitelistParsed);
			const _enabledForCollateral = await Promise.all(
				_whitelistParsed.filter(async (item) => {
					return await isEnabledForCollateral(
						currentChain.chainId,
						_whitelistContract,
						item,
					);
				}),
			);
			setEnabledForCollateral(_enabledForCollateral);
			const _enabledForFee = await Promise.all(
				_whitelistParsed.filter(async (item) => {
					return await isEnabledForFee(
						currentChain.chainId,
						_whitelistContract,
						item,
					);
				}),
			);
			setEnabledForFee(_enabledForFee);
			const _enabledRemoveFromCollateral = await Promise.all(
				_whitelistParsed.filter(async (item) => {
					return await isEnabledRemoveFromCollateral(
						currentChain.chainId,
						_whitelistContract,
						item,
					);
				}),
			);
			setEnabledRemoveFromCollateral(_enabledRemoveFromCollateral);
*/
			const _blacklist = await getBlacklist(
				currentChain.chainId,
				foundChain.wrapperContract,
			);
			console.log('_blacklist', _blacklist);
			if ( _blacklist[0]) {
				const _blacklistParsed = await Promise.all(
					_blacklist.map(async (item) => {
						/*if (item.assetType === _AssetType.ERC20) {
							requestERC20Token(item.contractAddress, userAddress);
						}*/
						return item.contractAddress;
					}),
				);
				setOriginalTokensBlacklist(_blacklistParsed);
			}
		};

		getWrapperParams();
	}, [network, userAddress]);

	useEffect(() => {
		const fetchOriginalToken = async () => {
			const address = requestOriginalTokenAddress;
			const tokenId = requestOriginalTokenId;
			console.log('pre-currentChainId');

			if (currentChainId === 0) {
				return;
			}
			console.log('wrapperContract', wrapperContract);
			if (wrapperContract === '') {
				return;
			}
			console.log('address',address);
/*
			if (!address || !isAptosAddress(address)) {
				return;
			}
*/

/*
			if (!foundName) {
				let originalProjectName = undefined;
				try {
					originalProjectName = await getContractNameERC1155(
						currentChainId,
						address,
					);
				} catch (ignored) {}
				if (!originalProjectName) {
					try {
						originalProjectName = await getContractNameERC721(
							currentChainId,
							address,
						);
					} catch (ignored) {}
				}
				if (originalProjectName) {
					setOriginalContractNamesCache([
						...originalContractNamesCache.filter((item) => {
							return (
								address.toLowerCase() !== item.contractAddress.toLowerCase()
							);
						}),
						{ contractAddress: address, name: originalProjectName },
					]);
				}
			}
*/
			if (!tokenId) {
				setInputOriginalTokenAddress('');
				return;
			}

			console.log('pre-foundSaved !isAptosAddress(tokenId)', !isAptosAddress(tokenId));
			if (!isAptosAddress(tokenId)) {
				setInputOriginalTokenAddress('');
				return;
			}
			console.log('pre-foundSaved');
			
			const foundSaved = originalTokenCache.find((item) => {
				return (
//					item.contractAddress.toLowerCase() === address.toLowerCase()
					// &&
					item.tokenId.toLowerCase() === tokenId.toLowerCase()
				);
			});
/*			console.log('foundSaved', foundSaved);
			if (foundSaved) {
				return;
			}
*/
			let originalTokenStandart = undefined;
			let originalTokenImage = undefined;
			let originalTokenName = undefined;
			let originalIsApproved = false;
			let originalOwner = undefined;
			let originalBalance = undefined;

			let token;

			try {
				let networkID = network
					? network.chainId
						? parseInt(network.chainId)
						: 1
					: 1;
				//				console.log('network',network);

				token = await getDAFromChain(tokenId, networkID, account?.address || 'wrong owner');
				//web3 token = await getNFTById(currentChainId, address, tokenId, userAddress);
//			} catch (ignored) {}
			} catch (err) {			console.log('getDAFromChain error',err);
							}
			console.log('getDAFromChain token',token);
			
			if (!token) {
				setOriginalTokenCache([
					...originalTokenCache.filter((item) => {
						return (
//							item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId.toLowerCase() !== tokenId.toLowerCase()
						);
					}),
					{
//						contractAddress: address,
						contractAddress: '',
						tokenId: tokenId,
						savedToken: {
							badContract: true,
						},
					},
				]);
				setInputOriginalTokenAddress('');
				return;
			} 


			originalTokenStandart = token.token.assetType;
			originalTokenImage = token.token.image;
			originalTokenName = token.token.name;
			let originalContractAddress = token.contractAddress;

			console.log('pre-foundName');
			
			const foundName = originalContractNamesCache.find((item) => {
				return (
					item.contractAddress.toLowerCase() ===
					originalContractAddress.toLowerCase()
				);
			});
			console.log('foundName',foundName);
			setInputOriginalTokenAddress(originalContractAddress);

			if (!foundName) {
				let originalProjectName = token.contractName;
				
				if (originalProjectName) {
					console.log('setOriginalContractNamesCache',originalProjectName);
//					setInputOriginalTokenAddress(originalContractAddress);
					setOriginalContractNamesCache([
						...originalContractNamesCache.filter((item) => {
							return (
								originalContractAddress.toLowerCase() !== item.contractAddress.toLowerCase()
							);
						}),
						{ contractAddress: originalContractAddress, name: originalProjectName },
					]);
				}
			}

/*
			try {
				if (userAddress) {
					if (originalTokenStandart === _AssetType.ERC1155) {
						originalIsApproved = await checkApprovalERC1155(
							currentChainId,
							address,
							userAddress,
							wrapperContract,
						);
					} else {
						originalIsApproved = await checkApprovalERC721(
							currentChainId,
							address,
							tokenId,
							userAddress,
							wrapperContract,
						);
					}
				}
			} catch (e: any) {
				console.log('Cannot check approval', token, userAddress, e);
			}
*/
			originalOwner = token.token.owner;
			originalBalance = token.token.amount;

			if (
//TODO				requestOriginalTokenAddress.toLowerCase() ===
//					token.contractAddress.toLowerCase() &&
				requestOriginalTokenId.toLowerCase() === token.token.tokenId.toLowerCase()
			) {
				console.log('token.token.tokenId.toLowerCase',token.token.tokenId.toLowerCase());
				setOriginalTokenCache([
					...originalTokenCache.filter((item) => {
						return (
//							item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId.toLowerCase() !== tokenId.toLowerCase()
						);
					}),
					{
						contractAddress: originalContractAddress,
//						contractAddress: '',
//						contractAddress: address,
						tokenId: tokenId,
						savedToken: {
							tokenName: originalTokenName || '',
							collectionImg: originalTokenImage || '',
							badContract: false,
							originalTokenStandart: originalTokenStandart,
							isApproved: originalIsApproved,
							owner: originalOwner,
							balance1155: originalBalance,
						},
					},
				]);
			}
		};

		fetchOriginalToken();
	}, [
		requestOriginalTokenAddress,
		requestOriginalTokenId,
		currentChainId,
		wrapperContract,
	]);

	const getPredefinedContractTip = () => {
		if (minterContract === '') {
			return (
				<TippyWrapper msg='No minter contract in current chain'></TippyWrapper>
			);
		}
		if (localStorageGet('authMethod').toLowerCase() === 'gnosis') {
			return (
				<TippyWrapper msg='Predefined contracts are not available for multisig wallets yet'></TippyWrapper>
			);
		}

		return (
			<TippyWrapper msg='Create wDA (wNFT) with original DA (NFT) inside using Envelop metadata'></TippyWrapper>
		);
	};
	/*web3
	const getWrapWithBlock = () => {
		if (pageMode === 'simple') {
			return null;
		}

		return (
			<div className='row mb-5 pt-2'>
				<div className='col-auto mb-2'>Wrap with</div>
				<div className='col-auto mb-2'>
					<label className='checkbox'>
						<input
							type='radio'
							name='wrap-with'
							checked={wrapWith === 'empty'}
							onChange={() => {
								setWrapWith('empty');
							}}
						/>
						<span className='check'></span>
						<span className='check-text'>
							empty
							<TippyWrapper msg='Create wNFT without original NFT inside'></TippyWrapper>
						</span>
					</label>
				</div>
				<div className='col-auto mb-2'>
					<label className='checkbox'>
						<input
							type='radio'
							name='wrap-with'
							disabled={minterContract === ''}
							checked={wrapWith === 'predefined'}
							onChange={() => {
								setWrapWith('predefined');
							}}
						/>
						<span className='check'></span>
						<span className='check-text'>
							default NFT
							{getPredefinedContractTip()}
						</span>
					</label>
				</div>
				<div className='col-auto mb-2'>
					<label className='checkbox'>
						<input
							type='radio'
							name='wrap-with'
							checked={wrapWith === 'own'}
							onChange={() => {
								setWrapWith('own');
							}}
						/>
						<span className='check'></span>
						<span className='check-text'>
							user's NFT
							<TippyWrapper msg='Create wNFT with original custom NFT inside'></TippyWrapper>
						</span>
					</label>
				</div>
			</div>
		);
	};
*/
	const getWrapWithBlock = () => {
		if (pageMode === 'simple') {
			return null;
		}

		return (
			<div className='row mb-5 pt-2'>
				<div className='col-auto mb-2'>Wrap with</div>
				<div className='col-auto mb-2'>
					<label className='checkbox'>
						<input
							type='radio'
							name='wrap-with'
							disabled={minterContract === ''}
							checked={wrapWith === 'predefined'}
							onChange={() => {
								setWrapWith('predefined');
							}}
						/>
						<span className='check'></span>
						<span className='check-text'>
							default DA (NFT)
							{getPredefinedContractTip()}
						</span>
					</label>
				</div>
				<div className='col-auto mb-2'>
					<label className='checkbox'>
						<input
							type='radio'
							name='wrap-with'
							checked={wrapWith === 'own'}
							onChange={() => {
								setWrapWith('own');
							}}
						/>
						<span className='check'></span>
						<span className='check-text'>
							user's DA (NFT)
							<TippyWrapper msg='Create wDA (wNFT) with original custom DA (NFT) inside'></TippyWrapper>
						</span>
					</label>
				</div>
			</div>
		);
	};

	const getModeSelector = () => {
		return (
			<div className='col-12 col-md-auto mb-3'>
				<div className='wf-settings__switcher'>
					<div className='label d-none d-sm-block'>Wrap mode</div>
					<div className='label d-sm-none'>Mode</div>
					<div className='switcher'>
						<input
							className='toggle toggle-left'
							type='radio'
							id='simple-on'
							checked={pageMode === 'simple'}
							onChange={() => {
								setPageMode('simple');
								setWrapWith('predefined');
								setInputOriginalTokenAddress('');
								setInputOriginalTokenId('');
								setInputOriginalCopies('');
								setInputApproveAll(false);
								setInputOutAssetType(_AssetType.ERC721);
								setInputCopies(2);
								setInputCollateralRecipientAddress('');
								setInputWNFTRecipientAddress('');
								setInputRules(decodeRules('0000'));
								setInputTransferFeeAddress('');
								setInputTransferFeeAmount('');
								setInputRecipientAddress('');
								setRecipients([]);
								setInputAddWNFTChecked(false);
							}}
						/>
						<label className='switcher__btn' htmlFor='simple-on'>
							Simple
						</label>
						<input
							className='toggle toggle-right'
							id='advanced-on'
							value='true'
							type='radio'
							checked={pageMode === 'advanced'}
							onChange={() => {
								setPageMode('advanced');
							}}
						/>
						<label className='switcher__btn' htmlFor='advanced-on'>
							Advanced
						</label>
					</div>
				</div>
			</div>
		);
	};
	const getTemplatesBlock = () => {
		return null;

		// return (
		// 	<div className="col-12 col-md-5">
		// 		<div className="wf-settings__template">
		// 			<div className="select-custom">
		// 				<InputWithOptions
		// 					value={ selectedTemplate }
		// 					options={ templates.map((item) => { return { label: item, value: item } }) }
		// 					onSelect={(item) => { setSelectedTemplate(item.label) }}
		// 					placeholder="Custom template"
		// 				/>
		// 			</div>
		// 		</div>
		// 	</div>
		// )
	};

	const filterERC20ContractByPermissions = (permissions: {
		enabledForCollateral?: boolean;
		enabledForFee?: boolean;
		enabledRemoveFromCollateral?: boolean;
	}): Array<ERC20Type> => {
		// OR LOGIC
		return erc20List.filter((item) => {
			if (
				permissions.enabledForCollateral &&
				enabledForCollateral.find((iitem) => {
					return item.contractAddress.toLowerCase() === iitem.toLowerCase();
				})
			) {
				return true;
			}
			if (
				permissions.enabledForFee &&
				enabledForFee.find((iitem) => {
					return item.contractAddress.toLowerCase() === iitem.toLowerCase();
				})
			) {
				return true;
			}
			if (
				permissions.enabledRemoveFromCollateral &&
				enabledRemoveFromCollateral.find((iitem) => {
					return item.contractAddress.toLowerCase() === iitem.toLowerCase();
				})
			) {
				return true;
			}

			return false;
		});
	};
	const addressIsStorage = (address: string): boolean => {
		console.log('addressIsStorage storageContracts', storageContracts);
		console.log('addressIsStorage !!storageContracts.find ', !!storageContracts.find((item) => {
			return item.toLowerCase() === address.toLowerCase();
		}));
		
		return !!storageContracts.find((item) => {
			return item.toLowerCase() === address.toLowerCase();
		});
	};

	const getTokenTypeAlert = () => {
		const foundSaved = originalTokenCache.find((item) => {
			return (
				item.contractAddress.toLowerCase() ===
					inputOriginalTokenAddress.toLowerCase() &&
				item.tokenId.toLowerCase() === inputOriginalTokenId.toLowerCase()
			);
		});
		console.log('getTokenTypeAlert foundSaved', foundSaved);
		
		if (
			foundSaved &&
			(foundSaved.savedToken.badContract ||
				foundSaved.savedToken.originalTokenStandart === undefined)
		) {
			return (
				<div className='alert mt-md-3 mb-4 mb-md-0'>
					We can't automatically determine the type of a contract. If your
					contract is compatible with the ERC-1155 standard, please specify the
					number of tokens to be wrapped.
				</div>
			);
		}

		return null;
	};
	const getTokenOwnerAlert = () => {
		if (addressIsStorage(inputOriginalTokenAddress)) {
			return (
				<div className='alert alert-error mt-md-3 mb-4 mb-md-0'>
					Cannot wrap wrapped
				</div>
			);
		}

		if (originalTokensBlacklist) {
			const foundBlacklistItem = originalTokensBlacklist.find((item) => {
				return item.toLowerCase() === inputOriginalTokenAddress.toLowerCase();
			});
			if (foundBlacklistItem) {
				return (
					<div className='alert alert-error mt-md-3 mb-4 mb-md-0'>
						Token address is blacklisted
					</div>
				);
			}
		}

		const foundSaved = originalTokenCache.find((item) => {
			return (
				item.contractAddress.toLowerCase() ===
					inputOriginalTokenAddress.toLowerCase() &&
				item.tokenId.toLowerCase() === inputOriginalTokenId.toLowerCase()
			);
		});
		if (
			foundSaved &&
			foundSaved.savedToken.owner &&
			userAddress &&
			foundSaved.savedToken.owner.toLowerCase() !== userAddress.toLowerCase()
		) {
			return (
				<div className='alert alert-error mt-md-3 mb-4 mb-md-0'>
					Token is not yours
				</div>
			);
		}

		if (
			foundSaved &&
			foundSaved.savedToken.balance1155 &&
			foundSaved.savedToken.balance1155.lt(new BigNumber(inputOriginalCopies))
		) {
			return (
				<div className='alert alert-error mt-md-3 mb-4 mb-md-0'>
					Not enough original token balance
				</div>
			);
		}

		return null;
	};
	const getCollectionBlock = () => {
		const getTokenName = () => {
			if (inputOriginalTokenAddress === '') {
				return null;
			}
			if (inputOriginalTokenId === '') {
				return null;
			}

			const foundSaved = originalTokenCache.find((item) => {
				return (
					item.contractAddress.toLowerCase() ===
						inputOriginalTokenAddress.toLowerCase() &&
					item.tokenId.toLowerCase() === inputOriginalTokenId.toLowerCase()
				);
			});
			if (!foundSaved) {
				return null;
			}

			return (
				<p>
					Name <b>{foundSaved.savedToken.tokenName}</b>
				</p>
			);
		};
		const getProjectName = () => {
			console.log('getProjectName originalContractNamesCache', originalContractNamesCache);
			console.log('getProjectName inputOriginalTokenAddress', inputOriginalTokenAddress);
			
			const foundName = originalContractNamesCache.find((item) => {
				return (
					item.contractAddress.toLowerCase() ===
					inputOriginalTokenAddress.toLowerCase()
				);
			});
			if (!foundName) {
				return <b> — </b>;
			}
			return <b>{foundName.name}</b>;
		};
		const getProjectBlock = () => {
			if (!currentChain) {
				return null;
			}
			const foundSaved = originalTokenCache.find((item) => {
				return (
					item.contractAddress.toLowerCase() ===
						inputOriginalTokenAddress.toLowerCase() &&
					item.tokenId.toLowerCase() === inputOriginalTokenId.toLowerCase()
				);
			});

			if (foundSaved && foundSaved.savedToken.badContract) {
				return (
					<p>
						<a
							className='ex-link'
							target='_blank'
							rel='noopener noreferrer'
							href={combineURLs(
								currentChain.explorerBaseUrl,
								`/address/${inputOriginalTokenAddress}`,
							)}
						>
							{'Cannot connect to contract'}
							<img className='i-ex' src={icon_external} alt='' />
						</a>
					</p>
				);
			}
			if (!inputOriginalTokenAddress) {
				return <p>Project {getProjectName()}</p>;
			}
			return (
				<p>
					<a
						className='ex-link'
						target='_blank'
						rel='noopener noreferrer'
						href={combineURLs(
							currentChain.explorerBaseUrl,
							`/address/${inputOriginalTokenAddress}`,
						)}
					>
						Project {getProjectName()}
						<img className='i-ex' src={icon_external} alt='' />
					</a>
				</p>
			);
		};

		return (
			<div className='info'>
				{getTokenName()}
				{getProjectBlock()}
			</div>
		);
	};
	const getOriginalTokenCopiesBlock = () => {
		const foundSaved = originalTokenCache.find((item) => {
			return (
				item.contractAddress.toLowerCase() ===
					inputOriginalTokenAddress.toLowerCase() &&
				item.tokenId.toLowerCase() === inputOriginalTokenId.toLowerCase()
			);
		});
		if (
			foundSaved &&
			(foundSaved.savedToken.badContract ||
				foundSaved.savedToken.originalTokenStandart === undefined ||
				foundSaved.savedToken.originalTokenStandart === _AssetType.ERC1155)
		) {
			return (
				<div className='col-12 col-md-2'>
					<div className='input-group mb-md-0'>
						<label className='input-label'>Copies</label>
						<input
							className='input-control'
							type='text'
							value={inputOriginalCopies}
							onChange={(e) => {
								const value = e.target.value.replaceAll(' ', '');
								if (value === '' || isNaN(parseInt(value))) {
									setInputOriginalCopies('');
									return;
								}

								setInputOriginalCopies(`${parseInt(value)}`);
							}}
						/>
					</div>
				</div>
			);
		}

		return null;
	};
	const getOriginalTokenApproveForAll = () => {
		const foundSaved = originalTokenCache.find((item) => {
			return (
				item.contractAddress.toLowerCase() ===
					inputOriginalTokenAddress.toLowerCase() &&
				item.tokenId.toLowerCase() === inputOriginalTokenAddress.toLowerCase()
			);
		});
		if (!foundSaved) {
			return null;
		}

		if (
			foundSaved.savedToken.owner &&
			userAddress &&
			foundSaved.savedToken.owner.toLowerCase() !== userAddress.toLowerCase()
		) {
			return null;
		}

		if (
			foundSaved.savedToken.originalTokenStandart === _AssetType.ERC721 &&
			!foundSaved.savedToken.isApproved
		) {
			return (
				<div className='pt-4'>
					<label className='checkbox'>
						<input
							type='checkbox'
							name='tokens-approval'
							checked={inputApproveAll}
							onChange={(e) => {
								setInputApproveAll(e.target.checked);
							}}
						/>
						<span className='check'> </span>
						<span className='check-text'>
							Set approval for all tokens of&nbsp;contract
							<TippyWrapper msg='Grant permission to the protocol for all your DA (NFT) smart contract to use them'></TippyWrapper>
						</span>
					</label>
				</div>
			);
		}
	};
	const getTokenInfoBlock = () => {
		if (pageMode === 'simple') {
			return null;
		}
		if (wrapWith !== 'own') {
			return null;
		}

		const getCollectionImg = () => {
			const foundSaved = originalTokenCache.find((item) => {
				return (
					item.contractAddress.toLowerCase() ===
						inputOriginalTokenAddress.toLowerCase() &&
					item.tokenId.toLowerCase() === inputOriginalTokenId.toLowerCase()
				);
			});
			if (!foundSaved || !foundSaved.savedToken.collectionImg) {
				return (
					<div className='img'>
						<img src={default_token_preview} alt='' />
					</div>
				);
			}
			return (
				<div className='img'>
					<img src={foundSaved.savedToken.collectionImg} alt='' />
				</div>
			);
		};

		return (
			<div
				className='c-wrap'
				ref={(e) => {
					scrollable.originalToken = e;
					console.log('scrollable', scrollable);
					
				}}
			>
				<div className='row'>
					<div className='col-12 col-md-7'>
						<div className='input-group mb-md-0'>
							<label className='input-label'>
								DA Address
								<TippyWrapper msg='Address of DA token to wrap'></TippyWrapper>
							</label>
							<input
								className='input-control'
								type='text'
								placeholder='Paste here'
								disabled={false}
								value={inputOriginalTokenId}
								onChange={(e) => {
									setInputOriginalTokenId(
										e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ''),
									);
									clearTimeout(inputsOriginalTokenTimeout);

									inputsOriginalTokenTimeout = setTimeout(() => {
										setRequestOriginalTokenId(
											e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ''),
										);
									}, 0.5 * 1000);
								}}
							/>
						</div>
					</div>
				</div>

				{
				//getTokenTypeAlert()
				}
				{getTokenOwnerAlert()}

				<div className='row mt-md-4'>
					<div className='col-12 col-md-7'>
						<div className='wrap__nft-prev'>
							{getCollectionImg()}
							{getCollectionBlock()}
						</div>
					</div>
					{//getOriginalTokenCopiesBlock()
					}
				</div>
				{getOriginalTokenApproveForAll()}
			</div>
		);
		/*web3
return (
			<div
				className='c-wrap'
				ref={(e) => {
					scrollable.originalToken = e;
				}}
			>
				<div className='row'>
					<div className='col-12 col-md-7'>
						<div className='input-group mb-md-0'>
							<label className='input-label'>
								NFT Address
								<TippyWrapper msg='Minter contract address of your original NFT'></TippyWrapper>
							</label>
							<input
								className='input-control'
								type='text'
								placeholder='Paste here'
								disabled={false}
								value={inputOriginalTokenAddress}
								onChange={(e) => {
									setInputOriginalTokenAddress(
										e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ''),
									);
									clearTimeout(inputsOriginalTokenTimeout);

									inputsOriginalTokenTimeout = setTimeout(() => {
										setRequestOriginalTokenAddress(
											e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ''),
										);
									}, 0.5 * 1000);
								}}
							/>
						</div>
					</div>
					<div className='col-12 col-md-5'>
						<div className='input-group mb-md-0'>
							<label className='input-label'>
								Token ID
								<TippyWrapper msg='Token ID of your original NFT'></TippyWrapper>
							</label>
							<input
								className='input-control'
								type='text'
								placeholder='Paste here'
								disabled={false}
								value={inputOriginalTokenId}
								onChange={(e) => {
									setInputOriginalTokenId(
										e.target.value.toLowerCase().replace(/[^0-9]/g, ''),
									);
									clearTimeout(inputsOriginalTokenTimeout);

									inputsOriginalTokenTimeout = setTimeout(() => {
										setRequestOriginalTokenId(
											e.target.value.toLowerCase().replace(/[^0-9]/g, ''),
										);
									}, 0.5 * 1000);
								}}
							/>
						</div>
					</div>
				</div>

				{getTokenTypeAlert()}
				{getTokenOwnerAlert()}

				<div className='row mt-md-4'>
					<div className='col-12 col-md-7'>
						<div className='wrap__nft-prev'>
							{getCollectionImg()}
							{getCollectionBlock()}
						</div>
					</div>
					{getOriginalTokenCopiesBlock()}
				</div>
				{getOriginalTokenApproveForAll()}
			</div>
		);
		*/
	};

	const getDisclaimerBlock = () => {
		return (
			<div className='alert mb-5'>
				<div className='row'>
					<div className='col-md-2 d-none d-md-block'>
						<div className='pl-3'>
							<img className='img-fluid' src={attention_sm} alt='' />
						</div>
					</div>
					<div className='col-md-4 mb-2 mb-md-0 pt-3'>
						<p>
							<b className='text-green'>Disclaimer</b>
						</p>
						<p>
							Be careful when wrapping your DAs (NFTs) and adding collateral!
							Incorrect use of the wrap settings can result in loss of access to
							your assets.
						</p>
					</div>
					<div className='col-md mb-2 mb-md-0 pt-3'>
						<p>This can happen if you have setup:</p>
						<ol className='mt-0'>
							<li>Too long Unlock Settings</li>
						</ol>
					</div>
				</div>
			</div>
		);
	};

	const getCopiesBlock = () => {
		const showError = showErrors.find((item) => {
			return item === 'standart';
		});

		if (inputOutAssetType === _AssetType.ERC1155) {
			return (
				<div className='col-12 col-md-2'>
					<div className='input-group mb-md-0'>
						<label className='input-label'>Copies</label>
						<NumberInput
							value={inputCopies}
							onChange={(e: number | undefined) => {
								setInputCopies(e || 1);
								setShowErrors(
									showErrors.filter((item) => {
										return item !== 'standart';
									}),
								);
							}}
							min={1}
							inputClass={showError ? 'has-error' : ''}
						/>
					</div>
				</div>
			);
		}
	};
	const getStandartSelectorBlock = () => {
		if (pageMode === 'simple') {
			return null;
		}

		return (
			<div
				className='c-wrap'
				ref={(e) => {
					scrollable.standart = e;
				}}
			>
				<div className='c-wrap__header'>
					<div className='h3'>wDA (wNFT) Standard</div>
				</div>
				<div className='row'>
					<div className='col-12 col-md-5'>
						<div className='input-group mb-md-0'>
							<label className='input-label'>I want to create:</label>
							<div className='row row-sm'>
								<div className='col-auto my-2'>
									<label className='checkbox'>
										<input
											type='radio'
											name='wnft-standard'
											value={_AssetType.ERC721}
											checked={inputOutAssetType === _AssetType.ERC721}
											onChange={() => {
												setInputOutAssetType(_AssetType.ERC721);
											}}
										/>
										<span className='check'> </span>
										<span className='check-text'>
											<b>{currentChain?.EIPPrefix || 'ERC'}-721</b>
										</span>
									</label>
								</div>
								<div className='col-auto my-2'>
									<label className='checkbox'>
										<input
											type='radio'
											name='wnft-standard'
											value={_AssetType.ERC1155}
											checked={inputOutAssetType === _AssetType.ERC1155}
											onChange={() => {
												setInputOutAssetType(_AssetType.ERC1155);
											}}
										/>
										<span className='check'> </span>
										<span className='check-text'>
											{' '}
											<b>{currentChain?.EIPPrefix || 'ERC'}-1155</b>
										</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					{//getCopiesBlock()
					}
				</div>
			</div>
		);
	};
	const getAdvancedOptionsBlock = () => {
		if (pageMode === 'simple') {
			return null;
		}

		return (
			<div
				className='c-wrap p-0'
				ref={(e) => {
					scrollable.advancedOptions = e;
				}}
			>
				<div
					className={`c-wrap__toggle ${advancedOptionsOpened ? 'active' : ''}`}
					onClick={() => {
						setAdvancedOptionsOpened(!advancedOptionsOpened);
					}}
				>
					<div className='h3 mb-0'>
						<b>Advanced options</b>
					</div>
				</div>

				<div className='c-wrap__dropdown'>
					<div className='row mt-3'>
						<div className='col-md-6'>
							{/* <div className="input-group">
								<label className="input-label">Collateral Recipient</label>
								<input
									className="input-control"
									type="text"
									placeholder="Paste here"
									value={ inputCollateralRecipientAddress }
									onChange={(e) => { setInputCollateralRecipientAddress(e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "") ) }}
								/>
							</div> */}
							<div className='input-group'>
								<label className='input-label'>wDA (wNFT) Recipient</label>
								<input
									className='input-control'
									type='text'
									placeholder='Paste here'
									value={inputWNFTRecipientAddress}
									onChange={(e) => {
										setInputWNFTRecipientAddress(
											e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ''),
										);
									}}
								/>
							</div>
						</div>
						<div className='col-md-6 col-lg-5 offset-lg-1 pl-md-5 pl-lg-0'>
							<div className='input-group pt-2 mb-0'>
								<label className='input-label pb-1'>Disable:</label>
								<div className='mb-3'>
									<label className='checkbox'>
										<input
											type='checkbox'
											name=''
											checked={inputRules.noUnwrap}
											onChange={(e) => {
												setInputRules({
													...inputRules,
													noUnwrap: e.target.checked,
												});
												setInputTimeLockChecked(false);
												setInputTimeLockDays('');
												setInputFeeLockChecked(false);
												setInputFeeLockAmount('');

												if (!collateralPopupShown) {
													if (collaterals.length) {
														setModal({
															type: _ModalTypes.info,
															title: 'Warning',
															text: [
																{
																	text: 'You can lock collateral forever with these settings',
																	clazz: 'text-orange',
																},
															],
														});
														setCollateralPopupShown(true);
													}
												}
											}}
										/>
										<span className='check'> </span>
										<span className='check-text'>
											Unwrapping
											<TippyWrapper
												msg='The owner of a future wDA (wNFT) will be unable to unwrap it'
												elClass='ml-1'
											></TippyWrapper>
										</span>
									</label>
								</div>
								<div className='mb-3'>
									<label className='checkbox'>
										<input
											type='checkbox'
											name=''
											checked={inputRules.noAddCollateral}
											onChange={(e) => {
												setInputRules({
													...inputRules,
													noAddCollateral: e.target.checked,
												});
												setInputAddWNFTChecked(e.target.checked);
												setInputFeeLockChecked(false);
												setTimeout(() => {
													removeRecipient(undefined);
												}, 1);
											}}
										/>
										<span className='check'> </span>
										<span className='check-text'>
											Adding collateral
											<TippyWrapper
												msg='No one will be able to add collateral to future wDA (wNFT) transactions'
												elClass='ml-1'
											></TippyWrapper>
										</span>
									</label>
								</div>
								{/* <div className="mb-3">
									<label className="checkbox">
										<input
											type="checkbox"
											name=""
											checked={ inputRules.noWrap }
											onChange={(e) => {
												setInputRules({
													...inputRules,
													noWrap: e.target.checked
												})
											}}
										/>
										<span className="check"> </span>
										<span className="check-text">
											Second wrapping
											<TippyWrapper
												msg="The owner of a future wNFT will be unable to wrap it a second time"
												elClass="ml-1"
											></TippyWrapper>
										</span>
									</label>
								</div> */}
								<div>
									<label className='checkbox'>
										<input
											type='checkbox'
											name=''
											checked={inputRules.noTransfer}
											onChange={(e) => {
												if (e.target.checked) {
													setInputRules({
														...inputRules,
														noTransfer: e.target.checked,
													});
													setCollaterals(
														collaterals.filter((item: CollateralItem) => {
															return !item.amount || !item.amount.eq(0);
														}),
													);
												} else {
													if (
														inputAddWNFTChecked &&
														inputTransferFeeAmount !== ''
													) {
														setTimeout(() => {
															addCollateralERC20Row(true);
														}, 1);
													}
													setInputRules({
														...inputRules,
														noTransfer: e.target.checked,
													});
													setInputFeeLockChecked(false);
												}
											}}
										/>
										<span className='check'> </span>
										<span className='check-text'>
											Transferring (Soulbound{' '}
											<span className='text-nowrap'>
												{' '}
												Token)
												<TippyWrapper
													msg='The owner of future wDA (wNFT) will be unable to transfer it'
													elClass='ml-1'
												></TippyWrapper>
											</span>
										</span>
									</label>
									<p>
										<small className='text-muted'>
											These options are&nbsp;irreversible. The custodian
											is&nbsp;accountable for&nbsp;any protocol-related actions.
										</small>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	};

	// ----- COLLATERAL -----
	// ----- COLLATERAL NATIVE -----
	const isNativeBalanceEnough = () => {
		if (!userAddress) {
			return true;
		}
		const amountParsed = new BigNumber(inputCollateralAmount);

		if (!amountParsed || amountParsed.isNaN() || amountParsed.eq(0)) {
			return true;
		}
		console.log('amountParsed', amountParsed);
		console.log('currentChain?.decimals', currentChain?.decimals);
		console.log('tokenToInt(amountParsed, currentChain?.decimals || 8)', tokenToInt(amountParsed, currentChain?.decimals || 8));

		console.log('balanceNative', balanceNative);

		let addedNativecollaterals = new BigNumber(0);
		const nativecollaterals = collaterals.filter((item) => {
				return item.assetType == _AssetType.native;
			});
		console.log('nativecollaterals',nativecollaterals);
		
		if (nativecollaterals.length>0 &&
			nativecollaterals[0].amount) {
			addedNativecollaterals = nativecollaterals[0].amount;
		}
		
		const resInt = tokenToInt(amountParsed, currentChain?.decimals || 8).plus(addedNativecollaterals);

		if (
			resInt.lte(balanceNative)
		) {
			return true;
		}

		return false;
	};

	/*web3
	const isNativeBalanceEnough = () => {
		if (!userAddress) {
			return true;
		}
		const amountParsed = new BigNumber(inputCollateralAmount);

		if (!amountParsed || amountParsed.isNaN() || amountParsed.eq(0)) {
			return true;
		}
		if (
			tokenToInt(amountParsed, currentChain?.decimals || 8).lte(balanceNative)
		) {
			return true;
		}

		return false;
	};
*/
	const isCollateralNativeBtnDisabled = () => {
		if (inputCollateralAmount === '') {
			return true;
		}

		const amountParsed = new BigNumber(inputCollateralAmount);
		if (!amountParsed || amountParsed.isNaN() || amountParsed.eq(0)) {
			return true;
		}

		if (inputRules.noAddCollateral) {
			return true;
		}

		if (!isNativeBalanceEnough()) {
			return true;
		}

		const foundNativeCollateral = collaterals.find((item: CollateralItem) => {
			return item.assetType === _AssetType.native;
		});
		if (foundNativeCollateral) {
			return false;
		}

		if (isMaxCollaterals() || overMaxCollaterals()) {
			return true;
		}

		return false;
	};
	const addCollateralNativeRow = () => {
		if (!userAddress) {
			return true;
		}
		if (!collateralPopupShown) {
			if (inputRules.noUnwrap) {
				setModal({
					type: _ModalTypes.info,
					title: 'Warning',
					text: [
						{
							text: 'You can lock collateral forever with these settings',
							clazz: 'text-orange',
						},
					],
				});
				setCollateralPopupShown(true);
			}
		}

		let amountNative = new BigNumber(0);
		const nativeAdded = collaterals.filter((item) => {
			return item.assetType === _AssetType.native;
		});
		if (nativeAdded.length && nativeAdded[0].amount) {
			amountNative = new BigNumber(nativeAdded[0].amount);
		}
		const collateralsUpdated = [
			...collaterals.filter((item) => {
				return item.assetType !== _AssetType.native;
			}),
			{
				assetType: _AssetType.native,
				contractAddress: '0x0000000000000000000000000000000000000000',
				amount: tokenToInt(
					new BigNumber(inputCollateralAmount),
					currentChain?.decimals || 8,
				).plus(amountNative),
			},
		];
		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
	};
	const getAddCollateralNativeBtn = () => {
		return (
			<button
				className='btn btn-grad'
				disabled={isCollateralNativeBtnDisabled()}
				onClick={() => {
					addCollateralNativeRow();
				}}
			>
				Add
			</button>
		);
	};
	const getCollateralNativeBalance = () => {
		if (!userAddress) {
			return null;
		}
		return (
			<div className='c-add__max mt-3'>
				<div>
					<span>Max: </span>
					<button
						onClick={() => {
							setInputCollateralAmount(
								tokenToFloat(
									balanceNative,
									currentChain?.decimals || 8,
								).toString(),
							);
						}}
					>
						{tokenToFloat(balanceNative, currentChain?.decimals || 8).toFixed(
							5,
						)}
					</button>
				</div>
			</div>
		);
	};
	const getNativeAmountInput = () => {
		if (isNativeBalanceEnough()) {
			return (
				<React.Fragment>
					<div className='select-group'>
						<input
							className='input-control'
							type='text'
							placeholder='0.000'
							value={addThousandSeparator(inputCollateralAmount)}
							onChange={(e) => {
								let value = removeThousandSeparator(e.target.value);
								if (
									value !== '' &&
									!value.endsWith('.') &&
									!value.endsWith('0')
								) {
									if (new BigNumber(value).isNaN()) {
										return;
									}
									value = new BigNumber(value).toString();
								}
								setInputCollateralAmount(value);
							}}
							onKeyPress={(e) => {
								if (e.defaultPrevented) {
									return;
								}
								if (!!isCollateralNativeBtnDisabled()) {
									return;
								}
								if (e.key !== 'Enter') {
									return;
								}

								addCollateralNativeRow();
							}}
						/>
						<div className='select-coin'>
							<div className='select-coin__value'>
								<span className='field-unit'>
									<span className='i-coin'>
										<img src={currentChain?.tokenIcon || default_icon} alt='' />
									</span>
									{currentChain?.symbol ||
										compactString('0x0000000000000000000000000000000000000000')}
								</span>
							</div>
						</div>
					</div>
				</React.Fragment>
			);
		} else {
			return (
				<React.Fragment>
					<div className='select-group'>
						<input
							className='input-control has-error'
							type='text'
							placeholder='0.000'
							value={addThousandSeparator(inputCollateralAmount)}
							onChange={(e) => {
								let value = removeThousandSeparator(e.target.value);
								if (
									value !== '' &&
									!value.endsWith('.') &&
									!value.endsWith('0')
								) {
									if (new BigNumber(value).isNaN()) {
										return;
									}
									value = new BigNumber(value).toString();
								}
								setInputCollateralAmount(value);
							}}
							onKeyPress={(e) => {
								if (e.defaultPrevented) {
									return;
								}
								if (!!isCollateralNativeBtnDisabled()) {
									return;
								}
								if (e.key !== 'Enter') {
									return;
								}

								addCollateralNativeRow();
							}}
						/>
						<div className='select-coin'>
							<div className='select-coin__value'>
								<span className='field-unit'>
									<span className='i-coin'>
										<img src={currentChain?.tokenIcon || default_icon} alt='' />
									</span>
									{currentChain?.symbol ||
										compactString('0x0000000000000000000000000000000000000000')}
								</span>
							</div>
						</div>
					</div>
					<div className='input-error'>Not enough balance</div>
				</React.Fragment>
			);
		}
	};
	/*web3
	const getCollateralNativeBlock = () => {
		if (inputCollateralAssetType === _AssetType.native) {
			return (
				<div className='row'>
					<div className='col col-12 col-md-7'>
						<label className='input-label'>Amount</label>
						{getNativeAmountInput()}

						{getCollateralNativeBalance()}
					</div>
					<div className='col col-12 col-md-2'>
						<label className='input-label'>&nbsp;</label>
						{getAddCollateralNativeBtn()}
					</div>
				</div>
			);
		}
	};
*/
	const getCollateralNativeBlock = () => {
		return (
			<div className='row'>
				<div className='col col-12 col-md-7'>
					<label className='input-label'>Amount</label>
					{getNativeAmountInput()}

					{
						//getCollateralNativeBalance()
					}
				</div>
				<div className='col col-12 col-md-2'>
					<label className='input-label'>&nbsp;</label>
					{getAddCollateralNativeBtn()}
				</div>
			</div>
		);
	};
	// ----- END COLLATERAL NATIVE -----

	// ----- COLLATERAL ERC20 -----
	const isERC20BalanceEnough = () => {
		if (!userAddress) {
			return true;
		}
		if (inputCollateralAddress.toLowerCase() === techToken.toLowerCase()) {
			return true;
		}
		const amountParsed = new BigNumber(inputCollateralAmount);
		const foundToken = erc20List.find((item) => {
			return (
				item.contractAddress.toLowerCase() ===
				inputCollateralAddress.toLowerCase()
			);
		});
		if (
			foundToken &&
			foundToken.balance.lt(tokenToInt(amountParsed, foundToken.decimals || 8))
		) {
			return false;
		}

		return true;
	};
	const isCollateralERC20BtnDisabled = () => {
		if (inputCollateralAddress === '') {
			return true;
		}
		if (inputCollateralAmount === '') {
			return true;
		}

		const amountParsed = new BigNumber(inputCollateralAmount);
		if (!amountParsed || amountParsed.isNaN() || amountParsed.eq(0)) {
			return true;
		}

		if (inputRules.noAddCollateral) {
			return true;
		}

		if (!isCollateralAddressInWhitelist()) {
			return true;
		}
		if (!isERC20BalanceEnough()) {
			return true;
		}

		const foundERC20Collateral = collaterals.find((item: CollateralItem) => {
			return (
				item.assetType === _AssetType.ERC20 &&
				item.contractAddress.toLowerCase() ===
					inputCollateralAddress.toLowerCase()
			);
		});
		if (!foundERC20Collateral) {
			if (isMaxCollaterals() || overMaxCollaterals()) {
				return true;
			}
		}

		return false;
	};
	const addCollateralERC20Row = (empty?: boolean) => {
		if (!collateralPopupShown) {
			if (inputRules.noUnwrap) {
				setModal({
					type: _ModalTypes.info,
					title: 'Warning',
					text: [
						{
							text: 'You can lock collateral forever with these settings',
							clazz: 'text-orange',
						},
					],
				});
				setCollateralPopupShown(true);
			}
		}

		// empty means 0-amount collateral slot
		// in case of checked the wNFT royalty recipient
		// for collecting royalty

		let amountAdded = new BigNumber(0);
		let amountToAdd = empty
			? new BigNumber(0)
			: new BigNumber(inputCollateralAmount);
		let addressToAdd = empty ? inputTransferFeeAddress : inputCollateralAddress;

		const erc20Added = collaterals.filter((item) => {
			if (!item.contractAddress) {
				return false;
			}
			return item.contractAddress.toLowerCase() === addressToAdd.toLowerCase();
		});
		if (erc20Added.length && erc20Added[0].amount) {
			amountAdded = new BigNumber(erc20Added[0].amount);
		}

		let tokenToAdd = undefined;

		const foundToken = erc20List.filter((item) => {
			if (!item.contractAddress) {
				return false;
			}
			return item.contractAddress.toLowerCase() === addressToAdd.toLowerCase();
		});
		if (foundToken.length) {
			tokenToAdd = foundToken[0];
		}

		if (tokenToAdd) {
			amountAdded = tokenToFloat(
				new BigNumber(amountAdded),
				tokenToAdd.decimals || 8,
			);
			amountToAdd = tokenToInt(
				amountToAdd.plus(amountAdded),
				tokenToAdd.decimals || 8,
			);
		} else {
			amountToAdd = amountToAdd.plus(amountAdded);
		}

		let collateralsUpdated = collaterals;
		if (empty) {
			collateralsUpdated = collateralsUpdated.filter((item: CollateralItem) => {
				return !item.amount || !item.amount.eq(0);
			});
		}
		collateralsUpdated = [
			...collateralsUpdated.filter((item) => {
				if (!item.contractAddress) {
					return false;
				}
				return (
					item.contractAddress.toLowerCase() !== addressToAdd.toLowerCase()
				);
			}),
			{
				assetType: _AssetType.ERC20,
				contractAddress: addressToAdd,
				amount: amountToAdd,
			},
		];

		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
		setInputCollateralAddress('');
	};
	const getAddCollateralERC20Btn = () => {
		return (
			<button
				className='btn btn-grad'
				disabled={!!isCollateralERC20BtnDisabled()}
				onClick={() => {
					addCollateralERC20Row();
				}}
			>
				Add
			</button>
		);
	};
	const getCollateralERC20AmountTitle = () => {
		const foundERC20 = erc20List.filter((item) => {
			if (!item.contractAddress) {
				return false;
			}
			return (
				item.contractAddress.toLowerCase() ===
				inputCollateralAddress.toLowerCase()
			);
		});
		if (
			inputCollateralAddress &&
			inputCollateralAddress !== '' &&
			inputCollateralAddress !== '0' &&
			inputCollateralAddress !== '0x0000000000000000000000000000000000000000' &&
			!foundERC20.length
		) {
			return (
				<TippyWrapper msg='Cannot get decimals from contract, enter amount in wei'>
					<label className='input-label text-orange'>Amount*</label>
				</TippyWrapper>
			);
		} else {
			return (
				<label className='input-label'>
					Amount
					<TippyWrapper msg='Maximum and allowanced amount of tokens which you can add to collateral of wrapped nft'></TippyWrapper>
				</label>
			);
		}
		// return ( <label className="input-label">{ t('Amount') }</label> )
	};
	const getCollateralERC20Balance = () => {
		if (!userAddress) {
			return null;
		}
		if (inputCollateralAddress.toLowerCase() === techToken.toLowerCase()) {
			return null;
		}
		if (inputCollateralAddress === '') {
			return null;
		}

		const foundToken = erc20List.find((item) => {
			return (
				item.contractAddress.toLowerCase() ===
				inputCollateralAddress.toLowerCase()
			);
		});
		if (!foundToken) {
			return null;
		}
		return (
			<div className='c-add__max mt-2 mb-0'>
				<div className='mt-1 mb-1'>
					<span>Max: </span>
					<button
						onClick={() => {
							setInputCollateralAmount(
								tokenToFloat(
									foundToken.balance,
									foundToken.decimals || 8,
								).toString(),
							);
						}}
					>
						{tokenToFloat(
							foundToken.balance,
							foundToken.decimals || 8,
						).toString()}
					</button>
				</div>
				{/* <div>
					<span>Allowance: </span>
					<button
						onClick={() => { setInputCollateralAmount(tokenToFloat(foundToken.allowance, foundToken.decimals || 8).toString()) }}
					>{ tokenToFloat(foundToken.allowance, foundToken.decimals || 8).toString() }</button>
				</div> */}
			</div>
		);
	};
	const getCollateralERC20AddressInput = () => {
		return (
			<React.Fragment>
				<div className='select-group'>
					<input
						className={`input-control ${
							!isCollateralAddressInWhitelist() ? 'has-error' : ''
						}`}
						type='text'
						placeholder='0.000'
						value={inputCollateralAddress}
						onChange={(e) => {
							const value = e.target.value
								.toLowerCase()
								.replace(/[^a-f0-9x]/g, '');
							setInputCollateralAddress(value);
							if (isAptosAddress(value)) {
								const foundToken = erc20List.find((item) => {
									return (
										item.contractAddress.toLowerCase() === value.toLowerCase()
									);
								});
								if (!foundToken || foundToken.balance.eq(0)) {
									requestERC20Token(value, userAddress);
								}
							}
						}}
						onKeyPress={(e) => {
							if (e.defaultPrevented) {
								return;
							}
							if (!!isCollateralERC20BtnDisabled()) {
								return;
							}
							if (e.key !== 'Enter') {
								return;
							}

							addCollateralERC20Row();
						}}
					/>
					<CoinSelector
						tokens={filterERC20ContractByPermissions({
							enabledForCollateral: true,
						})}
						selectedToken={inputCollateralAddress}
						onChange={(address: string) => {
							setInputCollateralAddress(address);
							if (isAptosAddress(address)) {
								const foundToken = erc20List.find((item) => {
									return (
										item.contractAddress.toLowerCase() === address.toLowerCase()
									);
								});
								if (!foundToken || foundToken.balance.eq(0)) {
									requestERC20Token(address, userAddress);
								}
							}
						}}
					/>
				</div>
				{!isCollateralAddressInWhitelist() ? (
					<div className='input-warning'>
						Please use the{' '}
						<a href='/saft' target='_blank' rel='noopener noreferrer'>
							SAFT dApp
						</a>{' '}
						if you want to add non-whitelisted tokens to your collateral.
					</div>
				) : null}
			</React.Fragment>
		);
	};
	const getCollateralERC20AmountInput = () => {
		if (!isERC20BalanceEnough()) {
			return (
				<React.Fragment>
					<input
						className='input-control has-error'
						type='text'
						placeholder=''
						value={addThousandSeparator(inputCollateralAmount)}
						onChange={(e) => {
							let value = removeThousandSeparator(e.target.value);
							if (
								value !== '' &&
								!value.endsWith('.') &&
								!value.endsWith('0')
							) {
								if (new BigNumber(value).isNaN()) {
									return;
								}
								value = new BigNumber(value).toString();
							}
							setInputCollateralAmount(value);
						}}
						onKeyPress={(e) => {
							if (e.defaultPrevented) {
								return;
							}
							if (!!isCollateralERC20BtnDisabled()) {
								return;
							}
							if (e.key !== 'Enter') {
								return;
							}

							addCollateralERC20Row();
						}}
					/>
					<div className='input-error'>Not enough balance</div>
				</React.Fragment>
			);
		}

		return (
			<React.Fragment>
				<input
					className='input-control'
					type='text'
					placeholder=''
					value={addThousandSeparator(inputCollateralAmount)}
					onChange={(e) => {
						let value = removeThousandSeparator(e.target.value);
						if (value !== '' && !value.endsWith('.') && !value.endsWith('0')) {
							if (new BigNumber(value).isNaN()) {
								return;
							}
							value = new BigNumber(value).toString();
						}
						setInputCollateralAmount(value);
					}}
					onKeyPress={(e) => {
						if (e.defaultPrevented) {
							return;
						}
						if (!!isCollateralERC20BtnDisabled()) {
							return;
						}
						if (e.key !== 'Enter') {
							return;
						}

						addCollateralERC20Row();
					}}
				/>
			</React.Fragment>
		);
	};
	const getERC20Link = () => {
		if (!currentChain) {
			return null;
		}
		if (inputCollateralAddress === '') {
			return null;
		}
		const foundToken = erc20List.find((item) => {
			return (
				item.contractAddress.toLowerCase() ===
				inputCollateralAddress.toLowerCase()
			);
		});

		if (!foundToken) {
			return null;
		}

		let tokenImage = '';
		if (foundToken.name.toLowerCase() === 'niftsy') {
			tokenImage = niftsyTokenIcon;
		}

		return (
			<div className='d-flex align-items-center flex-wrap'>
				<a
					className='ex-link mr-3 mt-3 mb-3'
					target='_blank'
					rel='noopener noreferrer'
					href={combineURLs(
						currentChain.explorerBaseUrl,
						`/token/${foundToken.contractAddress}`,
					)}
				>
					<small>More about {foundToken.symbol}</small>
					<img className='i-ex' src={icon_external} alt='' />
				</a>
				<a
					className='btn btn-sm btn-gray'
					href='/'
					onClick={(e) => {
						e.preventDefault();
						if (!foundToken) {
							return;
						}
						try {
							if (!(window as any).ethereum) {
								return;
							}
							// TODO
							(window as any).ethereum.request({
								method: 'wallet_watchAsset',
								params: {
									type: 'ERC20', // Initially only supports ERC20, but eventually more!
									options: {
										address: foundToken.contractAddress, // The address that the token is at.
										symbol: foundToken.symbol, // A ticker symbol or shorthand, up to 5 chars.
										decimals: foundToken.decimals, // The number of decimals in the token
										image: tokenImage, // A string url of the token logo
									},
								},
							});
						} catch (e) {
							console.log(e);
						}
					}}
				>
					Add to Metamask
				</a>
			</div>
		);
	};
	const getCollateralERC20Block = () => {
		if (inputCollateralAssetType === _AssetType.ERC20) {
			return (
				<div className='row'>
					<div className='col col-12 col-md-7'>
						<label className='input-label'>Token Address</label>
						{getCollateralERC20AddressInput()}
						{getERC20Link()}
					</div>
					<div className='col col-12 col-md-3'>
						{getCollateralERC20AmountTitle()}
						{getCollateralERC20AmountInput()}
						{getCollateralERC20Balance()}
					</div>
					<div className='col col-12 col-md-2'>
						<label className='input-label'>&nbsp;</label>
						{getAddCollateralERC20Btn()}
					</div>
				</div>
			);
		}
	};
	// ----- END COLLATERAL ERC20 -----

	// ----- COLLATERAL ERC721 -----
	const isCollateralERC721BtnDisabled = () => {
		if (inputCollateralAddress === '') {
			return true;
		}
		if (inputCollateralTokenId === '') {
			return true;
		}

		if (inputRules.noAddCollateral) {
			return true;
		}

		if (!isCollateralAddressInWhitelist()) {
			return true;
		}

		if (isMaxCollaterals() || overMaxCollaterals()) {
			return true;
		}

		return false;
	};
	const addCollateralERC721Row = () => {
		if (!collateralPopupShown) {
			if (inputRules.noUnwrap) {
				setModal({
					type: _ModalTypes.info,
					title: 'Warning',
					text: [
						{
							text: 'You can lock collateral forever with these settings',
							clazz: 'text-orange',
						},
					],
				});
				setCollateralPopupShown(true);
			}
		}

		const address = inputCollateralAddress;
		const tokenId = inputCollateralTokenId;

		const collateralsUpdated = [
			...collaterals.filter((item) => {
				if (item.assetType !== _AssetType.ERC721) {
					return true;
				}
				if (!item.contractAddress) {
					return false;
				}
				return (
					item.contractAddress.toLowerCase() !== address.toLowerCase() ||
					item.tokenId !== tokenId
				);
			}),
			{
				assetType: _AssetType.ERC721,
				contractAddress: address,
				tokenId: tokenId,
			},
		];

		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
		setInputCollateralAddress('');
		setInputCollateralTokenId('');

		if (!currentChain) {
			return;
		}
		if (!userAddress) {
			return;
		}
		//web3 getNFTById(currentChain.chainId, address, tokenId, userAddress)
		let networkID = network
			? network.chainId
				? parseInt(network.chainId)
				: 1
			: 1;
		console.log('network', network);

		getDAFromChain(tokenId, networkID, userAddress || '')
			.then((data) => {
				if (!data) {
					return;
				}

				const collateralsUpdated = [
					...collaterals.filter((item) => {
						if (item.assetType !== _AssetType.ERC721) {
							return true;
						}
						if (!item.contractAddress) {
							return false;
						}
						return (
							item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId !== tokenId
						);
					}),
					{
						assetType: _AssetType.ERC721,
						contractAddress: address,
						tokenId: tokenId,
						tokenImg: data.token.image || '',
					},
				];

				let collateralErrorsUpdated = collateralErrors;
				if (
					data.token.owner &&
					data.token.owner.toLowerCase() !== userAddress.toLowerCase()
				) {
					collateralErrorsUpdated = [
						...collateralErrorsUpdated,
						{
							contractAddress: address,
							tokenId: tokenId,
							msg: 'Not yours',
						},
					];
				}

				setCollaterals(collateralsUpdated);
				setCollateralErrors(collateralErrorsUpdated);
			})
			.catch((error) => {
				console.log('Cannot fetch 721 token', error);
				const collateralsUpdated = [
					...collaterals.filter((item) => {
						if (item.assetType !== _AssetType.ERC721) {
							return true;
						}
						if (!item.contractAddress) {
							return false;
						}
						return (
							item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId !== tokenId
						);
					}),
					{
						assetType: _AssetType.ERC721,
						contractAddress: address,
						tokenId: tokenId,
						tokenImg: '',
					},
				];

				let collateralErrorsUpdated = [
					...collateralErrors,
					{
						contractAddress: address,
						tokenId: tokenId,
						msg: 'Cannot fetch token',
					},
				];

				setCollaterals(collateralsUpdated);
				setCollateralErrors(collateralErrorsUpdated);
			});
	};
	const getAddCollateralERC721Btn = () => {
		return (
			<button
				className='btn btn-grad'
				disabled={isCollateralERC721BtnDisabled()}
				onClick={() => {
					addCollateralERC721Row();
				}}
			>
				Add
			</button>
		);
	};
	const getCollateralERC721AddressInput = () => {
		if (!isCollateralAddressInWhitelist()) {
			return (
				<React.Fragment>
					<input
						className='input-control'
						type='text'
						placeholder='Paste here'
						value={inputCollateralAddress}
						onChange={(e) => {
							setInputCollateralAddress(
								e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ''),
							);
						}}
						onKeyPress={(e) => {
							if (e.defaultPrevented) {
								return;
							}
							if (!!isCollateralERC20BtnDisabled()) {
								return;
							}
							if (e.key !== 'Enter') {
								return;
							}

							addCollateralERC721Row();
						}}
					/>
					<div className='input-error'>Address is not in whitelist</div>
				</React.Fragment>
			);
		}

		return (
			<React.Fragment>
				<input
					className='input-control'
					type='text'
					placeholder='Paste here'
					value={inputCollateralAddress}
					onChange={(e) => {
						setInputCollateralAddress(
							e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ''),
						);
					}}
					onKeyPress={(e) => {
						if (e.defaultPrevented) {
							return;
						}
						if (!!isCollateralERC20BtnDisabled()) {
							return;
						}
						if (e.key !== 'Enter') {
							return;
						}

						addCollateralERC721Row();
					}}
				/>
			</React.Fragment>
		);
	};
	const getCollateralERC721Block = () => {
		if (inputCollateralAssetType === _AssetType.ERC721) {
			return (
				<div className='row'>
					<div className='col col-12 col-md-7'>
						<label className='input-label'>DA Address</label>
						{getCollateralERC721AddressInput()}
					</div>
					<div className='col col-12 col-md-3'></div>
					<div className='col col-12 col-md-2'>
						<label className='input-label'>&nbsp;</label>
						{getAddCollateralERC721Btn()}
					</div>
				</div>
			);
		}
	};
	// ----- END COLLATERAL ERC721 -----

	/*web3
	const getCollateralERC721Block = () => {
		if (inputCollateralAssetType === _AssetType.ERC721) {
			return (
				<div className='row'>
					<div className='col col-12 col-md-7'>
						<label className='input-label'>NFT Address</label>
						{getCollateralERC721AddressInput()}
					</div>
					<div className='col col-12 col-md-3'>
						<label className='input-label'>Token ID</label>
						<input
							className='input-control'
							type='text'
							placeholder='99 900'
							value={addThousandSeparator(inputCollateralTokenId)}
							onChange={(e) => {
								setInputCollateralTokenId(
									e.target.value.toLowerCase().replace(/[^0-9]/g, ''),
								);
							}}
							onKeyPress={(e) => {
								if (e.defaultPrevented) {
									return;
								}
								if (!!isCollateralERC20BtnDisabled()) {
									return;
								}
								if (e.key !== 'Enter') {
									return;
								}

								addCollateralERC721Row();
							}}
						/>
					</div>
					<div className='col col-12 col-md-2'>
						<label className='input-label'>&nbsp;</label>
						{getAddCollateralERC721Btn()}
					</div>
				</div>
			);
		}
	};
*/ // ----- END COLLATERAL ERC721 -----

	// ----- COLLATERAL ERC1155 -----
	const isCollateralERC1155BtnDisabled = () => {
		if (inputCollateralAddress === '') {
			return true;
		}
		if (inputCollateralTokenId === '') {
			return true;
		}

		if (inputRules.noAddCollateral) {
			return true;
		}

		const amountParsed = new BigNumber(inputCollateralAmount);
		if (!amountParsed || amountParsed.isNaN() || amountParsed.eq(0)) {
			return true;
		}

		if (!isCollateralAddressInWhitelist()) {
			return true;
		}

		if (isMaxCollaterals() || overMaxCollaterals()) {
			return true;
		}

		return false;
	};
	const addCollateralERC1155Row = () => {
		if (!collateralPopupShown) {
			if (inputRules.noUnwrap) {
				setModal({
					type: _ModalTypes.info,
					title: 'Warning',
					text: [
						{
							text: 'You can lock collateral forever with these settings',
							clazz: 'text-orange',
						},
					],
				});
				setCollateralPopupShown(true);
			}
		}

		const address = inputCollateralAddress;
		const tokenId = inputCollateralTokenId;
		const amount = new BigNumber(inputCollateralAmount);

		const collateralsUpdated = [
			...collaterals.filter((item) => {
				if (item.assetType !== _AssetType.ERC1155) {
					return true;
				}
				if (!item.contractAddress) {
					return false;
				}
				return (
					item.contractAddress.toLowerCase() !== address.toLowerCase() ||
					item.tokenId !== tokenId
				);
			}),
			{
				assetType: _AssetType.ERC1155,
				contractAddress: address,
				tokenId: tokenId,
				amount: amount,
			},
		];

		setCollaterals(collateralsUpdated);
		setInputCollateralAmount('');
		setInputCollateralAddress('');
		setInputCollateralTokenId('');

		if (!currentChain) {
			return;
		}
		if (!userAddress) {
			return;
		}

		//web3 getNFTById(currentChain.chainId, address, tokenId, userAddress)
		let networkID = network
			? network.chainId
				? parseInt(network.chainId)
				: 1
			: 1;
		//		console.log('network',network);

		getDAFromChain(tokenId, networkID, userAddress || '')
			.then((data) => {
				if (!data) {
					return;
				}

				const collateralsUpdated = [
					...collaterals.filter((item) => {
						if (item.assetType !== _AssetType.ERC1155) {
							return true;
						}
						if (!item.contractAddress) {
							return false;
						}
						return (
							item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId !== tokenId
						);
					}),
					{
						assetType: _AssetType.ERC1155,
						contractAddress: address,
						tokenId: tokenId,
						amount: amount,
						tokenImg: data.token.image || '',
					},
				];

				let collateralErrorsUpdated = collateralErrors;
				if (data.token.amount && data.token.amount.lt(amount)) {
					collateralErrorsUpdated = [
						...collateralErrorsUpdated,
						{
							contractAddress: address,
							tokenId: tokenId,
							msg: 'Not enough balance',
						},
					];
				}

				setCollaterals(collateralsUpdated);
				setCollateralErrors(collateralErrorsUpdated);
			})
			.catch((error) => {
				console.log('Cannot fetch 1155 token', error);
				const collateralsUpdated = [
					...collaterals.filter((item) => {
						if (item.assetType !== _AssetType.ERC1155) {
							return true;
						}
						if (!item.contractAddress) {
							return false;
						}
						return (
							item.contractAddress.toLowerCase() !== address.toLowerCase() ||
							item.tokenId !== tokenId
						);
					}),
					{
						assetType: _AssetType.ERC1155,
						contractAddress: address,
						tokenId: tokenId,
						amount: amount,
						tokenImg: '',
					},
				];

				let collateralErrorsUpdated = [
					...collateralErrors,
					{
						contractAddress: address,
						tokenId: tokenId,
						msg: 'Cannot fetch token',
					},
				];

				setCollaterals(collateralsUpdated);
				setCollateralErrors(collateralErrorsUpdated);
			});
	};
	const getAddCollateralERC1155Btn = () => {
		return (
			<button
				className='btn btn-grad'
				disabled={isCollateralERC1155BtnDisabled()}
				onClick={() => {
					addCollateralERC1155Row();
				}}
			>
				Add
			</button>
		);
	};
	const getCollateralERC1155AddressInput = () => {
		if (!isCollateralAddressInWhitelist()) {
			return (
				<React.Fragment>
					<input
						className='input-control'
						type='text'
						placeholder='Paste here'
						value={inputCollateralAddress}
						onChange={(e) => {
							setInputCollateralAddress(
								e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ''),
							);
						}}
						onKeyPress={(e) => {
							if (e.defaultPrevented) {
								return;
							}
							if (!!isCollateralERC20BtnDisabled()) {
								return;
							}
							if (e.key !== 'Enter') {
								return;
							}

							addCollateralERC1155Row();
						}}
					/>
					<div className='input-error'>Address is not in whitelist</div>
				</React.Fragment>
			);
		}

		return (
			<React.Fragment>
				<input
					className='input-control'
					type='text'
					placeholder='Paste here'
					value={inputCollateralAddress}
					onChange={(e) => {
						setInputCollateralAddress(
							e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ''),
						);
					}}
					onKeyPress={(e) => {
						if (e.defaultPrevented) {
							return;
						}
						if (!!isCollateralERC20BtnDisabled()) {
							return;
						}
						if (e.key !== 'Enter') {
							return;
						}

						addCollateralERC1155Row();
					}}
				/>
			</React.Fragment>
		);
	};
	const getCollateralERC1155Block = () => {
		if (inputCollateralAssetType === _AssetType.ERC1155) {
			return (
				<div className='row'>
					<div className='col col-12 col-md-5'>
						<label className='input-label'>DA (NFT) Address</label>
						{getCollateralERC1155AddressInput()}
					</div>
					<div className='col col-12 col-md-3'>
						<label className='input-label'>Token ID</label>
						<input
							className='input-control'
							type='text'
							placeholder='99 900'
							value={addThousandSeparator(inputCollateralTokenId)}
							onChange={(e) => {
								setInputCollateralTokenId(
									e.target.value.toLowerCase().replace(/[^0-9]/g, ''),
								);
							}}
							onKeyPress={(e) => {
								if (e.defaultPrevented) {
									return;
								}
								if (!!isCollateralERC20BtnDisabled()) {
									return;
								}
								if (e.key !== 'Enter') {
									return;
								}

								addCollateralERC1155Row();
							}}
						/>
					</div>
					<div className='col col-12 col-md-2'>
						<label className='input-label'>Amount</label>
						<input
							className='input-control'
							type='text'
							placeholder='10'
							value={addThousandSeparator(inputCollateralAmount)}
							onChange={(e) => {
								const value = removeThousandSeparator(
									e.target.value.replaceAll(' ', ''),
								);
								if (value === '' || isNaN(parseInt(value))) {
									setInputCollateralAmount('');
									return;
								}

								setInputCollateralAmount(`${parseInt(value)}`);
							}}
							onKeyPress={(e) => {
								if (e.defaultPrevented) {
									return;
								}
								if (!!isCollateralERC20BtnDisabled()) {
									return;
								}
								if (e.key !== 'Enter') {
									return;
								}

								addCollateralERC20Row();
							}}
						/>
					</div>
					<div className='col col-12 col-md-2'>
						<label className='input-label'>&nbsp;</label>
						{getAddCollateralERC1155Btn()}
					</div>
				</div>
			);
		}
	};
	// ----- END COLLATERAL ERC1155 -----

	const isCollateralAddressInWhitelist = () => {
		if (inputCollateralAddress === '') {
			return true;
		}
		if (collateralWhitelist) {
			const foundWhitelistItem = collateralWhitelist.find((item: string) => {
				return item.toLowerCase() === inputCollateralAddress.toLowerCase();
			});
			if (!foundWhitelistItem) {
				return false;
			}
		}

		return true;
	};
	const isMaxCollaterals = () => {
		if (inputCollateralSlots !== undefined) {
			if (collaterals.length === inputCollateralSlots) {
				return true;
			}
		}
		if (collaterals.length === maxCollaterals) {
			return true;
		}

		return false;
	};
	const overMaxCollaterals = () => {
		if (inputCollateralSlots !== undefined) {
			if (collaterals.length > inputCollateralSlots) {
				return true;
			}
		}
		if (collaterals.length > maxCollaterals) {
			return true;
		}

		return false;
	};
	const getNonRemovableAddress = () => {
		if (inputTransferFeeAmount === '') {
			return undefined;
		}
		if (new BigNumber(inputTransferFeeAmount).isNaN()) {
			return undefined;
		}
		if (new BigNumber(inputTransferFeeAmount).eq(0)) {
			return undefined;
		}
		if (!inputAddWNFTChecked) {
			return undefined;
		}

		return inputTransferFeeAddress;
	};
	const getCollateralRows = () => {
		return (
			<CollateralViewer
				collaterals={collaterals.sort((item, prev) => {
					if (
						inputTransferFeeAmount !== '' &&
						!new BigNumber(inputTransferFeeAmount).isNaN() &&
						!new BigNumber(inputTransferFeeAmount).eq(0) &&
						inputAddWNFTChecked
					) {
						if (
							item.contractAddress.toLowerCase() ===
							inputTransferFeeAddress.toLowerCase()
						) {
							return -1;
						} else {
							return 1;
						}
					}

					return item.assetType - prev.assetType;
				})}
				collateralErrors={collateralErrors}
				nonRemovableAddress={getNonRemovableAddress()}
				removeRow={(item: CollateralItem) => {
					let collateralsUpdated = collaterals;

					if (
						inputAddWNFTChecked &&
						item.contractAddress.toLowerCase() ===
							inputTransferFeeAddress.toLowerCase()
					) {
						collateralsUpdated = [
							...collateralsUpdated.filter((iitem) => {
								return (
									iitem.contractAddress.toLowerCase() !==
										item.contractAddress.toLowerCase() ||
									(iitem.tokenId &&
										item.tokenId &&
										iitem.tokenId.toLowerCase() !== item.tokenId.toLowerCase())
								);
							}),
							{
								...item,
								amount: new BigNumber(0),
							},
						];
					} else {
						collateralsUpdated = [
							...collateralsUpdated.filter((iitem) => {
								return (
									iitem.contractAddress.toLowerCase() !==
										item.contractAddress.toLowerCase() ||
									(iitem.tokenId &&
										item.tokenId &&
										iitem.tokenId.toLowerCase() !== item.tokenId.toLowerCase())
								);
							}),
						];
					}

					setCollaterals(collateralsUpdated);
					setCollateralErrors(
						collateralErrors.filter((iitem) => {
							return (
								item.contractAddress.toLowerCase() !==
									iitem.contractAddress.toLowerCase() ||
								item.tokenId !== iitem.tokenId
							);
						}),
					);
				}}
				width={'wide'}
			/>
		);
	};
	const getMaxCollateralAlert = () => {
		if (
			inputCollateralSlots &&
			inputCollateralSlots === 0 &&
			!collaterals.length
		) {
			return null;
		}
		if (overMaxCollaterals()) {
			return (
				<div className='alert alert-error mb-3'>
					You have changed max collateral slots. Added amount exceeds allowed
					amount
				</div>
			);
		}
		if (isMaxCollaterals()) {
			return (
				<div className='alert alert-warning mb-3'>
					You have reached the maximum number of collateral slots
				</div>
			);
		}

		return null;
	};
	const getCollateralSlotSelector = () => {
		return (
			<div className='d-flex align-items-center'>
				<span className='text-muted text-right mr-2'>
					Mах entries
					<TippyWrapper msg='Enter the maximum number of tokens in collateral'></TippyWrapper>
				</span>
				<NumberInput
					value={inputCollateralSlots}
					onChange={(e: number | undefined) => {
						setInputCollateralSlots(e);
					}}
					min={0}
					max={maxCollaterals}
					placeholder={`${maxCollaterals}`}
				/>
			</div>
		);

		// <div className="d-flex align-items-center">
		// 	<img className="mr-2" src={ icon_attention } alt="" />
		// 	<span className="text-muted">max 25 entries</span>
		// 	<input
		// 		className="input-control"
		// 		type="text"
		// 		placeholder={ `${maxCollaterals}` }
		// 		value={ inputCollateralSlots }
		// 		onChange={(e) => {
		// 			const value = e.target.value.replaceAll(' ', '');
		// 			if (
		// 				value === '' ||
		// 				isNaN(parseInt(value))
		// 			) {
		// 				setState({ inputCollateralSlots: '' })
		// 				return;
		// 			}

		// 			setState({ inputCollateralSlots: `${parseInt(value)}` })
		// 		}}
		// 	/>
		// </div>
	};
	const getCollateralBlock = () => {
		if (inputRules.noAddCollateral) {
			return null;
		}

		return (
			<div
				className='c-wrap'
				ref={(e) => {
					scrollable.collaterals = e;
				}}
			>
				<div className='c-wrap__header'>
					<div className='h3'>
						Collateral
						<TippyWrapper
							msg='Assets with which you provide your DA (NFT)'
							elClass='ml-1'
						></TippyWrapper>
					</div>
					{
						//web3
						//getCollateralSlotSelector()
					}
				</div>
				{
					//web3
					//getMaxCollateralAlert()
				}
				<div className='alert mb-4'>
					You can add assets to collateral of your wrapped DA (wNFT). Use list of
					approved tokens.
				</div>
				<div className='c-wrap__form'>{getCollateralERC721Block()}</div>

				{getCollateralRows()}
			</div>
		);
	};

	const getNativeCollateralBlock = () => {
		return (
			<div
				className='c-wrap'
				ref={(e) => {
					scrollable.collaterals = e;
				}}
			>
				<div className='c-wrap__header'>
					<div className='h3'>
						Native Collateral
						<TippyWrapper
							msg='Aptos coin with which you provide your DA (NFT)'
							elClass='ml-1'
						></TippyWrapper>
					</div>
					{
						//web3
						//getCollateralSlotSelector()
					}
				</div>
				{
					//web3
					//getMaxCollateralAlert()
				}
				<div className='alert mb-4'>
					You can add Aptos coin to collateral of your wrapped DA (wNFT).
				</div>
				<div className='c-wrap__form'>
					{
						//getCollateralERC721Block()
					}
					{getCollateralNativeBlock()}
				</div>

				{getCollateralRows()}
			</div>
		);
	};

	/*web3
	const getCollateralBlock = () => {
		if (inputRules.noAddCollateral) {
			return null;
		}

		return (
			<div
				className='c-wrap'
				ref={(e) => {
					scrollable.collaterals = e;
				}}
			>
				<div className='c-wrap__header'>
					<div className='h3'>
						Collateral
						<TippyWrapper
							msg='Assets with which you provide your NFT'
							elClass='ml-1'
						></TippyWrapper>
					</div>
					{getCollateralSlotSelector()}
				</div>
				{getMaxCollateralAlert()}
				<div className='alert mb-4'>
					You can add assets to collateral of your wrapped NFT. Use list of
					approved tokens.
				</div>
				<div className='c-wrap__form'>
					<div className='row row-sm mb-5'>
						<div className='col-12 col-sm-auto mr-3 py-2'>
							<label className='input-label mb-0'>Token Type</label>
						</div>
						<div className='col-auto py-2'>
							<label className='checkbox'>
								<input
									type='radio'
									name='token-type'
									value={_AssetType.native}
									checked={inputCollateralAssetType === _AssetType.native}
									onChange={() => {
										setInputCollateralAssetType(_AssetType.native);
										setInputCollateralAddress('');
										setInputCollateralTokenId('');
										setInputCollateralAmount('');
									}}
								/>
								<span className='check'> </span>
								<span className='check-text'>Native</span>
							</label>
						</div>
						<div className='col-auto py-2'>
							<label className='checkbox'>
								<input
									type='radio'
									name='token-type'
									value={_AssetType.ERC20}
									checked={inputCollateralAssetType === _AssetType.ERC20}
									onChange={() => {
										setInputCollateralAssetType(_AssetType.ERC20);
										setInputCollateralAddress('');
										setInputCollateralTokenId('');
										setInputCollateralAmount('');
									}}
								/>
								<span className='check'> </span>
								<span className='check-text'>
									{currentChain?.EIPPrefix || 'ERC'}-20
								</span>
							</label>
						</div>
						<div className='col-auto py-2'>
							<label className='checkbox'>
								<input
									type='radio'
									name='token-type'
									value={_AssetType.ERC721}
									checked={inputCollateralAssetType === _AssetType.ERC721}
									onChange={() => {
										setInputCollateralAssetType(_AssetType.ERC721);
										setInputCollateralAddress('');
										setInputCollateralTokenId('');
										setInputCollateralAmount('');
									}}
								/>
								<span className='check'> </span>
								<span className='check-text'>
									{currentChain?.EIPPrefix || 'ERC'}-721
								</span>
							</label>
						</div>
						<div className='col-auto py-2'>
							<label className='checkbox'>
								<input
									type='radio'
									name='token-type'
									value={_AssetType.ERC1155}
									checked={inputCollateralAssetType === _AssetType.ERC1155}
									onChange={() => {
										setInputCollateralAssetType(_AssetType.ERC1155);
										setInputCollateralAddress('');
										setInputCollateralTokenId('');
										setInputCollateralAmount('');
									}}
								/>
								<span className='check'> </span>
								<span className='check-text'>
									{currentChain?.EIPPrefix || 'ERC'}-1155
								</span>
							</label>
						</div>
					</div>
					{getCollateralNativeBlock()}
					{getCollateralERC20Block()}
					{getCollateralERC721Block()}
					{getCollateralERC1155Block()}
				</div>

				<div className='text-right'>
					<b>{collaterals.length}</b> /{' '}
					<span className='text-muted'>
						{inputCollateralSlots === undefined
							? maxCollaterals
							: inputCollateralSlots}{' '}
						entries
					</span>
				</div>

				{getCollateralRows()}
			</div>
		);
	};
*/

	// ----- END COLLATERAL -----

	const getFeeBlock = () => {
		if (pageMode === 'simple') {
			return null;
		}
		if (inputRules.noTransfer) {
			return null;
		}

		return (
			<div className='c-wrap'>
				<div className='c-wrap__header'>
					<div className='h3'>
						Transfer Fee
						<TippyWrapper
							msg='Amount of transfer fee. Sender has to pay this fee to transfer wDA (wNFT). So that fee amount must be approved to this contract before any wDA (wNFT) transfer (marketplace trading).'
							elClass='ml-1'
						></TippyWrapper>
					</div>
				</div>
				<div className='c-wrap__form'>
					<div className='row'>
						<div className='col-md-7'>
							<div className='select-group'>
								<input
									className='input-control'
									type='text'
									placeholder='0.000'
									disabled={inputRules.noTransfer}
									value={addThousandSeparator(inputTransferFeeAmount)}
									onChange={(e) => {
										let value = removeThousandSeparator(e.target.value);
										if (
											value !== '' &&
											!value.endsWith('.') &&
											!value.endsWith('0')
										) {
											if (new BigNumber(value).isNaN()) {
												setInputFeeLockChecked(false);
												return;
											}
											value = new BigNumber(value).toString();
											setInputFeeLockChecked(false);
										}

										let feeLockChecked = inputFeeLockChecked;
										if (value === '' || new BigNumber(value).eq(0)) {
											feeLockChecked = false;
										} else {
											if (inputFeeLockAmount !== '') {
												feeLockChecked = true;
											}
										}

										if (inputAddWNFTChecked && value !== '') {
											setTimeout(() => {
												addCollateralERC20Row(true);
											}, 1);
										}

										setInputTransferFeeAmount(value);
										setInputFeeLockChecked(feeLockChecked);
									}}
									onBlur={(e) => {
										if (e.target.value === '') {
											setCollaterals(
												collaterals.filter((item: CollateralItem) => {
													return !item.amount || !item.amount.eq(0);
												}),
											);
										} else {
											if (inputAddWNFTChecked) {
												addCollateralERC20Row(true);
											}
										}
									}}
								/>
								<CoinSelector
									tokens={filterERC20ContractByPermissions({
										enabledForFee: true,
									})}
									selectedToken={inputTransferFeeAddress}
									onChange={(address: string) => {
										if (inputAddWNFTChecked && inputTransferFeeAmount !== '') {
											setTimeout(() => {
												addCollateralERC20Row(true);
											}, 1);
										}

										setInputTransferFeeAddress(address);
									}}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	};

	const recalcPercents = (recs: Array<RoyaltyInput>) => {
		let sum = 10000;
		const recLength = recs.length;
		return recs.map((item: RoyaltyInput): RoyaltyInput => {
			let percentToAdd;
			if (sum > Math.ceil(10000 / recLength) + recLength) {
				percentToAdd = new BigNumber(Math.floor(10000 / recLength)).dividedBy(
					100,
				);
				sum = sum - Math.floor(10000 / recLength);
			} else {
				percentToAdd = new BigNumber(sum).dividedBy(100);
				sum = 0;
			}
			return {
				...item,
				percent: percentToAdd.toFixed(2),
			};
		});
	};
	const addRecipient = (address: string | undefined) => {
		const foundRecipient = recipients.filter((item) => {
			if (!item.address && !address) {
				return true;
			}
			if (!item.address) {
				return false;
			}
			if (!address) {
				return false;
			}
			return item.address.toLowerCase() === address.toLowerCase();
		});
		if (foundRecipient.length) {
			return;
		}

		let collateralsUpdated = collaterals;
		if (!address) {
			const foundFeeToken = collateralsUpdated.find((item: CollateralItem) => {
				return (
					item.contractAddress.toLowerCase() ===
					inputTransferFeeAddress.toLowerCase()
				);
			});
			if (!foundFeeToken) {
				collateralsUpdated = [
					...collateralsUpdated,
					{
						assetType: _AssetType.ERC20,
						contractAddress: inputTransferFeeAddress,
						amount: new BigNumber(0),
					},
				];
			}
		}

		setRecipients([
			...recipients,
			{
				address: address,
				percent: '0',
				timeAdded: new Date().getTime(),
			},
		]);
		setCollaterals(collateralsUpdated);
		setInputRecipientAddress('');
		setShowErrors(
			showErrors.filter((item) => {
				return item !== 'royalty';
			}),
		);
	};
	const removeRecipient = (address: string | undefined) => {
		let recipientsUpdated: Array<RoyaltyInput>;
		let theWNFTChecked = inputAddWNFTChecked;
		if (address) {
			recipientsUpdated = recipients.filter((item) => {
				if (!item.address) {
					return true;
				}
				return item.address.toLowerCase() !== address.toLowerCase();
			});
		} else {
			recipientsUpdated = recipients.filter((item) => {
				return !!item.address;
			});
			theWNFTChecked = false;
		}

		let collateralsUpdated = collaterals;
		if (!address) {
			collateralsUpdated = collateralsUpdated.filter((item: CollateralItem) => {
				return !item.amount || !item.amount.eq(0);
			});
		}

		setRecipients(recipientsUpdated);
		setInputRecipientAddress('');
		setInputAddWNFTChecked(theWNFTChecked);
		setCollaterals(collateralsUpdated);
	};
	const getAddRecipientBtn = () => {
		return (
			<button
				className='btn btn-grad'
				type='submit'
				disabled={isAddRecipientDisabled()}
				onClick={() => {
					addRecipient(inputRecipientAddress);
				}}
			>
				Add
			</button>
		);
	};
	const isAddRecipientDisabled = () => {
		if (inputRecipientAddress === '') {
			return true;
		}
		if (!isAptosAddress(inputRecipientAddress)) {
			return true;
		}
		return false;
	};
	const getRecipientCalcedRoyalty = (item: RoyaltyInput) => {
		if (inputTransferFeeAmount === '') {
			return '—';
		}

		const fee = new BigNumber(inputTransferFeeAmount);
		if (!fee || fee.isNaN()) {
			return '—';
		}

		const percent = new BigNumber(item.percent).dividedBy(100);

		return fee.multipliedBy(percent).toString();
	};
	const getRecipientRow = (item: RoyaltyInput, idx: number) => {
		return (
			<div key={item.address || 'none'} className='item'>
				<div className='row'>
					<div className='mb-2 col-12 col-md-1'>#{idx}</div>
					<div className='mb-3 mb-md-2 col-12 col-md-4'>
						<span className='col-legend'>Token: </span>
						<span className='text-break'>
							{item.address ? compactString(item.address) : 'the wNFT'}
						</span>
					</div>
					<div className='mb-2 col-12 col-md-2'>
						<div className='tb-input tb-percent'>
							<input
								className='input-control'
								type='text'
								value={item.percent.toString()}
								onFocus={(e) => {
									e.target.select();
								}}
								onChange={(e) => {
									let value = e.target.value
										.replaceAll(' ', '')
										.replaceAll(',', '.');
									const valueParsed = new BigNumber(value);

									if (value === '') {
										value = '';
									} else {
										if (valueParsed.isNaN()) {
											return;
										}
										if (valueParsed.gt(100)) {
											return;
										}
									}

									// the wNFT
									if (!item.address) {
										setRecipients([
											...recipients.filter((iitem) => {
												return !!iitem.address;
											}),
											{
												address: undefined,
												percent: value,
											},
										]);
										setShowErrors(
											showErrors.filter((item) => {
												return item !== 'royalty';
											}),
										);
										return;
									}

									// plain address
									const foundRecipient = recipients.filter((iitem) => {
										if (!iitem.address) {
											return false;
										}
										if (!item.address) {
											return false;
										}
										return (
											iitem.address.toLowerCase() === item.address.toLowerCase()
										);
									});
									if (foundRecipient.length) {
										setRecipients([
											...recipients.filter((iitem) => {
												if (!iitem.address) {
													return true;
												}
												if (!item.address) {
													return true;
												}
												return (
													iitem.address.toLowerCase() !==
													item.address.toLowerCase()
												);
											}),
											{
												address: item.address,
												percent: value,
												timeAdded: foundRecipient[0].timeAdded,
											},
										]);
										setShowErrors(
											showErrors.filter((item) => {
												return item !== 'royalty';
											}),
										);
									}
								}}
								onBlur={() => {
									setRecipients(
										recipients.map((item) => {
											return {
												...item,
												percent: new BigNumber(item.percent).toFixed(
													2,
													BigNumber.ROUND_DOWN,
												),
											};
										}),
									);
								}}
							/>
							<TippyWrapper
								msg='Percent of royalty from the transfer fee amount'
								elClass='ml-1'
							></TippyWrapper>
						</div>
					</div>
					<div className='mb-2 col-12 col-md-4'>
						<span className='col-legend'>Amount: </span>
						<span className='text-break'>
							{getRecipientCalcedRoyalty(item)}
						</span>
					</div>
					{item.address ? (
						<button
							className='btn-del'
							onClick={() => {
								removeRecipient(item.address);
							}}
						>
							<img src={icon_i_del} alt='' />
						</button>
					) : null}
				</div>
			</div>
		);
	};
	const getDistributionAlert = () => {
		const showError = showErrors.find((item) => {
			return item === 'royalty';
		});
		if (!showError) {
			return null;
		}

		if (inputTransferFeeAmount !== '') {
			const sumRoyaltyPercent = recipients.reduce((acc, item) => {
				const percentToAdd = new BigNumber(item.percent);
				if (percentToAdd.isNaN()) {
					return acc;
				}
				return acc.plus(percentToAdd);
			}, new BigNumber(0));
			if (!sumRoyaltyPercent.eq(100)) {
				return (
					<div className='alert alert-error mt-3'>
						Royalty percent is not equal 100
					</div>
				);
			}
		}

		return null;
	};
	const getRoyaltyBlock = () => {
		if (pageMode === 'simple') {
			return null;
		}
		if (inputRules.noTransfer) {
			return null;
		}

		const showError = showErrors.find((item) => {
			return item === 'emptyRecipients';
		});

		return (
			<div className='c-wrap'>
				<div className='c-wrap__header d-block'>
					<div className='row align-items-center'>
						<div className='col-12 col-md-auto'>
							<div
								className='h3 mt-0'
								ref={(e) => {
									scrollable.royalty = e;
								}}
							>
								Royalty Recipients
							</div>
						</div>
						<div className='col-12 col-md-auto'>
							<label className='checkbox'>
								<input
									type='checkbox'
									disabled={
										inputTransferFeeAmount === '' ||
										inputTransferFeeAddress === '' ||
										inputRules.noTransfer ||
										inputRules.noAddCollateral
									}
									checked={inputAddWNFTChecked}
									onChange={(e) => {
										if (e.target.checked) {
											setInputAddWNFTChecked(e.target.checked);
											setShowErrors(
												showErrors.filter((item) => {
													return item !== 'emptyRecipients';
												}),
											);
											setTimeout(() => {
												addRecipient(undefined);
											}, 1);
										} else {
											setInputAddWNFTChecked(e.target.checked);
											setShowErrors(
												showErrors.filter((item) => {
													return item !== 'emptyRecipients';
												}),
											);
											setInputFeeLockChecked(false);
											setTimeout(() => {
												removeRecipient(undefined);
											}, 1);
										}
									}}
								/>
								<span className='check'> </span>
								<span className='check-text'>
									Add the wDA (wNFT) to the recipents’s list
								</span>
							</label>
						</div>
					</div>
				</div>
				<div className='c-wrap__form'>
					<div className='row mb-1'>
						<div className='col col-12 col-md-6'>
							<label className='input-label'>
								Address
								<TippyWrapper msg='Address of royalty income reciever'></TippyWrapper>
							</label>
							<input
								className={`input-control ${showError ? 'has-error' : ''}`}
								type='text'
								placeholder='Paste here'
								value={inputRecipientAddress}
								onChange={(e) => {
									setInputRecipientAddress(
										e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ''),
									);
									setShowErrors(
										showErrors.filter((item) => {
											return item !== 'emptyRecipients';
										}),
									);
								}}
								onKeyPress={(e) => {
									if (e.defaultPrevented) {
										return;
									}
									if (!!isAddRecipientDisabled()) {
										return;
									}
									if (e.key !== 'Enter') {
										return;
									}

									addRecipient(inputRecipientAddress);
								}}
							/>
						</div>
						<div className='col col-12 col-md-2'>
							<label className='input-label'>&nbsp;</label>
							{getAddRecipientBtn()}
						</div>
					</div>

					{recipients.length ? (
						<div className='row'>
							<div className='col-sm-6 col-md-3'>
								<button
									className='btn btn-sm btn-gray'
									onClick={() => {
										setRecipients(recalcPercents(recipients));
										setShowErrors(
											showErrors.filter((item) => {
												return item !== 'emptyRecipients';
											}),
										);
									}}
								>
									Divide percents evenly
								</button>
							</div>
						</div>
					) : null}

					{getDistributionAlert()}
				</div>
				<div className='c-wrap__table mt-3'>
					{recipients
						.sort((item, prev) => {
							if (!item.address) {
								return -1;
							}
							if (!prev.address) {
								return 1;
							}

							// if ( item.address < prev.address ) { return -1 }
							// if ( item.address > prev.address ) { return  1 }

							if (!item.timeAdded) {
								return -1;
							}
							if (!prev.timeAdded) {
								return 1;
							}
							if (item.timeAdded < prev.timeAdded) {
								return -1;
							}
							if (item.timeAdded > prev.timeAdded) {
								return 1;
							}

							return 0;
						})
						.map((item, idx) => {
							return getRecipientRow(item, idx + 1);
						})}
				</div>
			</div>
		);
	};

	const getFeeLockTokenIcon = () => {
		let icon = default_icon;

		const foundToken = erc20List.filter((item) => {
			if (!item.contractAddress) {
				return false;
			}
			return (
				inputTransferFeeAddress.toLowerCase() ===
				item.contractAddress.toLowerCase()
			);
		});
		if (foundToken.length && foundToken[0].icon) {
			icon = foundToken[0].icon;
		}

		return (
			<span className='i-coin'>
				<img src={icon} alt='' />
			</span>
		);
	};
	const getFeeUnlockInput = () => {
		if (!inputFeeLockChecked) {
			return null;
		}

		const feeShowError = showErrors.find((item) => {
			return item === 'feeUnlock';
		});

		if (feeShowError) {
			return (
				<div className='checkbox__extra'>
					<div className='coin-group'>
						{getFeeLockTokenIcon()}
						<input
							className='input-control has-error'
							type='text'
							placeholder='0'
							ref={focusableFees}
							value={addThousandSeparator(inputFeeLockAmount)}
							onChange={(e) => {
								let value = removeThousandSeparator(e.target.value);
								if (
									value !== '' &&
									!value.endsWith('.') &&
									!value.endsWith('0')
								) {
									if (new BigNumber(value).isNaN()) {
										return;
									}
									value = new BigNumber(value).toString();
								}
								setInputFeeLockAmount(value);
								setShowErrors(
									showErrors.filter((item) => {
										return item !== 'feeUnlock';
									}),
								);
							}}
						/>
					</div>
				</div>
			);
		}

		if (
			inputFeeLockChecked &&
			inputFeeLockAmount !== '' &&
			collaterals.length
		) {
			const foundCollateral = collaterals.find((item) => {
				return (
					item.contractAddress.toLowerCase() ===
					inputTransferFeeAddress.toLowerCase()
				);
			});
			if (foundCollateral && foundCollateral.amount) {
				if (foundCollateral.amount.gt(new BigNumber(inputFeeLockAmount))) {
					return (
						<div className='checkbox__extra'>
							<div className='coin-group'>
								{getFeeLockTokenIcon()}
								<input
									className='input-control has-error'
									type='text'
									placeholder='0'
									ref={focusableFees}
									value={addThousandSeparator(inputFeeLockAmount)}
									onChange={(e) => {
										let value = removeThousandSeparator(e.target.value);
										if (
											value !== '' &&
											!value.endsWith('.') &&
											!value.endsWith('0')
										) {
											if (new BigNumber(value).isNaN()) {
												return;
											}
											value = new BigNumber(value).toString();
										}
										setInputFeeLockAmount(value);
										setShowErrors(
											showErrors.filter((item) => {
												return item !== 'feeUnlock';
											}),
										);
									}}
								/>
								<div className='input-error'>Collateral exeeds lock</div>
							</div>
						</div>
					);
				}
			}
		}

		return (
			<div className='checkbox__extra'>
				<div className='coin-group'>
					{getFeeLockTokenIcon()}
					<input
						className='input-control'
						type='text'
						placeholder='0'
						ref={focusableFees}
						value={addThousandSeparator(inputFeeLockAmount)}
						onChange={(e) => {
							let value = removeThousandSeparator(e.target.value);
							if (
								value !== '' &&
								!value.endsWith('.') &&
								!value.endsWith('0')
							) {
								if (new BigNumber(value).isNaN()) {
									return;
								}
								value = new BigNumber(value).toString();
							}
							setInputFeeLockAmount(value);
							setShowErrors(
								showErrors.filter((item) => {
									return item !== 'feeUnlock';
								}),
							);
						}}
					/>
				</div>
			</div>
		);
	};
	const getFeeUnlockBlock = () => {
		if (pageMode === 'simple') {
			return null;
		}
		if (inputRules.noTransfer) {
			return null;
		}

		return (
			<div className='col-12 col-sm-auto mt-4'>
				<label className='checkbox'>
					<input
						type='checkbox'
						checked={inputFeeLockChecked}
						disabled={
							inputTransferFeeAmount === '' ||
							new BigNumber(inputTransferFeeAmount).isNaN() ||
							new BigNumber(inputTransferFeeAmount).eq(0) ||
							inputRules.noUnwrap ||
							!inputAddWNFTChecked
						}
						onChange={(e) => {
							if (
								inputTransferFeeAmount === '' ||
								new BigNumber(inputTransferFeeAmount).isNaN() ||
								new BigNumber(inputTransferFeeAmount).eq(0)
							) {
								return;
							}
							setInputFeeLockChecked(e.target.checked);
							setShowErrors(
								showErrors.filter((item) => {
									return item !== 'feeUnlock';
								}),
							);
							if (e.target.checked) {
								setTimeout(() => {
									focusableFees.current && focusableFees.current.focus();
								}, 1);
							}
						}}
					/>
					<span className='check'> </span>
					<span className='check-text'>FeeLock</span>
					<TippyWrapper msg='Unwrap will be available only after accumulated transfer fee will achieve this amount'></TippyWrapper>
				</label>
				{getFeeUnlockInput()}
			</div>
		);
	};
	const getDateWithAddedDays = () => {
		const days = !isNaN(parseInt(inputTimeLockDays))
			? parseInt(inputTimeLockDays)
			: 0;
		return dateToStrWithMonth(addDays(days));
	};
	const getUnlockBlock = () => {
		if (inputRules.noUnwrap) {
			return null;
		}

		const timeShowError = showErrors.find((item) => {
			return item === 'timeUnlock';
		});
		return (
			<div
				className='c-wrap wrap__settings'
				ref={(e) => {
					scrollable.locks = e;
				}}
			>
				<div>
					<b>Unlock Settings</b>
				</div>
				<div className='row'>
					<div className='col-12 col-sm-auto mt-4'>
						<label className='checkbox'>
							<input
								type='checkbox'
								checked={inputTimeLockChecked}
								disabled={inputRules.noUnwrap}
								onChange={(e) => {
									setInputTimeLockChecked(e.target.checked);
									setShowErrors(
										showErrors.filter((item) => {
											return item !== 'timeUnlock';
										}),
									);
									if (e.target.checked) {
										setTimeout(() => {
											focusableTime.current && focusableTime.current.focus();
										}, 1);
									}
								}}
							/>
							<span className='check'> </span>
							<span className='check-text'>TimeLock (days)</span>
							<TippyWrapper msg='Unwrap will be available only after this date'></TippyWrapper>
						</label>

						{inputTimeLockChecked ? (
							<div className='checkbox__extra'>
								<input
									className={`input-control ${
										timeShowError ? 'has-error' : ''
									}`}
									type='text'
									placeholder='0'
									ref={focusableTime}
									value={inputTimeLockDays}
									onChange={(e) => {
										let value = e.target.value.replaceAll(' ', '');

										if (value === '' || isNaN(parseInt(value))) {
											setInputTimeLockDays('');
										} else {
											if (parseInt(value) > 9999999) {
												return;
											}

											setInputTimeLockDays(`${parseInt(value)}`);
											setShowErrors(
												showErrors.filter((item) => {
													return item !== 'timeUnlock';
												}),
											);
										}
									}}
								/>
								<div className='text-muted mt-2'>{getDateWithAddedDays()}</div>
							</div>
						) : null}
					</div>

					{
						//getFeeUnlockBlock()
					}
				</div>
			</div>
		);
	};

	const hasErrors = () => {
		const errors: Array<{
			msg: string;
			block: HTMLElement;
			showError?: string;
		}> = [];
		const foundSaved = originalTokenCache.find((item) => {
			return (
				item.contractAddress.toLowerCase() ===
					inputOriginalTokenAddress.toLowerCase() &&
				item.tokenId.toLowerCase() === inputOriginalTokenId.toLowerCase()
			);
		});

		// ----- ORIGINAL TOKEN -----
		if (
			wrapWith === 'own' &&
			(inputOriginalTokenAddress === '' || inputOriginalTokenId === '')
		) {
			errors.push({
				msg: 'Enter original token info or set empty',
				block: scrollable.originalToken,
			});
		}

		
		if (
			foundSaved &&
			foundSaved.savedToken.owner &&
			userAddress &&
			foundSaved.savedToken.owner.toLowerCase() !== userAddress.toLowerCase()
		) 
/*		if (
			//web3			wrapWith !== 'empty' &&
			foundSaved?.savedToken.owner &&
			userAddress &&
			foundSaved?.savedToken.owner.toLowerCase() !== userAddress.toLowerCase()
		) 
*/		{
			errors.push({
				msg: 'Not yours original token',
				block: scrollable.originalToken,
			});
	
		}

		if (originalTokensBlacklist) {
			const foundBlacklistItem = originalTokensBlacklist.find((item: any) => {
				if (typeof item === 'string') {
					return item.toLowerCase() === inputOriginalTokenAddress.toLowerCase();
				}
				return (
					item.contractAddress.toLowerCase() ===
					inputOriginalTokenAddress.toLowerCase()
				);
			});
			if (foundBlacklistItem) {
				errors.push({
					msg: 'Original token address is blacklisted',
					block: scrollable.originalToken,
				});
			}
		}

		if (inputOriginalCopies === '') {
			if (foundSaved?.savedToken.originalTokenStandart === _AssetType.ERC1155) {
				errors.push({
					msg: 'Enter original token amount',
					block: scrollable.originalToken,
				});
			}
		} else {
			if (
				//web3				wrapWith !== 'empty' &&
				foundSaved?.savedToken.balance1155 &&
				foundSaved?.savedToken.balance1155.lt(
					new BigNumber(inputOriginalCopies),
				)
			) {
				errors.push({
					msg: 'Not enough original token balance',
					block: scrollable.originalToken,
				});
			}
		}

		if (inputOutAssetType === _AssetType.ERC1155 && inputCopies === 0) {
			errors.push({
				msg: 'Enter wnft token amount',
				block: scrollable.standart,
				showError: 'standart',
			});
		}

		if (
			//web3			wrapWith !== 'empty' &&
			inputOriginalTokenAddress !== '' &&
			!isAptosAddress(inputOriginalTokenAddress)
		) {
			errors.push({
				msg: 'Bad address format',
				block: scrollable.originalToken,
			});
		}
		if (
			inputWNFTRecipientAddress !== '' &&
			!isAptosAddress(inputWNFTRecipientAddress)
		) {
			errors.push({
				msg: 'Bad wDA (wNFT) recipient address format',
				block: scrollable.advancedOptions,
			});
		}

		if ( addressIsStorage(inputOriginalTokenAddress) ) {
			errors.push({
				msg: 'Cannot wrap wrapped',
			block: scrollable.originalToken,
			});
		}
		// ----- END ORIGINAL TOKEN -----

		// ----- COLLATERALS -----'
		if (inputRules.noAddCollateral && collaterals.length) {
			errors.push({
				msg: 'Cannot add collaterals due to rule',
				block: scrollable.collaterals,
			});
		}
		if (overMaxCollaterals()) {
			errors.push({
				msg: 'Over max collaterals',
				block: scrollable.collaterals,
			});
		}
		// ----- END COLLATERALS -----

		// ----- ROYALTIES -----
		if (!inputRules.noTransfer) {
			if (inputTransferFeeAmount !== '' && !recipients.length) {
				errors.push({
					msg: 'Empty recipients',
					block: scrollable.royalty,
					showError: 'emptyRecipients',
				});
			} else {
				if (inputTransferFeeAmount !== '') {
					const sumRoyaltyPercent = recipients.reduce((acc, item) => {
						const percentToAdd = new BigNumber(item.percent);
						if (percentToAdd.isNaN()) {
							return acc;
						}
						return acc.plus(percentToAdd);
					}, new BigNumber(0));
					if (!sumRoyaltyPercent.eq(100)) {
						errors.push({
							msg: 'Royalty percent is not equal 100',
							block: scrollable.royalty,
							showError: 'royalty',
						});
					}
				}
			}
		}
		// ----- END ROYALTIES -----

		// ----- LOCKS -----
		if (inputFeeLockChecked) {
			const feeAmount = new BigNumber(inputFeeLockAmount);
			if (feeAmount.isNaN() || feeAmount.eq(0)) {
				errors.push({
					msg: 'Empty fee unlock',
					block: scrollable.locks,
					showError: 'feeUnlock',
				});
			}
		}
		if (inputFeeLockChecked && !inputAddWNFTChecked) {
			errors.push({
				msg: 'No the wDA (wNFT) in recipients list with fee lock',
				block: scrollable.royalty,
			});
		}

		if (
			inputFeeLockChecked &&
			inputFeeLockAmount !== '' &&
			collaterals.length
		) {
			const foundCollateral = collaterals.find((item) => {
				return (
					item.contractAddress.toLowerCase() ===
					inputTransferFeeAddress.toLowerCase()
				);
			});
			if (foundCollateral && foundCollateral.amount) {
				if (foundCollateral.amount.gt(new BigNumber(inputFeeLockAmount))) {
					errors.push({
						msg: 'Collateral exeeds lock',
						block: scrollable.locks,
					});
				}
			}
		}

		if (inputTimeLockChecked) {
			const days = new BigNumber(inputTimeLockDays);
			if (days.isNaN() || days.eq(0)) {
				errors.push({
					msg: 'Empty time unlock',
					block: scrollable.locks,
					showError: 'timeUnlock',
				});
			}
		}

		// ----- END LOCKS -----

		// if (
		// 	wrapWith !== 'empty' &&
		// 	originalTokenWrapped &&
		// 	originalTokenWrapped.rules.noWrap
		// ) { return t('No wrap') }

		// TODO

		// if (
		// 	metamaskAdapter.wrapperContract &&
		// 	wrapWith !== 'empty' &&
		// 	store.getState().wNFTStorages.storagesCount === store.getState().wNFTStorages.list.length &&
		// 	metamaskAdapter.wrapperContract.addressIsStorage(inputOriginalTokenAddress)
		// ) {
		// 	errors.push({ msg: 'Cannot wrap wrapped', block: scrollable.originalToken })
		// }

		collateralErrors.forEach((item) => {
			errors.push({
				msg: `Error with collateral: ${compactString(item.contractAddress)} - ${
					item.tokenId || ''
				}. ${item.msg}`,
				block: scrollable.collaterals,
			});
		});

		
		
		return errors;
	};
	const getErrorsBlock = () => {
		const errors = hasErrors();

		if (!errors.length) {
			return null;
		}

		return (
			<div className='c-errors'>
				<div className='mb-3'>
					To enable the Wrap button, correct the following errors:
				</div>
				<ul>
					{errors.map((item) => {
						return (
							<li key={item.msg}>
								<button
									className='btn-link'
									onClick={async () => {
										item.block.scrollIntoView();
										if (item.showError) {
											setShowErrors([
												...showErrors.filter((iitem) => {
													return iitem !== item.showError;
												}),
												item.showError,
											]);
										}
									}}
								>
									{item.msg}
								</button>
							</li>
						);
					})}
				</ul>
			</div>
		);
	};

	const getWrapParamUnwrapDestination = (_userAddress: string) => {
		if (inputCollateralRecipientAddress !== '') {
			return inputCollateralRecipientAddress;
		}
		return _userAddress;
	};
	const getWrapParamWrapFor = (_userAddress: string) => {
		if (inputWNFTRecipientAddress !== '') {
			return inputWNFTRecipientAddress;
		}
		return _userAddress;
	};
	const getWrapParamOutAmount = () => {
		if (inputOutAssetType === _AssetType.ERC721) {
			return 0;
		}
		return inputCopies || 1;
	};
	const getWrapParamLocks = (): Array<Lock> => {
		const locks: Array<Lock> = [];

		if (!inputRules.noUnwrap && inputTimeLockChecked) {
			const days = parseInt(inputTimeLockDays);
			const unlockDate = addDays(days);
			locks.push({
				lockType: LockType.time,
				param: dateToUnixtime(unlockDate), // TODO
			});
		}

		if (!inputRules.noUnwrap && inputFeeLockChecked) {
			let foundToken = erc20List.find((item) => {
				if (!item.contractAddress) {
					return false;
				}
				return (
					item.contractAddress.toLowerCase() ===
					inputTransferFeeAddress.toLowerCase()
				);
			});
			if (!foundToken) {
				foundToken = getNullERC20(inputTransferFeeAddress);
			}

			const amount = tokenToInt(
				new BigNumber(inputFeeLockAmount),
				foundToken.decimals || 8,
			);
			locks.push({
				lockType: LockType.value,
				param: amount,
			});
		}

		if (inputCollateralSlots) {
			const amount = new BigNumber(inputCollateralSlots);
			locks.push({
				lockType: LockType.slots,
				param: amount,
			});
		}

		return locks;
	};

	const getWrapParamTimeLock = (): number => {
		let timeLock=0;
		if (!inputRules.noUnwrap && inputTimeLockChecked) {
			const days = parseInt(inputTimeLockDays);
			const unlockDate = addDays(days);
			timeLock= dateToUnixtimeNumber(unlockDate); // TODO

		}

		return timeLock;
	};

	const getWrapParamFees = (): Array<Fee> => {
		if (inputRules.noTransfer) {
			return [];
		}

		let foundToken = erc20List.find((item) => {
			if (!item.contractAddress) {
				return false;
			}
			return (
				item.contractAddress.toLowerCase() ===
				inputTransferFeeAddress.toLowerCase()
			);
		});
		if (!foundToken) {
			foundToken = getNullERC20(inputTransferFeeAddress);
		}

		const amount = tokenToInt(
			new BigNumber(inputTransferFeeAmount),
			foundToken.decimals || 8,
		);

		const output: Array<Fee> = [];
		if (inputTransferFeeAmount !== '') {
			output.push({
				token: inputTransferFeeAddress,
				value: amount,
			});
		}
		return output;
	};
	const getWrapParamRoyalty = () => {
		let royaltiesParsed: Array<Royalty> = [];
		if (!inputRules.noTransfer) {
			royaltiesParsed = recipients.map((item: RoyaltyInput): Royalty => {
				return {
					address: item.address,
					percent: new BigNumber(item.percent),
				};
			});
		}

		return royaltiesParsed;
	};
	const getWrapParamCollateral = () => {
		const collateralsSorted = collaterals.sort((item, prev) => {
			if (
				inputTransferFeeAmount !== '' &&
				!new BigNumber(inputTransferFeeAmount).isNaN() &&
				!new BigNumber(inputTransferFeeAmount).eq(0) &&
				inputAddWNFTChecked
			) {
				if (
					item.contractAddress.toLowerCase() ===
					inputTransferFeeAddress.toLowerCase()
				) {
					return -1;
				} else {
					return 1;
				}
			}

			return item.assetType - prev.assetType;
		});

		return collateralsSorted;
	};

	const createAdvancedLoaderWrap = (
		_currentChain: ChainType,
//		_web3: Web3,
		_userAddress: string,
	) => {
		const loaderStages: Array<AdvancedLoaderStageType> = [];
		if (wrapWith === 'predefined') {
			loaderStages.push({
				id: 'mint',
				sortOrder: 1,
				text: `Minting token for wrap`,
				status: _AdvancedLoadingStatus.queued,
			});
			loaderStages.push({
				id: 'fetchminted',
				sortOrder: 2,
				text: `Waiting for minted token`,
				status: _AdvancedLoadingStatus.queued,
			});
			loaderStages.push({
				id: 'approveminted',
				sortOrder: 3,
				text: `Approving token to wrap`,
				status: _AdvancedLoadingStatus.queued,
			});
		}

		if (wrapWith === 'own') {
			loaderStages.push({
				id: 'approve_original',
				sortOrder: 1,
				text: `Approving token to wrap`,
				status: _AdvancedLoadingStatus.queued,
			});
		}

		const qtyERC20Collaterals = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC20;
		});
		if (qtyERC20Collaterals.length) {
			loaderStages.push({
				id: 'approve20collateral',
				sortOrder: 10,
				text: `Approving ${_currentChain.EIPPrefix}-20 collateral tokens`,
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyERC20Collaterals.length,
			});
		}
		const qtyERC721Collaterals = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC721;
		});
		if (qtyERC721Collaterals.length) {
			loaderStages.push({
				id: 'approve721collateral',
				sortOrder: 11,
				text: `Approving ${_currentChain.EIPPrefix}-721 collateral tokens`,
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyERC721Collaterals.length,
			});
		}
		const qtyERC1155Collaterals = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC1155;
		});
		if (qtyERC1155Collaterals.length) {
			loaderStages.push({
				id: 'approve1155collateral',
				sortOrder: 12,
				text: `Approving ${_currentChain.EIPPrefix}-1155 collateral tokens`,
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyERC1155Collaterals.length,
			});
		}

		const qtyWNFTCollateralFees = collaterals.filter((item) => {
			return addressIsStorage(item.contractAddress);
		});
		if (qtyWNFTCollateralFees.length) {
			loaderStages.push({
				id: 'approvewnftcollateralfees',
				sortOrder: 15,
				text: 'Approving wDA (wNFT) collateral fees',
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyWNFTCollateralFees.length,
			});
		}

		loaderStages.push({
			id: 'wrap',
			sortOrder: 20,
			text: 'Wrapping token',
			status: _AdvancedLoadingStatus.queued,
		});

		const advLoader = {
			title: 'Waiting for create',
			stages: loaderStages,
		};
		createAdvancedLoader(advLoader);
	};

	const getWrapParamToken = async (
		_currentChain: ChainType,
//		_web3: Web3,
		_userAddress: string,
		isMultisig?: boolean,
	) => {
		let tokenToWrap;
		/*web3
		if (wrapWith === 'empty') {
			tokenToWrap = {
				assetType: _AssetType.empty,
				contractAddress: '0x0000000000000000000000000000000000000000',
				tokenId: '0',
				amount: new BigNumber(0),
			};
		}
*/
		// TODO: minted event
		if (wrapWith === 'predefined') {
			updateStepAdvancedLoader({
				id: 'mint',
				status: _AdvancedLoadingStatus.loading,
			});

			let minted: any;
			try {
				minted = await mintDA(//_web3,
					 minterContract, _userAddress,_currentChain.chainId, signAndSubmitTransaction);
			} catch (e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot mint token`,
					details: [
						`Contract address: ${minterContract}`,
						`User address: ${_userAddress}`,
						'',
						e.message || e,
					],
				});
				throw new Error();
			}

			updateStepAdvancedLoader({
				id: 'mint',
				status: _AdvancedLoadingStatus.complete,
			});

			let mintedTokenId: string | undefined;
			try {
				mintedTokenId = minted.events.Transfer.returnValues.tokenId;
			} catch (e: any) {}
			if (!mintedTokenId) {
				setModal({
					type: _ModalTypes.error,
					title: 'Cannot fetch minted token',
					links: [{ text: 'Mint tx', url: minted.transactionHash }],
				});
				throw new Error();
			}
			updateStepAdvancedLoader({
				id: 'fetchminted',
				text: `Waiting for minted token: ${mintedTokenId}`,
				status: _AdvancedLoadingStatus.loading,
			});

			try {
				await waitUntilAsync(
					async () => {
						let mintedToken;
						try {
							mintedToken = await getERC721ByIdFromChain(
								_currentChain.chainId,
								minterContract,
								mintedTokenId || '',
							);
						} catch (ignored) {}
						if (mintedToken !== undefined) {
							return true;
						}
						return false;
					},
					3 * 1000,
					10,
				); // TODO
			} catch (e: any) {
				setModal({
					type: _ModalTypes.error,
					title: 'Cannot fetch minted token',
					text: [{ text: 'You can use it manually later' }],
					copyables: [
						{ title: 'Contract address', content: minterContract },
						{ title: 'Token Id', content: `${mintedTokenId}` },
					],
				});
				throw new Error();
			}

			updateStepAdvancedLoader({
				id: 'fetchminted',
				status: _AdvancedLoadingStatus.complete,
			});

			updateStepAdvancedLoader({
				id: 'approveminted',
				text: `Approving token to wrap: ${mintedTokenId}`,
				status: _AdvancedLoadingStatus.loading,
			});
/*
			const isApproved = await checkApprovalERC721(
				_currentChain.chainId,
				minterContract,
				mintedTokenId,
				_userAddress,
				wrapperContract,
			);
			if (!isApproved) {
				try {
					await setApprovalForAllERC721(
						_web3,
						minterContract,
						_userAddress,
						wrapperContract,
					);
				} catch (e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${_currentChain.EIPPrefix}-721 token`,
						details: [
							`Contract address: ${minterContract}`,
							`Token id: all`,
							`User address: ${_userAddress}`,
							'',
							e.message || e,
						],
					});
					throw new Error();
				}
			}
*/
			updateStepAdvancedLoader({
				id: 'approveminted',
				status: _AdvancedLoadingStatus.complete,
			});

			tokenToWrap = {
				assetType: _AssetType.ERC721,
				contractAddress: minterContract,
				tokenId: mintedTokenId,
				amount: new BigNumber(0),
			};
		}

		if (wrapWith === 'own') {
			const foundSaved = originalTokenCache.find((item) => {
				return (
					item.contractAddress.toLowerCase() ===
						inputOriginalTokenAddress.toLowerCase() &&
					item.tokenId.toLowerCase() === inputOriginalTokenId.toLowerCase()
				);
			});

			let amount = new BigNumber(0);
			if (foundSaved?.savedToken.originalTokenStandart === _AssetType.ERC1155) {
				if (inputOriginalCopies !== '') {
					amount = new BigNumber(inputOriginalCopies);
				} else {
					amount = new BigNumber(2);
				}
			}
			tokenToWrap = {
				assetType:
					foundSaved?.savedToken.originalTokenStandart || _AssetType.ERC721,
				contractAddress: inputOriginalTokenAddress,
				tokenId: inputOriginalTokenId,
				amount: amount,
			};

			updateStepAdvancedLoader({
				id: 'approve_original',
				status: _AdvancedLoadingStatus.loading,
			});
/*
			if (foundSaved?.savedToken.originalTokenStandart === _AssetType.ERC1155) {
				// ERC-1155
				const isApproved = await checkApprovalERC1155(
					_currentChain.chainId,
					inputOriginalTokenAddress,
					_userAddress,
					wrapperContract,
				);
				if (!isApproved) {
					try {
						await setApprovalERC1155(
							_web3,
							inputOriginalTokenAddress,
							_userAddress,
							wrapperContract,
						);
					} catch (e: any) {
						setModal({
							type: _ModalTypes.error,
							title: `Cannot approve all of ${_currentChain.EIPPrefix}-1155 tokens`,
							details: [
								`Contract address: ${inputOriginalTokenAddress}`,
								`User address: ${_userAddress}`,
								'',
								e.message || e,
							],
						});
						throw new Error();
					}
				}
			} else {
				if (inputApproveAll) {
					// ERC-721 all
					const isApproved = await checkApprovalForAllERC721(
						_currentChain.chainId,
						inputOriginalTokenAddress,
						_userAddress,
						wrapperContract,
					);
					if (!isApproved) {
						try {
							await setApprovalForAllERC721(
								_web3,
								inputOriginalTokenAddress,
								_userAddress,
								wrapperContract,
							);
						} catch (e: any) {
							setModal({
								type: _ModalTypes.error,
								title: `Cannot approve all of ${_currentChain.EIPPrefix}-721 tokens`,
								details: [
									`Contract address: ${inputOriginalTokenAddress}`,
									`User address: ${_userAddress}`,
									'',
									e.message || e,
								],
							});
							throw new Error();
						}
					}
				} else {
					// ERC-721 exact
					const isApproved = await checkApprovalERC721(
						_currentChain.chainId,
						inputOriginalTokenAddress,
						inputOriginalTokenId,
						_userAddress,
						wrapperContract,
					);
					if (!isApproved) {
						try {
							await setApprovalERC721(
								_web3,
								inputOriginalTokenAddress,
								inputOriginalTokenId,
								_userAddress,
								wrapperContract,
							);
						} catch (e: any) {
							setModal({
								type: _ModalTypes.error,
								title: `Cannot approve ${_currentChain.EIPPrefix}-721 token`,
								details: [
									`Contract address: ${inputOriginalTokenAddress}`,
									`Token id: ${inputOriginalTokenId}`,
									`User address: ${_userAddress}`,
									'',
									e.message || e,
								],
							});
							throw new Error();
						}
					}
				}
			}
*/
			updateStepAdvancedLoader({
				id: 'approve_original',
				status: _AdvancedLoadingStatus.complete,
			});
		}

		return tokenToWrap;
	};
	const approve20Collateral = async (
		_currentChain: ChainType,
		_web3: Web3,
		_userAddress: string,
		isMultisig?: boolean,
	) => {
		const erc20CollateralsToCheck = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC20;
		});
		if (!erc20CollateralsToCheck.length) {
			return;
		}

		for (let idx = 0; idx < erc20CollateralsToCheck.length; idx++) {
			const item = erc20CollateralsToCheck[idx];
			let foundToken = erc20List.find((iitem) => {
				return (
					iitem.contractAddress.toLowerCase() ===
					item.contractAddress.toLowerCase()
				);
			});
			if (!foundToken) {
				foundToken = getNullERC20(item.contractAddress);
			}

			updateStepAdvancedLoader({
				id: 'approve20collateral',
				status: _AdvancedLoadingStatus.loading,
				text: `Approving ${foundToken.symbol}`,
				current: idx + 1,
			});

			if (!item.amount) {
				continue;
			}
			if (item.contractAddress.toLowerCase() === techToken.toLowerCase()) {
				continue;
			}

			const balance = await getERC20BalanceFromChain(
				_currentChain.chainId,
				item.contractAddress,
				_userAddress,
				wrapperContract,
			);
			if (balance.balance.lt(item.amount)) {
				setModal({
					type: _ModalTypes.error,
					title: `Not enough ${foundToken.symbol}`,
					details: [
						`Token ${foundToken.symbol}: ${item.contractAddress}`,
						`User address: ${_userAddress}`,
						`User balance: ${tokenToFloat(
							balance.balance,
							foundToken.decimals,
						).toString()}`,
					],
				});
				throw new Error();
			}
			if (!balance.allowance || balance.allowance.amount.lt(item.amount)) {
				try {
					if (isMultisig) {
						await makeERC20AllowanceMultisig(
							_web3,
							item.contractAddress,
							_userAddress,
							item.amount,
							wrapperContract,
						);
					} else {
						await makeERC20Allowance(
							_web3,
							item.contractAddress,
							_userAddress,
							item.amount,
							wrapperContract,
						);
					}
				} catch (e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${foundToken.symbol}`,
						details: [
							`Token ${foundToken.symbol}: ${item.contractAddress}`,
							`User address: ${_userAddress}`,
							`User balance: ${tokenToFloat(
								balance.balance,
								foundToken.decimals,
							).toString()}`,
							'',
							e.message || e,
						],
					});
					throw new Error();
				}
			}
		}

		updateStepAdvancedLoader({
			id: 'approve20collateral',
			text: `Approving ${_currentChain.EIPPrefix}-20 collateral tokens`,
			status: _AdvancedLoadingStatus.complete,
		});
	};
	const approve721Collateral = async (
		_currentChain: ChainType,
		_web3: Web3,
		_userAddress: string,
		isMultisig?: boolean,
	) => {
		const erc721CollateralsToCheck = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC721;
		});
		if (!erc721CollateralsToCheck.length) {
			return;
		}

		for (let idx = 0; idx < erc721CollateralsToCheck.length; idx++) {
			const item = erc721CollateralsToCheck[idx];

			if (!item.tokenId) {
				continue;
			}

			updateStepAdvancedLoader({
				id: 'approve721collateral',
				text: `Approving ${_currentChain.EIPPrefix}-721: ${compactString(
					item.contractAddress,
				)}: ${item.tokenId}`,
				status: _AdvancedLoadingStatus.loading,
			});

			const isApproved = await checkApprovalERC721(
				_currentChain.chainId,
				item.contractAddress,
				item.tokenId,
				_userAddress,
				wrapperContract,
			);
			if (!isApproved) {
				try {
					if (isMultisig) {
						await setApprovalERC721Multisig(
							_web3,
							item.contractAddress,
							item.tokenId,
							_userAddress,
							wrapperContract,
						);
					} else {
						await setApprovalERC721(
							_web3,
							item.contractAddress,
							item.tokenId,
							_userAddress,
							wrapperContract,
						);
					}
				} catch (e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${_currentChain.EIPPrefix}-721`,
						details: [
							`Contract address: ${item.contractAddress}`,
							`Token id: ${item.tokenId}`,
							`User address: ${_userAddress}`,
							'',
							e.message || e,
						],
					});
					throw new Error();
				}
			}
		}

		updateStepAdvancedLoader({
			id: 'approve721collateral',
			text: `Approving ${_currentChain.EIPPrefix}-721 collateral tokens`,
			status: _AdvancedLoadingStatus.complete,
		});
	};
	const approve1155Collateral = async (
		_currentChain: ChainType,
		_web3: Web3,
		_userAddress: string,
		isMultisig?: boolean,
	) => {
		const erc1155CollateralsToCheck = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC1155;
		});
		if (!erc1155CollateralsToCheck.length) {
			return;
		}

		for (let idx = 0; idx < erc1155CollateralsToCheck.length; idx++) {
			const item = erc1155CollateralsToCheck[idx];

			updateStepAdvancedLoader({
				id: 'approve1155collateral',
				text: `Approving ${_currentChain.EIPPrefix}-1155: ${compactString(
					item.contractAddress,
				)}`,
				status: _AdvancedLoadingStatus.loading,
			});

			const isApproved = await checkApprovalERC1155(
				_currentChain.chainId,
				item.contractAddress,
				_userAddress,
				wrapperContract,
			);
			if (!isApproved) {
				try {
					if (isMultisig) {
						await setApprovalERC1155Multisig(
							_web3,
							item.contractAddress,
							_userAddress,
							wrapperContract,
						);
					} else {
						await setApprovalERC1155(
							_web3,
							item.contractAddress,
							_userAddress,
							wrapperContract,
						);
					}
				} catch (e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${_currentChain.EIPPrefix}-1155`,
						details: [
							`Contract address: ${item.contractAddress}`,
							`User address: ${_userAddress}`,
							'',
							e.message || e,
						],
					});
					throw new Error();
				}
			}
		}

		updateStepAdvancedLoader({
			id: 'approve1155collateral',
			text: `Approving ${_currentChain.EIPPrefix}-1155 collateral tokens`,
			status: _AdvancedLoadingStatus.complete,
		});
	};
	const approveWNFTFees = async (
		_currentChain: ChainType,
		_web3: Web3,
		_userAddress: string,
		isMultisig?: boolean,
	) => {
		const wnftCollateralsToCheck = collaterals.filter((item) => {
			return addressIsStorage(item.contractAddress);
		});
		if (!wnftCollateralsToCheck.length) {
			return;
		}

		for (let idx = 0; idx < wnftCollateralsToCheck.length; idx++) {
			const item = wnftCollateralsToCheck[idx];

			if (!item.tokenId) {
				continue;
			}

			updateStepAdvancedLoader({
				id: 'approvewnftcollateralfees',
				text: `Approving fee of ${compactString(item.contractAddress)}: ${
					item.tokenId
				}`,
				status: _AdvancedLoadingStatus.loading,
			});

			const wnft = await getWNFTById(
				_currentChain.chainId,
				item.contractAddress,
				item.tokenId,
			);
			if (!wnft || !wnft.fees.length) {
				continue;
			}

			const fee = wnft.fees[0];

			if (fee.token.toLowerCase() === techToken.toLowerCase()) {
				continue;
			}

			let foundToken = erc20List.find((iitem) => {
				return iitem.contractAddress.toLowerCase() === fee.token.toLowerCase();
			});
			if (!foundToken) {
				foundToken = getNullERC20(fee.token);
			}

			updateStepAdvancedLoader({
				id: 'approvewnftcollateralfees',
				text: `Approving (${foundToken.symbol}) of ${compactString(
					item.contractAddress,
				)}: ${item.tokenId}`,
				status: _AdvancedLoadingStatus.loading,
			});

			// erc20 collateral of same token
			// without summing this tx will overwrite approval amount
			const erc20Collateral = collaterals.find((iitem) => {
				return iitem.contractAddress.toLowerCase() === fee.token.toLowerCase();
			});
			const amountToCheck = fee.value.plus(
				erc20Collateral?.amount || new BigNumber(0),
			);
			const balance = await getERC20BalanceFromChain(
				_currentChain.chainId,
				fee.token,
				_userAddress,
				wrapperContract,
			);
			if (balance.balance.lt(amountToCheck)) {
				setModal({
					type: _ModalTypes.error,
					title: `Not enough ${foundToken.symbol}`,
					details: [
						`Token ${foundToken.symbol}: ${fee.token}`,
						`User address: ${_userAddress}`,
						`User balance: ${tokenToFloat(
							balance.balance,
							foundToken.decimals,
						).toString()}`,
					],
				});
				throw new Error();
			}
			if (!balance.allowance || balance.allowance.amount.lt(amountToCheck)) {
				try {
					if (isMultisig) {
						await makeERC20AllowanceMultisig(
							_web3,
							fee.token,
							_userAddress,
							amountToCheck,
							wrapperContract,
						);
					} else {
						await makeERC20Allowance(
							_web3,
							fee.token,
							_userAddress,
							amountToCheck,
							wrapperContract,
						);
					}
				} catch (e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${foundToken.symbol}`,
						details: [
							`Token ${foundToken.symbol}: ${fee.token}`,
							`User address: ${_userAddress}`,
							`User balance: ${tokenToFloat(
								balance.balance,
								foundToken.decimals,
							).toString()}`,
							'',
							e.message || e,
						],
					});
					throw new Error();
				}
			}
		}

		updateStepAdvancedLoader({
			id: 'approvewnftcollateralfees',
			text: `Approving wDA (wNFT) collateral fees`,
			status: _AdvancedLoadingStatus.complete,
		});
	};


	const wrapWithMintSubmit = async (
		_currentChain: ChainType,
//		_web3: Web3,
		_userAddress: string,
		isMultisig?: boolean,
	) => {
		createAdvancedLoaderWrap(_currentChain,  _userAddress);
	console.log('_currentChain',_currentChain);

		
		let tokenToWrap;
/*
		try {
			tokenToWrap = await getWrapParamToken(
				_currentChain,
				_userAddress,
				isMultisig,
			);
		} catch (e: any) {
			console.log('Cannot getWrapParamToken', e);
			return;
		}

		if (!tokenToWrap) {
			return;
		}
*/
		updateStepAdvancedLoader({
			id: 'wrap',
			status: _AdvancedLoadingStatus.loading,
		});
		const wrapArgs = {
			originalToken: tokenToWrap,
			collaterals: getWrapParamCollateral(),
			locks: getWrapParamLocks(),
		};
		let txResp;
		try {
			txResp = await aptosWrapTokenWithMint(
				 wrapperContract, 
				 _currentChain.chainId, 
				 { locks: getWrapParamTimeLock(), nativeCollaterals: getNativeCollateralNumber(wrapArgs.collaterals)},
				 signAndSubmitTransaction,
				 _userAddress
				 );
				 console.log('txResp',txResp);
				 
		} catch (e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot wrap token`,
				details: [
					`User address: ${_userAddress}`,
					`Args: ${JSON.stringify(wrapArgs)}`,
					'',
					e.message || e,
				],
			});
			return;
		}
		let resultDaAddress = '';
		if (!txResp) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot wrap token`,
				details: [
					`User address: ${_userAddress}`,
					`Args: ${JSON.stringify(wrapArgs)}`,
					'',
				],
			});
			return;
		}
		else {
			if (txResp.output.events) {
				console.log('txResp.events', txResp.output.events);
				
			let listEvents = txResp.output.events;
			console.log('wrapperCollection', wrapperCollection);
			let resEvent = listEvents.find((element: { guid: { account_address: string; }; }) => element.guid.account_address==wrapperCollection);
			console.log('resEvent', resEvent);

			if (resEvent?.data.token) {
				resultDaAddress = resEvent.data.token;
				}
			}
		}
		updateStepAdvancedLoader({
			id: 'wrap',
			status: _AdvancedLoadingStatus.complete,
		});
		unsetModal();

		
		let testnetStr = _currentChain.isTestNetwork?'?network=testnet':'?network=mainnet';  
console.log('testnetStr', testnetStr);
console.log('_currentChain', _currentChain);


{
			setModal({
				type: _ModalTypes.success,
				title: `Token successfully wrapped`,
				text: currentChain?.hasOracle
					? [
							{
								text: 'After oracle synchronization, your changes will appear on dashboard soon. Please refresh the dashboard page',
							},
					  ]
					: [],
				copyables: [
					{
						title: 'Token address',
						content: resultDaAddress,
					},
				],
				buttons: [
					{
						text: 'Ok',
						clickFunc: () => {
							unsetModal();
							window.location.href = `/dashboard/${_currentChain.chainId}/${resultDaAddress}`;
			//					window.location.reload();
						},
					},
				],
				links: [
					{
						text: `View on ${_currentChain.explorerName}`,
						url: combineURLs(
							_currentChain.explorerBaseUrl,
							`/txn/${txResp.hash}${testnetStr}`, //TODO
						),
					},
				],
			});
		}
	};

	const wrapSubmit = async (
		_currentChain: ChainType,
//		_web3: Web3,
		_userAddress: string,
		isMultisig?: boolean,
	) => {
		console.log('wrapWith', wrapWith);
		if (wrapWith === 'predefined') {
			await wrapWithMintSubmit(_currentChain,_userAddress, false);
			return
		}
			//		createAdvancedLoaderWrap(_currentChain, _web3, _userAddress);
		createAdvancedLoaderWrap(_currentChain,  _userAddress);
		let tokenToWrap;

		try {
			tokenToWrap = await getWrapParamToken(
				_currentChain,
//				_web3,
				_userAddress,
				isMultisig,
			);
		} catch (e: any) {
			console.log('Cannot getWrapParamToken', e);
			return;
		}
		/*web3
		try {
			await approve20Collateral(_currentChain, _web3, _userAddress, isMultisig);
		} catch (e: any) {
			console.log('Cannot approve20Collateral', e);
			return;
		}
		try {
			await approve721Collateral(
				_currentChain,
				_web3,
				_userAddress,
				isMultisig,
			);
		} catch (e: any) {
			console.log('Cannot approve721Collateral', e);
			return;
		}
		try {
			await approve1155Collateral(
				_currentChain,
				_web3,
				_userAddress,
				isMultisig,
			);
		} catch (e: any) {
			console.log('Cannot approve1155Collateral', e);
			return;
		}
		try {
			await approveWNFTFees(_currentChain, _web3, _userAddress, isMultisig);
		} catch (e: any) {
			console.log('Cannot approveWNFTFees', e);
			return;
		}
*/
		if (!tokenToWrap) {
			return;
		}

		updateStepAdvancedLoader({
			id: 'wrap',
			status: _AdvancedLoadingStatus.loading,
		});
		const wrapArgs = {
			originalToken: tokenToWrap,
			collaterals: getWrapParamCollateral(),
			locks: getWrapParamLocks(),
		};
		let txResp;
		try {
			/*
			if (isMultisig) {
				txResp = await wrapTokenMultisig(_web3, wrapperContract, wrapArgs);
			} else {
				txResp = await wrapToken(_web3, wrapperContract, wrapArgs);
			}
			*/
			txResp = await aptosWrapToken(//_web3,
				 wrapperContract, 
				 _currentChain.chainId, 
				 { tokenId:tokenToWrap.tokenId, locks: getWrapParamTimeLock(), nativeCollaterals: getNativeCollateralNumber(wrapArgs.collaterals)},
				 signAndSubmitTransaction,
				 _userAddress
				 );
				 console.log('txResp',txResp);
				 
		} catch (e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot wrap token`,
				details: [
					`Original token ${tokenToWrap.contractAddress}: ${tokenToWrap.tokenId}`,
					`User address: ${_userAddress}`,
					`Args: ${JSON.stringify(wrapArgs)}`,
					'',
					e.message || e,
				],
			});
			return;
		}
		let resultDaAddress = '';
		if (!txResp) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot wrap token`,
				details: [
					`User address: ${_userAddress}`,
					`Args: ${JSON.stringify(wrapArgs)}`,
					'',
				],
			});
			return;
		}
		else {
			if (txResp.output.events) {
				console.log('txResp.events', txResp.output.events);
				
			let listEvents = txResp.output.events;
			console.log('wrapperCollection', wrapperCollection);
			let resEvent = listEvents.find((element: { guid: { account_address: string; }; }) => element.guid.account_address==wrapperCollection);
			console.log('resEvent', resEvent);

			if (resEvent?.data.token) {
				resultDaAddress = resEvent.data.token;
				}
			}
		}

		updateStepAdvancedLoader({
			id: 'wrap',
			status: _AdvancedLoadingStatus.complete,
		});
		unsetModal();
/*
		if (isMultisig) {
			setModal({
				type: _ModalTypes.success,
				title: `Transactions have been created`,
				copyables: [
					{
						title: 'Contract address',
						content: txResp.events.WrappedV1.returnValues.outAssetAddress,
					},
					{
						title: 'Token ID',
						content: txResp.events.WrappedV1.returnValues.outTokenId,
					},
				],
				buttons: [
					{
						text: 'Ok',
						clickFunc: () => {
							unsetModal();
							window.location.href = '/dashboard';
						},
					},
				],
				links: [
					{
						text: `View on ${_currentChain.explorerName}`,
						url: combineURLs(
							_currentChain.explorerBaseUrl,
							`/tx/${txResp.transactionHash}`,
						),
					},
				],
			});
		} else */
		let testnetStr = _currentChain.isTestNetwork?'?network=testnet':'';  

		{
			setModal({
				type: _ModalTypes.success,
				title: `Token successfully wrapped`,
				text: currentChain?.hasOracle
					? [
							{
								text: 'After oracle synchronization, your changes will appear on dashboard soon. Please refresh the dashboard page',
							},
					  ]
					: [],
				copyables: [
					{
						title: 'Token address',
						content: resultDaAddress,
					},
				],
				buttons: [
					{
						text: 'Ok',
						clickFunc: () => {
							unsetModal();
						window.location.href = `/dashboard/${_currentChain.chainId}/${resultDaAddress}`;
						//window.location.reload();
						},
					},
				],
				links: [
					{
						text: `View on ${_currentChain.explorerName}`,
						url: combineURLs(
							_currentChain.explorerBaseUrl,
							`/txn/${txResp.hash}${testnetStr}`, //TODO
						),
					},
				],
			});
		}
	};
	const getWrapBtn = () => {
		return (
			<button
				className='btn btn-lg w-100'
				disabled={!!hasErrors().length}
				onClick={async () => {
					if (!currentChain) {
						return;
					}
					if (wrapperContract === '') {
						return;
					}
					if (wrapWith === 'own') {
						const foundSaved = originalTokenCache.find((item) => {
							return (
								item.contractAddress.toLowerCase() ===
									inputOriginalTokenAddress.toLowerCase() &&
								item.tokenId.toLowerCase() ===
									inputOriginalTokenId.toLowerCase()
							);
						});
						console.log('wrap - foundSaved',foundSaved);
						
						if (!foundSaved) {
							setInfo('Wait for original token data load');
							return;
						}
					}

					let _web3 = web3;

					let _userAddress = userAddress;
					console.log('onClickWrap currentChain',currentChain);
					
					let _currentChain = currentChain;
					if (//!_web3 ||
						 !_userAddress || !_currentChain) {
						return;
					}

					/*web3
					if (
						!web3 ||
						!userAddress ||
						(await getChainId(web3)) !== _currentChain.chainId
					) {
						setLoading('Waiting for wallet');

						try {
							const web3Params = await getWeb3Force(_currentChain.chainId);

							_web3 = web3Params.web3;
							_userAddress = web3Params.userAddress;
							_currentChain = web3Params.chain;
							unsetModal();
						} catch (e: any) {
							setModal({
								type: _ModalTypes.error,
								title: 'Error with wallet',
								details: [e.message || e],
							});
							return;
						}
					}

					if (!_web3 || !_userAddress || !_currentChain) {
						return;
					}
					const isMultisig =
						localStorageGet('authMethod').toLowerCase() === 'gnosis';
					if (isMultisig && wrapWith === 'predefined') {
						setInfo(
							'Predefined contracts are not available for multisig wallets yet',
						);
						return;
					}
					wrapSubmit(_currentChain, _web3, _userAddress, false);
					*/
					wrapSubmit(_currentChain, _userAddress, false);
				}}
			>
				Wrap
			</button>
		);
	};

	return (
		<main className='s-main'>
			<div className='container'>
				<div className='wf-settings mb-8'>
					<div className='row'>
						{//getModeSelector()
						}
						{getTemplatesBlock()}
					</div>
				</div>
				{getWrapWithBlock()}

				{getDisclaimerBlock()}

				{getTokenInfoBlock()}

				{
					//getStandartSelectorBlock()
				}

				{
					//getAdvancedOptionsBlock()
				}

				{
					//getCollateralBlock()
					pageMode === 'simple'
						? getCollateralBlock()
						: getNativeCollateralBlock()
				}

				{
					//getFeeBlock()
				}
				{
					//getRoyaltyBlock()
				}

				{getUnlockBlock()}

				<div className='row'>
					<div className='col-md-6'>{getErrorsBlock()}</div>
					<div className='col-md-6'>{getWrapBtn()}</div>
				</div>
			</div>
		</main>
	);
}
