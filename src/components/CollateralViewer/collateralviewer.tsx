import React, { useContext } from 'react';
import Tippy from '@tippyjs/react';

import icon_i_del from '../../static/pics/icons/i-del.svg';
import default_token_preview from '../../static/pics/tb-nft-default.svg';
import icon_loading from '../../static/pics/loading.svg';
import default_icon from '../../core-pics/coins/_default.svg';
import {
	BigNumber,
	CollateralItem,
	_AssetType,
	compactString,
	tokenToFloat,
} from '../../core-src';
import { ERC20Context, Web3Context } from '../../dispatchers';
import TippyWrapper from '../TippyWrapper';

export type CollateralError = {
	contractAddress: string;
	tokenId?: string;
	msg: string;
};

type CollateralViewerProps = {
	collaterals: Array<CollateralItem>;
	collateralErrors?: Array<CollateralError>;
	nonRemovableAddress?: string;
	removeRow?: Function;
	width: string; // 'wide' or 'narrow'
	color?: string; // default: none, '' is for dark, also can be 'light'
	showHeader?: boolean;
};

export default function CollateralViewer(props: CollateralViewerProps) {
	const columnClasses = {
		wide: {
			fungible: {
				icon: 'col-auto col-md-2 mb-2 order-1',
				address: 'col-md-3 mb-2 order-3 order-md-2',
				amount: 'col-md-5 mb-2 order-4 order-md-3',
				assetType: 'col-auto col-md-2 mb-2 order-2 order-md-4',
			},
			nft: {
				icon: 'col-auto col-md-2 mb-2 order-1',
				address: 'col-md-3 mb-2 order-3 order-md-2',
				tokenId: 'col-auto col-md-3 order-5 order-md-4',
				amount: 'col-md-2 mb-2 order-4 order-md-3',
				assetType: 'col-auto col-md-2 mb-2 order-2 order-md-5',
			},
		},
		narrow: {
			fungible: {
				icon: 'col-auto col-md-2 mb-2 order-1',
				address: 'col-md-3 mb-2 order-3 order-md-2',
				amount: 'col-md-5 mb-2 order-4 order-md-3',
				assetType: 'col-auto col-md-2 mb-2 order-2 order-md-4 md-right',
			},
			nft: {
				icon: 'col-auto col-md-2 mb-2 order-1',
				address: 'col-md-3 mb-2 order-3 order-md-2',
				tokenId: 'col-auto col-md-3 mb-2 order-5 order-md-4',
				amount: 'col-md-2 mb-2 order-4 order-md-3',
				assetType:
					'col-auto col-md-2 mb-2 order-2 order-md-5 md-right align-self-center align-self-md-start',
			},
		},
	};

	const { currentChain } = useContext(Web3Context);
	const { erc20List, requestERC20Token } = useContext(ERC20Context);

	const getErrorMsgs = (item: CollateralItem): Array<CollateralError> => {
		let errors: Array<CollateralError> = [];

		if (props.collateralErrors && props.collateralErrors.length) {
			errors = props.collateralErrors.filter((iitem) => {
				if (item.tokenId) {
					return (
						item.contractAddress.toLowerCase() ===
							iitem.contractAddress.toLowerCase() &&
						item.tokenId === iitem.tokenId
					);
				} else {
					return (
						!iitem.tokenId &&
						item.contractAddress.toLowerCase() ===
							iitem.contractAddress.toLowerCase()
					);
				}
			});
		}

		return errors;
	};

	const getDelButton = (item: CollateralItem) => {
		if (!props.removeRow) {
			return null;
		}

		if (
			props.nonRemovableAddress &&
			props.nonRemovableAddress.toLowerCase() ===
				item.contractAddress.toLowerCase()
		) {
			return (
				<TippyWrapper msg="You can\'t delete slot. It is reserved for transfer fee collateral. Switch off wDA (wNFT) from royalty recipients">
					<button className='btn-del'>
						<img src={icon_i_del} alt='' />
					</button>
				</TippyWrapper>
			);
		}

		return (
			<button
				className='btn-del'
				onClick={() => {
					if (props.removeRow) {
						props.removeRow(item);
					}
				}}
			>
				<img src={icon_i_del} alt='' />
			</button>
		);
	};
	const getCollateralNativeRow = (item: CollateralItem) => {
		const errors = getErrorMsgs(item);

		return (
			<React.Fragment key={'native'}>
				<div className={`item ${errors.length ? 'has-error' : ''}`}>
					<div className='row'>
						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.fungible.icon
									: columnClasses.narrow.fungible.icon
							}
						>
							<div className='tb-coin'>
								<span className='i-coin'>
									<img src={currentChain?.tokenIcon || default_icon} alt='' />
								</span>
								<span>{currentChain?.symbol || '0x000'}</span>
							</div>
						</div>

						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.fungible.address
									: columnClasses.narrow.fungible.address
							}
						>
							{/* <span className="col-legend">Contract: </span>
							<span className="text-break">0x000...00000</span> */}
						</div>

						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.fungible.amount
									: columnClasses.narrow.fungible.amount
							}
						>
							<span className='col-legend'>Amount:</span>
							<span className='text-break'>
								{item.amount
									? tokenToFloat(
											item.amount,
											currentChain?.decimals || 8,
									  ).toString()
									: ''}
							</span>
						</div>

						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.fungible.assetType
									: columnClasses.narrow.fungible.assetType
							}
						>
							Native
						</div>

						{getDelButton(item)}
					</div>
				</div>
				{/* {
					errors.map((item) => { return ( <div className="item-error">{ item.msg }</div> ) })
				} */}
				<div className='item-error'>
					{errors
						.map((iitem) => {
							return iitem.msg;
						})
						.join(', ')}
				</div>
			</React.Fragment>
		);
	};
	const getCollateralERC20Row = (item: CollateralItem) => {
		const errors = getErrorMsgs(item);

		let foundToken = erc20List.find((iitem) => {
			if (!iitem.contractAddress) {
				return false;
			}
			return (
				item.contractAddress.toLowerCase() ===
				iitem.contractAddress.toLowerCase()
			);
		});

		if (foundToken) {
			return (
				<React.Fragment key={item.contractAddress}>
					<div className={`item ${errors.length ? 'has-error' : ''}`}>
						<div className='row'>
							<div
								className={
									props.width === 'wide'
										? columnClasses.wide.fungible.icon
										: columnClasses.narrow.fungible.icon
								}
							>
								<div className='tb-coin'>
									<span className='i-coin'>
										<img src={foundToken.icon} alt='' />
									</span>
									<span>{foundToken.symbol}</span>
								</div>
							</div>

							<div
								className={
									props.width === 'wide'
										? columnClasses.wide.fungible.address
										: columnClasses.narrow.fungible.address
								}
							>
								<span className='col-legend'>Contract: </span>
								<span className='text-break'>
									{compactString(item.contractAddress)}
								</span>
							</div>

							<div
								className={
									props.width === 'wide'
										? columnClasses.wide.fungible.amount
										: columnClasses.narrow.fungible.amount
								}
							>
								<span className='col-legend'>Amount:</span>
								<span className='text-break'>
									{item.amount
										? tokenToFloat(
												item.amount,
												foundToken.decimals || 8,
										  ).toString()
										: ''}
								</span>
							</div>

							<div
								className={
									props.width === 'wide'
										? columnClasses.wide.fungible.assetType
										: columnClasses.narrow.fungible.assetType
								}
							>
								{currentChain?.EIPPrefix || 'ERC'}-20
							</div>

							{getDelButton(item)}
						</div>
					</div>
					{/* {
						errors.map((item) => { return ( <div className="item-error">{ item.msg }</div> ) })
					} */}
					<div className='item-error'>
						{errors
							.map((iitem) => {
								return iitem.msg;
							})
							.join(', ')}
					</div>
				</React.Fragment>
			);
		} else {
			requestERC20Token(item.contractAddress);
			return (
				<React.Fragment key={item.contractAddress}>
					<div className={`item ${errors.length ? 'has-error' : ''}`}>
						<div className='row'>
							<div
								className={
									props.width === 'wide'
										? columnClasses.wide.fungible.icon
										: columnClasses.narrow.fungible.icon
								}
							>
								<div className='tb-coin'>
									<span className='i-coin'>
										<img src={default_icon} alt='' />
									</span>
									<span>{compactString(item.contractAddress)}</span>
								</div>
							</div>

							<div
								className={
									props.width === 'wide'
										? columnClasses.wide.fungible.address
										: columnClasses.narrow.fungible.address
								}
							>
								<span className='col-legend'>Contract: </span>
								<span className='text-break'>
									{compactString(item.contractAddress)}
								</span>
							</div>

							<Tippy
								content={'decimals is unknown; amount shown in wei'}
								appendTo={document.getElementsByClassName('wrapper')[0]}
								trigger='mouseenter'
								interactive={false}
								arrow={false}
								maxWidth={260}
							>
								<div
									className={
										props.width === 'wide'
											? columnClasses.wide.fungible.amount
											: columnClasses.narrow.fungible.amount
									}
								>
									<span className='col-legend'>Amount*:</span>
									<span className='text-break text-orange'>
										{item.amount ? item.amount.toString() : ''}*
									</span>
								</div>
							</Tippy>

							<div
								className={
									props.width === 'wide'
										? columnClasses.wide.fungible.assetType
										: columnClasses.narrow.fungible.assetType
								}
							>
								{currentChain?.EIPPrefix || 'ERC'}-20
							</div>

							{getDelButton(item)}
						</div>
					</div>
					{/* {
						errors.map((item) => { return ( <div className="item-error">{ item.msg }</div> ) })
					} */}
					<div className='item-error'>
						{errors
							.map((iitem) => {
								return iitem.msg;
							})
							.join(', ')}
					</div>
				</React.Fragment>
			);
		}
	};
	const getNFTPreview = (item: CollateralItem) => {
		if (item.tokenImg === undefined) {
			return (
				<span className='i-coin'>
					<img src={icon_loading} alt='' />
				</span>
			);
		}
		if (item.tokenImg === '') {
			return <img src={default_token_preview} alt='' />;
		}

		return <img src={item.tokenImg} alt='' />;
	};
	const getCollateralERC721Row = (item: CollateralItem) => {
		const errors = getErrorMsgs(item);

		return (
			<React.Fragment key={`ERC721${item.contractAddress}${item.tokenId}`}>
				<div className={`item ${errors.length ? 'has-error' : ''}`}>
					<div className='row'>
						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.nft.icon
									: columnClasses.narrow.nft.icon
							}
						>
							<div className='tb-nft'>{getNFTPreview(item)}</div>
						</div>

						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.nft.address
									: columnClasses.narrow.nft.address
							}
						>
							<span className='col-legend'>Contract: </span>
							<span className='text-break'>
								{item.contractAddress
									? compactString(item.contractAddress)
									: ''}
							</span>
						</div>

						<div className='col-md-2 mb-2 order-4 order-md-3 d-none d-md-block'></div>

						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.nft.tokenId
									: columnClasses.narrow.nft.tokenId
							}
						>
							<span className='text-break'>ID {item.tokenId || ''}</span>
						</div>

						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.nft.assetType
									: columnClasses.narrow.nft.assetType
							}
						>
							{currentChain?.EIPPrefix || 'ERC'}-721
						</div>

						{getDelButton(item)}
					</div>
				</div>
				{/* {
					errors.map((item) => { return ( <div className="item-error">{ item.msg }</div> ) })
				} */}
				<div className='item-error'>
					{errors
						.map((iitem) => {
							return iitem.msg;
						})
						.join(', ')}
				</div>
			</React.Fragment>
		);
	};
	const get1155Amount = (qty: BigNumber | undefined) => {
		if (!qty) {
			return '';
		}
		if (qty.eq(0)) {
			return '';
		}
		if (qty.eq(1)) {
			return '1 item';
		}
		return `${qty} items`;
	};
	const getCollateralERC1155Row = (item: CollateralItem) => {
		const errors = getErrorMsgs(item);

		return (
			<React.Fragment key={`ERC1155${item.contractAddress}${item.tokenId}`}>
				<div className={`item ${errors.length ? 'has-error' : ''}`}>
					<div className='row'>
						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.nft.icon
									: columnClasses.narrow.nft.icon
							}
						>
							<div className='tb-nft'>{getNFTPreview(item)}</div>
						</div>

						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.nft.address
									: columnClasses.narrow.nft.address
							}
						>
							<span className='col-legend'>Contract: </span>
							<span className='text-break'>
								{item.contractAddress
									? compactString(item.contractAddress)
									: ''}
							</span>
						</div>

						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.nft.amount
									: columnClasses.narrow.nft.amount
							}
						>
							<span className='col-legend'>Amount:</span>
							<span className='text-break'>{get1155Amount(item.amount)}</span>
						</div>

						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.nft.tokenId
									: columnClasses.narrow.nft.tokenId
							}
						>
							<span className='text-break'>ID {item.tokenId || ''}</span>
						</div>

						<div
							className={
								props.width === 'wide'
									? columnClasses.wide.nft.assetType
									: columnClasses.narrow.nft.assetType
							}
						>
							{currentChain?.EIPPrefix || 'ERC'}-1155
						</div>

						{getDelButton(item)}
					</div>
				</div>
				{/* {
					errors.map((item) => { return ( <div className="item-error">{ item.msg }</div> ) })
				} */}
				<div className='item-error'>
					{errors
						.map((iitem) => {
							return iitem.msg;
						})
						.join(', ')}
				</div>
			</React.Fragment>
		);
	};

	return (
		<div className={`c-wrap__table mt-3 ${props.color || ''}`}>
			{props.showHeader ? (
				<div className='item item-header'>
					<div className='row'>
						<div className='mb-2 col-md-2 text-muted'>Token</div>
						<div className='mb-2 col-md-3'>Contract</div>
						<div className='mb-2 col-md-2'>Amount</div>
						<div className='mb-2 col-md-3'>TokenId</div>
						<div className='mb-2 col-md-2 md-right'>Type</div>
					</div>
				</div>
			) : null}
			{props.collaterals
				.sort((item, prev) => {
					if (item.assetType < prev.assetType) {
						return -1;
					}
					if (item.assetType > prev.assetType) {
						return 1;
					}

					if (
						item.contractAddress.toLowerCase() <
						prev.contractAddress.toLowerCase()
					) {
						return -1;
					}
					if (
						item.contractAddress.toLowerCase() >
						prev.contractAddress.toLowerCase()
					) {
						return 1;
					}

					if (item.tokenId && prev.tokenId) {
						try {
							if (
								new BigNumber(item.tokenId).isNaN() ||
								new BigNumber(prev.tokenId).isNaN()
							) {
								if (parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`)) {
									return -1;
								}
								if (parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`)) {
									return 1;
								}
							}
							const itemTokenIdNumber = new BigNumber(item.tokenId);
							const prevTokenIdNumber = new BigNumber(prev.tokenId);

							if (itemTokenIdNumber.lt(prevTokenIdNumber)) {
								return -1;
							}
							if (itemTokenIdNumber.gt(prevTokenIdNumber)) {
								return 1;
							}
						} catch (ignored) {
							if (
								`${item.tokenId}`.toLowerCase() <
								`${prev.tokenId}`.toLowerCase()
							) {
								return -1;
							}
							if (
								`${item.tokenId}`.toLowerCase() >
								`${prev.tokenId}`.toLowerCase()
							) {
								return 1;
							}
						}
					}

					return 0;
				})
				.map((item) => {
					if (item.assetType === _AssetType.native) {
						return getCollateralNativeRow(item);
					}
					if (item.assetType === _AssetType.ERC20) {
						return getCollateralERC20Row(item);
					}
					if (item.assetType === _AssetType.ERC721) {
						return getCollateralERC721Row(item);
					}
					if (item.assetType === _AssetType.ERC1155) {
						return getCollateralERC1155Row(item);
					}

					return null;
				})}
		</div>
	);
}
