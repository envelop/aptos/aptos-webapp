import React, { useContext, useEffect, useState } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Blockies from 'react-blockies';
import NavLinks from '../NavLinks';
import TippyWrapper from '../TippyWrapper';

import { InfoModalContext, Web3Context, _ModalTypes } from '../../dispatchers';
		
import { compactString, localStorageGet } from '../../core-src';
import { useWallet, WalletName,NetworkName, WalletReadyState  } from "@aptos-labs/wallet-adapter-react";
import {
	Network,
	NetworkToChainId
  } from "@aptos-labs/ts-sdk";
  
import default_icon from '../../core-pics/networks/_default.png';
import icon_logo from '../../static/pics/logo.svg';
import icon_logo_mob from '../../static/pics/logo-mob.svg';
import icon_i_copy from '../../static/pics/icons/i-copy.svg';
import icon_i_arrow_down from '../../static/pics/icons/i-arrow-down.svg';
import icon_i_del from '../../static/pics/i-del.svg';
import icon_i_attention from '../../static/pics/icons/i-warning.svg';
import { log } from 'console';

export default function Header() {
	const versionMenuBlockRef = React.createRef<HTMLDivElement>();
	const chainMenuBlockRef = React.createRef<HTMLDivElement>();
	const userMenuBlockRef = React.createRef<HTMLDivElement>();

	const [copiedHint, setCopiedHint] = useState(false);
	const [versionMenuOpened, setVersionMenuOpened] = useState(false);
	const [chainMenuOpened, setChainMenuOpened] = useState(false);
	const [userMenuOpened, setUserMenuOpened] = useState(false);
	const [cursorOnUserMenuBtn, setCursorOnUserMenuBtn] = useState(false);
	const [cursorOnUserMenuList, setCursorOnUserMenuList] = useState(false);

	const {
		getWeb3Force,
		currentChainId,
		walletChainId,
		userAddress,
		availableChains,
		switchChain,
//		disconnect,
		silentConnect,
		aptosSetCurrentChainId,
		setAptosNativeBalance
	} = useContext(Web3Context);

	const {
		connect,
		account,
		network,
		connected,
		disconnect,
		wallet,
		wallets,
		signAndSubmitTransaction,
//		signAndSubmitBCSTransaction,
		signTransaction,
		signMessage,
		signMessageAndVerify,
		
	  } = useWallet();

	const onConnect = async (walletName: WalletName) => {
		await connect(walletName);
	};

	const { setModal } = useContext(InfoModalContext);

	const getLogo = () => {
		return (
			<React.Fragment>
				<a href='/' className='s-header__logo d-none d-sm-block'>
					<img src={icon_logo} alt='ENVELOP' />
				</a>
				<a href='/' className='s-header__logo mob d-sm-none'>
					<img src={icon_logo_mob} alt='ENVELOP' />
				</a>
			</React.Fragment>
		);
	};
	const getVersionBlock = () => {
		const closeVersionMenu = () => {
			const body = document.querySelector('body');
			if (!body) {
				return;
			}
			body.onclick = null;
			setVersionMenuOpened(false);
		};
		const openVersionMenu = () => {
			setTimeout(() => {
				const body = document.querySelector('body');
				if (!body) {
					return;
				}
				body.onclick = (e: any) => {
					if (!versionMenuBlockRef.current) {
						return;
					}
					const _path = e.composedPath() || e.path;
					if (_path && _path.includes(versionMenuBlockRef.current)) {
						return;
					}
					closeVersionMenu();
				};
			}, 100);
			setVersionMenuOpened(true);
		};
		return (
			<div
				className='s-header__version'
				ref={versionMenuBlockRef}
				onMouseLeave={closeVersionMenu}
			>
				<button
					className={`btn btn-sm btn-gray ${versionMenuOpened ? 'active' : ''}`}
					onClick={openVersionMenu}
					onMouseEnter={openVersionMenu}
				>
					<span>v1.1.0</span>
					<img className='arrow' src={icon_i_arrow_down} alt='' />
				</button>
				{versionMenuOpened ? (
					<div className='btn-dropdown'>
						<ul>
							<li>
								<button
									onClick={() => {
										window.location.href = 'https://appv0.envelop.is';
									}}
									className='item'
								>
									v0
								</button>
							</li>
							<li>
								<button
									onClick={() => {
										window.location.href = 'https://archv1.envelop.is';
									}}
									className='item'
								>
									v1.0.0
								</button>
							</li>
							<li>
								<button className='item'>v1.1.0</button>
							</li>
						</ul>
					</div>
				) : null}
			</div>
		);
	};

	const closeChainMenu = () => {
		const body = document.querySelector('body');
		if (!body) {
			return;
		}
		body.onclick = null;
		setChainMenuOpened(false);
	};
	const openChainMenu = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if (!body) {
				return;
			}
			body.onclick = (e: any) => {
				if (!chainMenuBlockRef.current) {
					return;
				}
				const _path = e.composedPath() || e.path;
				if (_path && _path.includes(chainMenuBlockRef.current)) {
					return;
				}
				closeChainMenu();
			};
		}, 100);
		setChainMenuOpened(true);
	};
	const getChainSelectorDropdown = () => {
		if (!chainMenuOpened) {
			return null;
		}

		const mainnets = availableChains.filter((item) => {
			return !item.isTestNetwork;
		});
		const testnets = availableChains.filter((item) => {
			return item.isTestNetwork;
		});

		return (
			<div className='btn-dropdown s-header__network-dropdown'>
				<div className='dropdown-wrap'>
					<div className='dropdown-header'>
						<b>Select network</b>
						<div
							className='close'
							onClick={() => {
								setChainMenuOpened(false);
							}}
						>
							<img src={icon_i_del} alt='' />
						</div>
					</div>
					<div className='scroll'>
						<ul>
							{mainnets.map((item) => {
								return (
									<li
										key={item.chainId}
										onClick={() => {
											switchChain(item.chainId).catch((e: any) => {
												setModal({
													type: _ModalTypes.error,
													title: 'Cannot change network',
													details: [e.message || e],
												});
											});
											closeChainMenu();
										}}
									>
										<button className='item'>
											<span className='logo'>
												<img src={item.networkIcon} alt='' />
											</span>
											<span className='name'>{item.name}</span>
										</button>
									</li>
								);
							})}
						</ul>
						<ul>
							{testnets.map((item) => {
								return (
									<li
										key={item.chainId}
										onClick={() => {
											switchChain(item.chainId).catch((e: any) => {
												setModal({
													type: _ModalTypes.error,
													title: 'Cannot change network',
													details: [e.message || e],
												});
											});
											closeChainMenu();
										}}
									>
										<button className='item'>
											<span className='logo'>
												<img src={item.networkIcon} alt='' />
											</span>
											<span className='name'>{item.name} Testnet</span>
										</button>
									</li>
								);
							})}
						</ul>
					</div>
				</div>
			</div>
		);
	};
	const getChainSelector = () => {
	/*	console.log('walletChainId',walletChainId);
		console.log('availableChains',availableChains);
console.log('network?.name',network?.name);
console.log('NetworkToChainId',NetworkToChainId);
console.log('Object.keys(NetworkToChainId)',Object.keys(NetworkToChainId));
const ass = Object.keys(NetworkToChainId).filter((item)=>{ return NetworkToChainId[item]===1})
console.log('NetworkToChainId-ass',ass);
console.log('NetworkToChainId-mainnet',NetworkToChainId['mainnet']);


console.log('Network',Network);
console.log('NetworkName',NetworkName);

//console.log('availableChains',availableChains);
*/

		if (!walletChainId) {
			return null;
		}
		const chainData = availableChains.find((item) => {
/*			console.log('item.chainId',item.chainId);
			console.log('item.chainId === walletChainId', item.chainId === walletChainId);
			console.log('walletChainId', walletChainId);
*/
			return item.chainId == walletChainId;
		});
//		console.log('chainData',chainData);

		if (!chainData) {
			return null;
		}

		return (
			<>
				{currentChainId !== walletChainId ? (
					<TippyWrapper msg='Chain in wallet does not match the one is using on page'>
						<div style={{ marginRight: '30px' }}>
							<img src={icon_i_attention} alt='' />
						</div>
					</TippyWrapper>
				) : null}
				<div
					className={`s-header__network`}
					ref={chainMenuBlockRef}
					onMouseLeave={closeChainMenu}
				>
					<button
						className={`btn btn-sm btn-gray s-header__network-btn ${
							chainMenuOpened ? 'active' : ''
						}`}
						onClick={openChainMenu}
						onMouseEnter={openChainMenu}
					>
						<span className='logo'>
							<img src={chainData.networkIcon || default_icon} alt='' />
						</span>
						<span className='name'>{`${chainData.name} ${
							chainData.isTestNetwork ? 'Testnet' : ''
						}`}</span>
						<svg
							className='arrow'
							width='16'
							height='16'
							viewBox='0 0 16 16'
							fill='none'
							xmlns='http://www.w3.org/2000/svg'
						>
							<path
								d='M4.94 5.72667L8 8.78L11.06 5.72667L12 6.66667L8 10.6667L4 6.66667L4.94 5.72667Z'
								fill='white'
							></path>
						</svg>
					</button>

					{getChainSelectorDropdown()}
				</div>
			</>
		);
	};
	const getConnectBtn = () => {
		return (
			<button
				className='btn btn-connect'
				onClick={
					() => {
						let need_walet=true;
						if (wallets)
						if (wallets[0].readyState) {
							if (wallets[0].readyState===WalletReadyState.Installed) {
								setUserMenuOpened(false);
								setCursorOnUserMenuBtn(false);
								setCursorOnUserMenuList(false);

								onConnect(wallets[0].name);
								need_walet=false;
							}
						}
						if (need_walet) {
							setModal({
								type: _ModalTypes.error,
								title: `Petra wallet not installed`,
								text: [
									{
									text: `Please install Petra wallet from https://petra.app/`,
									clazz: 'text-orange',
									},
								],
							});
						}
	
					}

			}
			>
				Connect
				<span className='d-none d-md-inline'>&nbsp;Wallet</span>
			</button>
		);
	};

	useEffect(() => {
		console.log('cursorOnUserMenuBtn',cursorOnUserMenuBtn);
		console.log('cursorOnUserMenuList',cursorOnUserMenuList);
		console.log('userMenuOpened',userMenuOpened);
		
		if (!userMenuOpened && (cursorOnUserMenuBtn || cursorOnUserMenuList)) {
			console.log('openUserMenu');
			openUserMenu();
		}
		if (userMenuOpened && !cursorOnUserMenuBtn && !cursorOnUserMenuList) {
			console.log('closeUserMenu');
			closeUserMenu();
		}
	}, [cursorOnUserMenuBtn, cursorOnUserMenuList]);

////APTOS WALLET USEEFFECT
	// get initial chain id

useEffect(() => {
	console.log('connected', connected);
	const setInitialChainId = async () => {
		if (connected) {
			if (network?.chainId) {
			await aptosSetCurrentChainId(network?.chainId);
			console.log('if (network?.chainId)',network?.chainId);
			console.log('account?.address', account?.address);
			setAptosNativeBalance(network?.chainId, account?.address);
			}
		}
		else {
			//let wallet_name=wallets?wallets[0].name:"";
			if (wallets) onConnect(wallets[0].name);
			const savedChain = parseInt(localStorageGet('lastChainId'));
			if (!isNaN(savedChain)) {
				
				await 	aptosSetCurrentChainId(savedChain);
				console.log('savedChain', savedChain);
			}
			else {
				console.log('aptosSetCurrentChainId',1);
				await 	aptosSetCurrentChainId(1);
			} 
		} 
	}
	setInitialChainId();
}, [connected, network]);



useEffect(() => {
	console.log('connected', connected);
	const setInitialChainId = async () => {

	if (connected) {
	if (account?.address && userAddress!=account.address) {
		//			console.log('-----------------');
		//				console.log(account);
				if (network?.chainId) {
					await aptosSetCurrentChainId(network?.chainId);
			
					await silentConnect(account.address, network.chainId);
				}
			}

		}
	}
	setInitialChainId();
}, [account]);

useEffect(() => {
	const setInitialChainId = async () => {

	if (connected) {
		if (account?.address && userAddress!=account.address) {
			//			console.log('-----------------');
			//				console.log(account);
					if (network?.chainId) {
						await aptosSetCurrentChainId(network?.chainId);
				
						await silentConnect(account.address, network.chainId);
					}
				}
	
			}
		}
	setInitialChainId();
	
  }, []);
////END APTOS WALLET USEEFFECT


	const closeUserMenu = () => {
		const body = document.querySelector('body');
		if (!body) {
			return;
		}
		body.onclick = null;
		setUserMenuOpened(false);
	};
	const openUserMenu = () => {
		const body = document.querySelector('body');
		if (!body) {
			return;
		}
		body.onclick = (e: any) => {
			if (!userMenuBlockRef.current) {
				return;
			}
			const _path = e.composedPath() || e.path;
			if (_path && _path.includes(userMenuBlockRef.current)) {
				return;
			}
			closeUserMenu();
		};
		setUserMenuOpened(true);
	};
	const getUserMenu = () => {
		if (!userMenuOpened) {
			return;
		}

		return (
			<div
				className='s-user__menu'
				onMouseEnter={() => {
					console.log('setCursorOnUserMenuList(true)');
					
					setCursorOnUserMenuList(true);
				}}
				onMouseLeave={() => {
					console.log('setCursorOnUserMenuList(false)');
					setCursorOnUserMenuList(false);
				}}
			>
				<ul className='inner'>
					<li className='d-md-none'>
						<div className='item address'>
							<button className='btn-copy'>
								<span>{userAddress ? compactString(userAddress) : ''}</span>
								<img src={icon_i_copy} alt='' />
								<span
									className='btn-action-info'
									style={{ display: copiedHint ? 'block' : 'none' }}
								>
									Copied
								</span>
							</button>
						</div>
					</li>
					<li>
						<a href='/dashboard' className='item'>
							Dashboard
						</a>
					</li>
					<li className='mt-md-2'>
						<button
							onClick={(e) => {
								setUserMenuOpened(false);
								setCursorOnUserMenuBtn(false);
								setCursorOnUserMenuList(false);
								disconnect();
							}}
							className='item disconnect'
						>
							Disconnect
						</button>
					</li>
				</ul>
			</div>
		);
	};
	const getAvatarBlock = () => {
		if (!userAddress) {
			return null;
		}
		return (
			<div className='s-user__avatar'>
				<div className='img'>
					<Blockies
						seed={userAddress}
						size={5}
						scale={8}
						color='#141616'
						bgColor='#4afebf'
						spotColor='#ffffff'
					/>
				</div>
			</div>
		);
	};
	const getUserData = () => {
//		console.log('+++++++++++++++');

		if (!connected) {
			return null;
		}
//		console.log('0000000000000000');
/*
		let _userAddress='';
//		console.log(!userAddress);
//		console.log(userAddress);
		if (!userAddress) {
//			console.log('-----------------');
			if (account) {
				_userAddress=account.address;
//				console.log(account);
//				silentConnect(_userAddress);
			}
		}
//		console.log(compactString(_userAddress));
//		let compactUserAddress=compactString(_userAddress);
*/
		return (
			<React.Fragment>
				<div
					className='s-user'
					ref={userMenuBlockRef}
					onClick={() => {
						if (userMenuOpened) {
							closeUserMenu();
						} else {
							openUserMenu();
						}
					}}
					onMouseEnter={() => {
						console.log('setCursorOnUserMenuBtn(true)');
						
						setCursorOnUserMenuBtn(true);
					}}
					onMouseLeave={() => {
						console.log('setCursorOnUserMenuBtn(false)');
						setTimeout(() => {
							console.log('setCursorOnUserMenuBtn(false)');
							setCursorOnUserMenuBtn(false);
						}, 100);
					}}
				>
					<div className='s-user__toggle'>
						{getAvatarBlock()}
						<div className='s-user__data'>
							<span className='mr-2'>{userAddress?compactString(userAddress):''}</span>
							<svg
								className='arrow'
								width='16'
								height='16'
								viewBox='0 0 16 16'
								fill='none'
								xmlns='http://www.w3.org/2000/svg'
							>
								<path
									d='M4.94 5.72667L8 8.78L11.06 5.72667L12 6.66667L8 10.6667L4 6.66667L4.94 5.72667Z'
									fill='white'
									fillOpacity='0.6'
								></path>
							</svg>
						</div>
					</div>
					<CopyToClipboard
						text={userAddress?userAddress:''}
						onCopy={() => {
							setCopiedHint(true);
							setTimeout(() => {
								setCopiedHint(false);
							}, 5 * 1000);
						}}
					>
						<button className='btn-copy'>
							<img src={icon_i_copy} alt='' />
							<span
								className='btn-action-info'
								style={{ display: copiedHint ? 'block' : 'none' }}
							>
								Copied
							</span>
						</button>
					</CopyToClipboard>
				</div>
				{getUserMenu()}
			</React.Fragment>
		);
	};
	const getBtnOrData = () => {
//		connected
		if (!connected) {
			return getConnectBtn();
		} else {
			return getUserData();
		}

		//web3
		/*
		if (userAddress === undefined) {
			return getConnectBtn();
		} else {
			return getUserData();
		}
		*/
	};

	return (
		<header className='s-header'>
			<div className='container-fluid'>
				<div className='d-flex align-items-center h-100'>
					{getLogo()}
					{//getVersionBlock()
					}

					<NavLinks currentChainId={currentChainId} />
				</div>

				<div className='d-flex align-items-center'>
					{getChainSelector()}
					{getBtnOrData()}
				</div>
			</div>
		</header>
	);
}
