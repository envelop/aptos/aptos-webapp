import React, { useState } from 'react';

import { ERC20Type } from '../../core-src';
import default_icon from '../../core-pics/coins/_default.svg';

import icon_i_arrow_down from '../../static/pics/icons/i-arrow-down.svg';

type CoinSelectorProps = {
	tokens: Array<ERC20Type>;
	selectedToken: string;
	onChange: (address: string) => void;
};

export default function CoinSelector(props: CoinSelectorProps) {
	const [listOpened, setListOpened] = useState(false);

	const selectorBlockRef = React.createRef<HTMLDivElement>();

	const closeList = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if (!body) {
				return;
			}
			body.onclick = null;
			setListOpened(false);
		}, 100);
	};
	const openList = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if (!body) {
				return;
			}
			body.onclick = (e: any) => {
				if (!selectorBlockRef.current) {
					return;
				}
				const _path = e.composedPath() || e.path;
				if (_path && _path.includes(selectorBlockRef.current)) {
					return;
				}
				closeList();
			};
		}, 100);
		setListOpened(true);
	};

	let selectedTokenObj;
	if (props.selectedToken === '') {
		selectedTokenObj = {
			address: '',
			icon: default_icon,
			symbol: '',
		};
	}

	const foundToken = props.tokens.filter((item) => {
		if (!item.contractAddress) {
			return false;
		}
		return (
			item.contractAddress.toLowerCase() === props.selectedToken.toLowerCase()
		);
	});
	if (foundToken.length) {
		selectedTokenObj = foundToken[0];
	} else {
		selectedTokenObj = {
			address: '',
			icon: default_icon,
			symbol: '',
		};
	}

	return (
		<div
			className='select-coin'
			ref={selectorBlockRef}
			onMouseLeave={closeList}
		>
			<div
				className='select-coin__value'
				onMouseEnter={openList}
				onClick={openList}
			>
				<span className='field-unit'>
					<span className='i-coin'>
						<img src={selectedTokenObj.icon} alt='' />
					</span>
					{selectedTokenObj.symbol}
				</span>
				<img className='arrow' src={icon_i_arrow_down} alt='' />
			</div>

			{listOpened ? (
				<ul className='select-coin__list'>
					{props.tokens
						.sort((item, prev) => {
							return item.contractAddress
								.toLowerCase()
								.localeCompare(prev.contractAddress.toLowerCase());
						})
						.map((item) => {
							return (
								<li
									key={item.contractAddress}
									onClick={() => {
										props.onChange(item.contractAddress);
									}}
								>
									<span className='field-unit'>
										<span className='i-coin'>
											<img src={item.icon} alt='' />
										</span>
										{item.symbol}
									</span>
								</li>
							);
						})}
				</ul>
			) : null}
		</div>
	);
}
