import React, {
	useState
} from "react";
import { Link } from "react-router-dom";
import config from '../../app.config.json';

type NavLinkItem = {
	name: string,
	description: string,
	url: string,
	active: boolean,
	local: boolean,
	newTab: boolean,
	children: Array<NavLinkItem>,
	showInChains?: Array<number>, // undefined means show everywhere; [ ] means do not show
}

type NavLinksProps = {
	currentChainId: number,
}

export default function NavLinks(props: NavLinksProps) {

	const {
		currentChainId
	} = props;

	const [ opened, setOpened ] = useState(false);
	const linksBlockRef         = React.useRef(null);

	const getLinkBody = (item: NavLinkItem) => {
		if ( item.description && item.description !== '' ) {
			return (
				<React.Fragment>
					<span>{ item.name }</span>
					<small>{ item.description }</small>
				</React.Fragment>
			)
		}

		return item.name
	}
	const getLink = (item: NavLinkItem, nested: boolean) => {

		let clazz = '';
		if ( !nested ) {
			clazz = `${clazz} link`;
			if ( item.active ) {
				clazz = `${clazz} active`;
			}
		}

		if ( item.local ) {
			return (
				<Link
					className={ clazz }
					to={ item.url }
				>
					{ getLinkBody(item) }
				</Link>
			)
		}

		if ( item.newTab ) {
			return (
				<a
					href={ item.url }
					className={ clazz }
					target="_blank"
					rel="noopener noreferrer"
				>
					{ getLinkBody(item) }
				</a>
			)
		}

		return (
			<a
				href={ item.url }
				className={ clazz }
			>
				{ getLinkBody(item) }
			</a>
		)

	}
	const getList = (item: NavLinkItem) => {

		const listToRender = item.children.filter((iitem) => { return iitem.showInChains === undefined || iitem.showInChains.includes(currentChainId) });
		if ( !listToRender.length ) { return null; }

		return (
			<div className="nav-item" key={ item.name }>
				<div className="link">
					<span className="mr-1">{ getLinkBody(item) }</span>
					<svg className="arrow" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M4.94 5.72667L8 8.78L11.06 5.72667L12 6.66667L8 10.6667L4 6.66667L4.94 5.72667Z" fill="white"></path>
					</svg>
				</div>
				<div className="nav-dropdown dropdown-launchpads">
					{
						listToRender.map((iitem) => {
							return (
								<div className="dropdown-item" key={ iitem.name }>
									{ getLink(iitem, true) }
								</div>
							)
						})
					}
				</div>
			</div>
		)

	}
	const getItem = (item: NavLinkItem) => {

		if ( item.showInChains !== undefined && !item.showInChains.includes(currentChainId) ) { return null; }

		if ( item.children && item.children.length ) { return getList(item) }

		return (
			<div className="nav-item" key={ item.name }>
				{ getLink(item, false) }
			</div>
		)

	}

	const navlinks = config.navigationLinks;

	const getBlockStyle = () => {
		if ( opened ) {
			return { display: 'block' }
		}
		return {}
	}

	const closeLinksBar = () => {
		const body = document.querySelector('body');
		if ( !body ) { return; }
		body.onclick = null;
		setOpened(false);
	}
	const openLinksBar = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if ( !body ) { return; }
			body.onclick = (e: any) => {
				const _path = e.composedPath() || e.path;
				if ( !linksBlockRef.current ) { return; }
				if ( _path && _path.includes(linksBlockRef.current) ) { return; }
				closeLinksBar();
			};
		}, 100);

		setOpened(true);
	}

	return (
		<React.Fragment>
			<button
				className="s-header__nav-toggle"
				onClick={() => {
					if ( !opened ) {
						openLinksBar();
					} else {
						closeLinksBar();
					}
				}}
			>
				<span className="burger-lines"></span>
			</button>
			<div
				ref={ linksBlockRef }
				className="s-header__nav"
				style={ getBlockStyle() }
			>
				<div className="container">
					{ navlinks.map((item) => { return getItem(item) }) }
				</div>
			</div>
		</React.Fragment>
	)
}