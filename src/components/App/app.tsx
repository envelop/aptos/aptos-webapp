
import 'tippy.js/dist/tippy.css';

import {
	matchRoutes,
	useNavigate
} from 'react-router-dom';

import Header from '../Header';
import Footer from '../Footer';

import {
	ERC20Dispatcher,
	InfoModalDispatcher,
	Web3Dispatcher
} from '../../dispatchers';

import { PetraWallet } from "petra-plugin-wallet-adapter";
import { AptosWalletAdapterProvider } from "@aptos-labs/wallet-adapter-react";

import WrapPage from '../WrapPage';

export default function App() {

	const navigate = useNavigate();
	const sourceUrlParams = [
		{ path: "/:chainId/:contractAddress/:tokenId" },
		{ path: "/:chainId/:contractAddress" },
		{ path: "/:chainId" },
	];
	const matches = matchRoutes(sourceUrlParams, location);

	const wallets = [new PetraWallet()];

	return (
		<>
			<InfoModalDispatcher>
			<AptosWalletAdapterProvider plugins={wallets} autoConnect={true}>
			<Web3Dispatcher switchChainCallback={(targetChainId: number) => {
				if (
					matches &&
					matches[0] &&
					matches[0].params &&
					matches[0].params.chainId
				) {
					const switchedTo = parseInt(matches[0].params.chainId);
					if ( switchedTo !== targetChainId ) { navigate('/'); }
				}
			}}>
			<ERC20Dispatcher>

				<Header />
				<WrapPage />
				<Footer />

			</ERC20Dispatcher>
			
			</Web3Dispatcher>
			</AptosWalletAdapterProvider>
			</InfoModalDispatcher>
		</>
	)

}