
import {
	NFT,
	_AssetType,
	_Fee,
	_Lock,
	_Royalty,
	decodeAssetTypeFromIndex
} from '../_types';
import {
	BigNumber,
	combineURLs,
	processSwarmUrl
} from '../_utils';
import { fetchTokenJSON } from '../fetchtokenwrapper';
import {
	createAuthToken,
	requestTokenURIUpdate
} from './common';

export type API_NFT = {
	blocknumber: string,
	logindex: string,
	contract_address: string,
	token_id: string,
	owner: string,
	token_uri: string,
	asset_type: string,
	is_wnft?: boolean,
	balance?: string,
	in_contract_address?: string,
	in_token_id?: string,
	in_asset_type?: string,
	totalSupply?: string
}

const _decodeNFTFromAPIWithJSON = async (tokenToParse: API_NFT, chainId: number): Promise<NFT> => {

	const tokenUrlData = await fetchTokenJSON(tokenToParse.token_uri);
	if ( !tokenUrlData ) {
		return _decodeNFTFromAPI(tokenToParse, chainId);
	}
	const baseData = await _decodeNFTFromAPI(tokenToParse, chainId);

	return {
		...baseData,
		...tokenUrlData,
	}
}
const _decodeNFTFromAPI = async (tokenToParse: API_NFT, chainId: number): Promise<NFT> => {

	const tokenUrlRaw = tokenToParse.token_uri;
	const tokenUrl = processSwarmUrl(tokenUrlRaw);

	return {
		assetType      : decodeAssetTypeFromIndex(`${tokenToParse.asset_type}`),
		contractAddress: tokenToParse.contract_address,
		chainId        : chainId,
		owner          : tokenToParse.owner,
		tokenId        : tokenToParse.token_id,
		amount         : tokenToParse.balance     ? new BigNumber(tokenToParse.balance)     : undefined,
		totalSupply    : tokenToParse.totalSupply ? new BigNumber(tokenToParse.totalSupply) : undefined,
		tokenUrl,
		tokenUrlRaw,
		blockNumber: tokenToParse.blocknumber,
		logIndex: tokenToParse.logindex,
	}
}

export const getNFTByIdFromAPI = async (
	chainId: number,
	contractAddress: string,
	tokenId: string,
	userAddress?: string,
	assetType?: _AssetType
):Promise<NFT | undefined> => {

	if ( contractAddress === '' ) { throw new Error('Empty contract address'); }
	if ( tokenId === ''         ) { throw new Error('Empty token id'); }

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		throw new Error('Cannot create token');
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return undefined; }

	const url721 = combineURLs(BASE_URL, `/discover/721/${chainId}/${contractAddress}/${tokenId}`);
	const url1155 = combineURLs(BASE_URL, `/discover/1155/${chainId}/${contractAddress}/${tokenId}`);

	let respParsed: Array<API_NFT> | undefined;

	let url = '';
	try {
		if ( assetType ) {
			// exact type
			if ( assetType === _AssetType.ERC721 ) {
				url = url721;
			}
			if ( assetType === _AssetType.ERC1155 ) {
				url = url1155;
			}

			const resp = await fetch(url, {
				headers: {
					'Authorization': authToken,
				}
			});
			respParsed = await resp.json();
		}
		if ( !assetType ) {
			// unknown type

			// erc721
			const resp = await fetch(url721, {
				headers: {
					'Authorization': authToken,
				}
			});
			if ( resp && resp.ok ) {
				respParsed = await resp.json();
			}

			// erc1155
			if ( !respParsed || !respParsed.length || 'error' in respParsed ) {
				const resp = await fetch(url1155, {
					headers: {
						'Authorization': authToken,
					}
				});
				if ( resp && resp.ok ) {
					respParsed = await resp.json();
				}
			}

			if ( !respParsed || !respParsed.length || 'error' in respParsed ) {
				console.log('Cannot find token niether 721 nor 1155 from oracle');
				return undefined;
			}
		}
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		throw new Error('Cannot fetch token from oracle');
	}

	if ( !respParsed || 'error' in respParsed ) { return undefined; }

	let tokenToParse: API_NFT | undefined = undefined;
	if ( userAddress ) {
		tokenToParse = respParsed.find((item: API_NFT) => {
			return item.owner.toLowerCase() === userAddress.toLowerCase()
		});
	} else {
		if ( respParsed[0] ) {
			tokenToParse = {
				...respParsed[0],
				balance: '0',
			};
		}
	}

	if ( !tokenToParse ) { return undefined; }

	return _decodeNFTFromAPIWithJSON(tokenToParse, chainId);
}

export const getUserNFTsFromAPI = async (
	chainId: number,
	assetType: _AssetType,
	userAddress: string,
	page: number,
	options?: {
		withJSON?: boolean,
		filterWNFTS?: boolean,
		tokensOnPage?: number,
	}
):Promise<Array<NFT>> => {

	const withJSON = options && options.withJSON !== undefined ? options.withJSON : false;
	const filterWNFTS = options && options.filterWNFTS !== undefined ? options.filterWNFTS : false;
	const tokensOnPage = options && options.tokensOnPage !== undefined ? options.tokensOnPage : 12;

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		return [];
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return []; }

	let url = '';
	if ( assetType === _AssetType.ERC721 ) {
		url = combineURLs(BASE_URL, `/discover/721/user/${chainId}/${userAddress}?page=${page}&size=${tokensOnPage}`);
	}
	if ( assetType === _AssetType.ERC1155 ) {
		url = combineURLs(BASE_URL, `/discover/1155/user/${chainId}/${userAddress}?page=${page}&size=${tokensOnPage}`);
	}

	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		let respParsed: Array<API_NFT> = await resp.json();
		if ( filterWNFTS ) {
			respParsed = respParsed.filter((item) => { return item.is_wnft === undefined || item.is_wnft === false });
		}
		if ( !resp.ok ) {
			console.log('Cannot fetch token from oracle', respParsed);
			return [];
		}
		if ('error' in respParsed) {
			return [];
		}

		if ( withJSON ) {
			return await Promise.all(respParsed.map(async (item) => { return await _decodeNFTFromAPIWithJSON(item, chainId) }));
		} else {
			return await Promise.all(respParsed.map(async (item) => { return await _decodeNFTFromAPI(item, chainId) }));
		}
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		return [];
	}
}
export const getUserNFTsOfContractFromAPI = async (
	chainId: number,
	assetType: _AssetType,
	userAddress: string,
	contractAddress: string,
	page: number,
	options?: {
		withJSON?: boolean,
		filterWNFTS?: boolean,
		tokensOnPage?: number,
	}
):Promise<Array<NFT>> => {

	const withJSON = options && options.withJSON !== undefined ? options.withJSON : false;
	const filterWNFTS = options && options.filterWNFTS !== undefined ? options.filterWNFTS : false;
	const tokensOnPage = options && options.tokensOnPage !== undefined ? options.tokensOnPage : 12;

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		return [];
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return []; }

	let url = '';
	if ( assetType === _AssetType.ERC721 ) {
		url = combineURLs(BASE_URL, `/discover/721/user/${chainId}/${userAddress}/${contractAddress}?page=${page}&size=${tokensOnPage}`);
	}
	if ( assetType === _AssetType.ERC1155 ) {
		url = combineURLs(BASE_URL, `/discover/1155/user/${chainId}/${userAddress}/${contractAddress}?page=${page}&size=${tokensOnPage}`);
	}

	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		let respParsed: Array<API_NFT> = await resp.json();
		if ( filterWNFTS ) {
			respParsed = respParsed.filter((item) => { return item.is_wnft === undefined || item.is_wnft === false });
		}
		if ( !resp.ok ) {
			console.log('Cannot fetch token from oracle', respParsed);
			return [];
		}
		if ('error' in respParsed) {
			return [];
		}

		if ( withJSON ) {
			return await Promise.all(respParsed.map(async (item) => { return await _decodeNFTFromAPIWithJSON(item, chainId) }));
		} else {
			return await Promise.all(respParsed.map(async (item) => { return await _decodeNFTFromAPI(item, chainId) }));
		}
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		return [];
	}
}