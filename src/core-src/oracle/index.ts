
import {
	createAuthToken,
	requestTokenUpdate,
	requestTokenURIUpdate,
	getChainParamsFromAPI,
	getChainParamsAllFromAPI,
	chainTypeToERC20,
} from './common';

import {
	getERC20ParamsFromAPI
} from './erc20';

import {
	getNFTByIdFromAPI,
	getUserNFTsFromAPI,
	getUserNFTsOfContractFromAPI,
} from './nft';

import {
	getWNFTByIdFromAPI,
	getUserWNFTsFromAPI,
} from './wnft';

import {
	getDisplayFromAPI,
	getDisplaysOfUserFromAPI,
	getDisplayParamsFromAPI,
} from './kiosk';

export {
	createAuthToken,
	requestTokenUpdate,
	requestTokenURIUpdate,

	getChainParamsFromAPI,
	getChainParamsAllFromAPI,
	chainTypeToERC20,

	getERC20ParamsFromAPI,

	getNFTByIdFromAPI,
	getUserNFTsFromAPI,
	getUserNFTsOfContractFromAPI,

	getWNFTByIdFromAPI,
	getUserWNFTsFromAPI,

	getDisplayFromAPI,
	getDisplaysOfUserFromAPI,
	getDisplayParamsFromAPI,
}
