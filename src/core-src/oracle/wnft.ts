
import { fetchTokenJSON, fillCollateralsData } from '../fetchtokenwrapper';
import {
	WNFT,
	_AssetType,
	_Fee,
	_Lock,
	_Royalty,
	decodeAssetTypeFromIndex,
	decodeWrappedToken,
	_AssetItem
} from '../_types';
import {
	BigNumber,
	combineURLs
} from '../_utils';
import {
	createAuthToken, requestTokenURIUpdate,
} from './common';

export type API_WNFT = {
	collateral_json: Array<{
		amount: number,
		tokenId: number,
		assetType: number,
		contractAddress: string
	}>,
	contract_address: string,
	token_id: string,
	token_uri: string,
	owner: string,
	blocknumber: string,
	logindex: string,
	wnft_type: _AssetType,
	balance: number | undefined,
	initial_out_balance: BigNumber,
	in_asset_type: number,
	in_contract_address: string,
	in_token_id: string,
	in_amount: BigNumber,
	unwrap_destination: string,
	rules: string,
	is_burned: boolean,
	fees: Array<_Fee>,
	locks: Array<_Lock>,
	royalties: Array<_Royalty>,
	first_owner: string,
	create_tx: string,
	burn_tx: string,
	inserted: string,
	updated: string,
	updated_by: string,
	asset_type: _AssetType,
	is_wnft: boolean,
	totalSupply: string | undefined,
}

const decodeWNFTFromAPIWithJSON = async (item: API_WNFT, chainId: number): Promise<WNFT> => {
	const inAsset = {
		asset: {
			assetType: decodeAssetTypeFromIndex(`${item.in_asset_type}`),
			contractAddress: item.in_contract_address,
		},
		tokenId: `${item.in_token_id}`,
		amount: `${item.in_amount}`,
	};

	let collateral: Array<_AssetItem> = [];
	if ( item.collateral_json ) {
		collateral = item.collateral_json.map((iitem): _AssetItem => {
			return {
				asset: {
					assetType: decodeAssetTypeFromIndex(`${iitem.assetType}`),
					contractAddress: iitem.contractAddress,
				},
				tokenId: `${iitem.tokenId}`,
				amount: `${iitem.amount}`,
			}
		})
	}

	const amount      = item.balance ? new BigNumber(item.balance) : undefined;
	const totalSupply = item.totalSupply ? new BigNumber(item.totalSupply) : undefined;

	let tokenJSONData: any = {};
	if ( item.token_uri ) {
		tokenJSONData = await fetchTokenJSON(item.token_uri);
	} else {
		requestTokenURIUpdate(chainId, item.contract_address, item.token_id);
	}

	const outData = await fillCollateralsData(await decodeWrappedToken({
		inWNFT: {
			inAsset,
			collateral,
			unWrapDestinition: item.unwrap_destination,
			unWrapDestination: item.unwrap_destination,
			fees: item.fees,
			locks: item.locks,
			royalties: item.royalties,
			rules: item.rules,
		},
		owner: item.owner,
		chainId: chainId,
		contractAddress: item.contract_address,
		tokenId: item.token_id,
		assetType: item.asset_type,
		amount,
		totalSupply,
		tokenUrl: item.token_uri,
		blockNumber: item.blocknumber,
		logIndex: item.logindex,
		createTx: item.create_tx,
	}));

	return {
		...outData,
		...tokenJSONData,
	}
}
const decodeWNFTFromAPI = async (item: API_WNFT, chainId: number): Promise<WNFT> => {
	const inAsset = {
		asset: {
			assetType: decodeAssetTypeFromIndex(`${item.in_asset_type}`),
			contractAddress: item.in_contract_address,
		},
		tokenId: `${item.in_token_id}`,
		amount: `${item.in_amount}`,
	};

	let collateral: Array<_AssetItem> = [];
	if ( item.collateral_json ) {
		collateral = item.collateral_json.map((iitem): _AssetItem => {
			return {
				asset: {
					assetType: decodeAssetTypeFromIndex(`${iitem.assetType}`),
					contractAddress: iitem.contractAddress,
				},
				tokenId: `${iitem.tokenId}`,
				amount: `${iitem.amount}`,
			}
		})
	}

	const amount      = item.balance ? new BigNumber(item.balance) : undefined;
	const totalSupply = item.totalSupply ? new BigNumber(item.totalSupply) : undefined;

	return await fillCollateralsData(await decodeWrappedToken({
		inWNFT: {
			inAsset,
			collateral,
			unWrapDestinition: item.unwrap_destination,
			unWrapDestination: item.unwrap_destination,
			fees: item.fees,
			locks: item.locks,
			royalties: item.royalties,
			rules: item.rules,
		},
		owner: item.owner,
		chainId: chainId,
		contractAddress: item.contract_address,
		tokenId: item.token_id,
		assetType: item.asset_type,
		amount,
		totalSupply,
		tokenUrl: item.token_uri,
		blockNumber: item.blocknumber,
		logIndex: item.logindex,
		createTx: item.create_tx,
	}))
}
const decodeWNFTFromAPIBatch = async (tokens: Array<API_WNFT>, chainId: number, withJSON?: boolean): Promise<Array<WNFT>> => {
	if ( withJSON ) {
		return await Promise.all(tokens.map((item: API_WNFT): Promise<WNFT> => { return decodeWNFTFromAPIWithJSON(item, chainId) }));
	} else {
		return await Promise.all(tokens.map((item: API_WNFT): Promise<WNFT> => { return decodeWNFTFromAPI(item, chainId) }));
	}
}

export const getWNFTByIdFromAPI = async (
	chainId: number,
	contractAddress: string,
	tokenId: string,
	userAddress?: string,
	assetType?: _AssetType
):Promise<WNFT | undefined> => {

	if ( contractAddress === '' ) { throw new Error('Empty contract address'); }
	if ( tokenId === ''         ) { throw new Error('Empty token id'); }

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		throw new Error('Cannot create token');
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}

	const url721 = combineURLs(BASE_URL, `/wnft_collateral/721/${chainId}/${contractAddress}/${tokenId}`);
	const url1155 = combineURLs(BASE_URL, `/wnft_collateral/1155/${chainId}/${contractAddress}/${tokenId}`);

	let respParsed;

	let url = '';
	try {
		if ( assetType ) {
			if ( assetType === _AssetType.ERC721 ) {
				url = url721;
			}
			if ( assetType === _AssetType.ERC1155 ) {
				url = url1155;
			}
			const resp = await fetch(url, {
				headers: {
					'Authorization': authToken,
				}
			});
			respParsed = await resp.json();
		} else {

			const resp = await fetch(url721, {
				headers: {
					'Authorization': authToken,
				}
			});
			if ( resp && resp.ok ) {
				respParsed = await resp.json();
			}

			if ( !respParsed || !respParsed.length || 'error' in respParsed ) {

				const resp = await fetch(url1155, {
					headers: {
						'Authorization': authToken,
					}
				});
				if ( resp && resp.ok ) {
					respParsed = await resp.json();
				}

			}
		}

		if ( !respParsed || !respParsed.length || 'error' in respParsed ) {
			console.log('Cannot find token niether 721 nor 1155 from oracle');
			return undefined;
		}
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		throw new Error('Cannot fetch token from oracle');
	}

	const foundUserToken = respParsed.find((item: API_WNFT) => {
		if ( !userAddress ) { return false; }
		return item.owner.toLowerCase() === userAddress.toLowerCase()
	});

	if ( foundUserToken ) {
		return await decodeWNFTFromAPIWithJSON(foundUserToken, chainId);
	}

	return await decodeWNFTFromAPIWithJSON(respParsed[0], chainId);

}

export const getUserWNFTsFromAPI = async (
	chainId: number,
	assetType: _AssetType,
	userAddress: string,
	page: number,
	options?: {
		tokensOnPage?: number,
		withJSON?: boolean,
	}
):Promise<Array<WNFT>> => {

	const tokensOnPage = options && options.tokensOnPage !== undefined ? options.tokensOnPage : 12;
	const withJSON = options && options.withJSON !== undefined ? options.withJSON : false;

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		return [];
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return []; }

	let url = '';
	if ( assetType === _AssetType.ERC721 ) {
		url = combineURLs(BASE_URL, `/wnft_collateral/721/user/${chainId}/${userAddress}?page=${page}&size=${tokensOnPage}`);
	}
	if ( assetType === _AssetType.ERC1155 ) {
		url = combineURLs(BASE_URL, `/wnft_collateral/1155/user/${chainId}/${userAddress}?page=${page}&size=${tokensOnPage}`);
	}

	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		const respParsed: Array<API_WNFT> = await resp.json();
		if ( !resp.ok ) {
			console.log('Cannot fetch token from oracle', respParsed);
			return [];
		}
		if ('error' in respParsed) {
			return [];
		}

		if ( withJSON ) {
			return await decodeWNFTFromAPIBatch(respParsed, chainId, true);
		} else {
			return await decodeWNFTFromAPIBatch(respParsed, chainId, false);
		}
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		return [];
	}
}