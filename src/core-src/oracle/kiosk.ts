import {
	DisplayParam,
	_Display,
	_DisplayParamType
} from "../_types";
import { combineURLs } from "../_utils";
import { createAuthToken } from "./common";

export const getDisplayFromAPI = async (
	chainId: number,
	kioskAddress: string,
	displayHash: string,
):Promise<_Display | undefined> => {

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		const err = 'Cannot create auth token';
		console.log(err);
		throw new Error(err);
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return undefined; }
	const url = combineURLs(BASE_URL, `/kiosk/display/${chainId}/${kioskAddress}/${displayHash}`);

	let respParsed: _Display | undefined = undefined;
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
		if (!respParsed || 'error' in respParsed) {
			console.log('Cannot fetch display');
			throw new Error('Cannot fetch display');
		}
	} catch (e) {
		console.log('Cannot fetch display', e);
		throw new Error('Cannot fetch display');
	}

	if ( !respParsed ) {
		console.log('Cannot parse display');
		throw new Error('Cannot parse display');
	}

	return respParsed;
}

export const getDisplayParamsFromAPI = async (
	chainId: number,
	kioskAddress: string,
	displayHash: string,
):Promise<Array<DisplayParam>> => {

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		const err = 'Cannot create auth token';
		console.log(err);
		throw new Error(err);
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return []; }
	const url = combineURLs(BASE_URL, `/kiosk/display_params/${chainId}/${kioskAddress}/${displayHash}`);

	let respParsed: Array<DisplayParam> = [];
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
		if (!respParsed || 'error' in respParsed) {
			console.log('Cannot fetch display');
			throw new Error('Cannot fetch display');
		}
	} catch (e) {
		console.log('Cannot fetch display', e);
		throw new Error('Cannot fetch display');
	}

	if ( !respParsed ) {
		console.log('Cannot parse display');
		throw new Error('Cannot parse display');
	}

	return respParsed;
}

export const getDisplaysOfUserFromAPI = async (
	chainId: number,
	userAddress: string,
	page: number,
	size = 12,
):Promise<Array<_Display> | undefined> => {

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		const err = 'Cannot create auth token';
		console.log(err);
		throw new Error(err);
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return undefined; }
	const url = combineURLs(BASE_URL, `/kiosk/user/${chainId}/${userAddress}?page=${page}&size=${size}`);

	let respParsed: Array<_Display> | undefined = undefined;
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
		if (!respParsed || 'error' in respParsed) {
			console.log('Cannot fetch displays');
			throw new Error('Cannot fetch displays');
		}
	} catch (e) {
		console.log('Cannot fetch displays', e);
		throw new Error('Cannot fetch displays');
	}

	if ( !respParsed ) {
		console.log('Cannot parse displays');
		throw new Error('Cannot parse displays');
	}

	return respParsed;
}
