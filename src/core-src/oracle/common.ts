import { BigNumber, combineURLs } from '../_utils';
import {
	ChainType,
	ERC20Type,
	_AssetItem,
	_AssetType,
	_Fee,
	_Lock,
	_Royalty,
} from '../_types';

import default_icon from '../../core-pics/networks/_default.png';
import default_coin from '../../core-pics/coins/_default.svg';

import { _getCacheItem, _setCacheItem } from '..';

export const createAuthToken = async () => {
	async function sha256(message: string) {
		const msgBuffer = new TextEncoder().encode(message);
		const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);
		const hashArray = Array.from(new Uint8Array(hashBuffer));
		const hashHex = hashArray
			.map((b) => b.toString(16).padStart(2, '0'))
			.join('');
		return hashHex;
	}
	async function signTimed(name: string, key: string) {
		const now = new Date().getTime();
		const timeBlock = parseInt(`${now / (parseInt(key_active || '0') * 1000)}`);

		return sha256(name + key + timeBlock);
	}

	const app_name = window.location.host;
	const app_id = process.env.REACT_APP_ORACLE_APP_ID;
	const app_key = process.env.REACT_APP_ORACLE_APP_KEY;
	const key_active = process.env.REACT_APP_ORACLE_KEY_ACTIVE_TIME;

	if (!app_id || !app_key || !key_active) {
		console.log('No app_id or app_key of key_active in .env');
		return '';
	}
	const tempKey = await signTimed(app_name, app_key);

	return `${app_id}.${tempKey}`;
};

export const requestTokenUpdate = async (
	chainId: number,
	assetType: _AssetType,
	contractAddress: string,
) => {
	if (contractAddress === '0x0000000000000000000000000000000000000000') {
		console.log('Cannot fetch native token');
		return undefined;
	}
	const authToken = await createAuthToken();
	if (authToken === '') {
		console.log('Cannot create token');
		throw new Error('Cannot create token');
	}
	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if (!BASE_URL) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(
		BASE_URL,
		`/update_token/${chainId}/${assetType}/${contractAddress}`,
	);

	const resp = await fetch(url, {
		headers: { Authorization: authToken },
	});

	if (resp && resp.ok) {
		return true;
	}

	console.log(
		`Cannot update token: ${chainId}/${assetType}/${contractAddress}`,
	);
	throw new Error(
		`Cannot update token: ${chainId}/${assetType}/${contractAddress}`,
	);
};
export const requestTokenURIUpdate = async (
	chainId: number,
	contractAddress: string,
	tokenId: string,
) => {
	console.log(
		'TokenURI update requested for',
		chainId,
		contractAddress,
		tokenId,
	);

	if (contractAddress === '0x0000000000000000000000000000000000000000') {
		console.log('Cannot fetch native token');
		return undefined;
	}
	const authToken = await createAuthToken();
	if (authToken === '') {
		console.log('Cannot create token');
		throw new Error('Cannot create token');
	}
	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if (!BASE_URL) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(
		BASE_URL,
		`/update_token_uri/${chainId}/${contractAddress}/${tokenId}`,
	);

	const resp = await fetch(url, {
		headers: { Authorization: authToken },
	});

	if (resp && resp.ok) {
		return true;
	}

	console.log(
		`Cannot update token URI: ${chainId}/${contractAddress}/${tokenId}`,
	);
	throw new Error(
		`Cannot update token URI: ${chainId}/${contractAddress}/${tokenId}`,
	);
};
export const chainTypeToERC20 = (chain: ChainType): ERC20Type => {
	return {
		contractAddress: '0x0000000000000000000000000000000000000000',
		name: chain.symbol,
		decimals: chain.decimals,
		assetType: _AssetType.native,
		symbol: chain.symbol,
		icon: chain.tokenIcon || default_coin,
		balance: new BigNumber(0),
		allowance: [
			{
				allowanceTo: '0x0000000000000000000000000000000000000000',
				amount: new BigNumber(0),
			},
		],
	};
};
const parseChain = (resp: ChainType): ChainType => {
	let networkIcon = default_icon;
	try {
		networkIcon = require(`../../core-pics/networks/${resp.chainId}.jpeg`);
	} catch (ignored) {}
	try {
		networkIcon = require(`../../core-pics/networks/${resp.chainId}.jpg`);
	} catch (ignored) {}
	try {
		networkIcon = require(`../../core-pics/networks/${resp.chainId}.png`);
	} catch (ignored) {}
	try {
		networkIcon = require(`../../core-pics/networks/${resp.chainId}.svg`);
	} catch (ignored) {}
	let tokenIcon = default_coin;
	try {
		tokenIcon = require(`../../core-pics/coins/${resp.symbol.toLowerCase()}.jpeg`);
	} catch (ignored) {}
	try {
		tokenIcon = require(`../../core-pics/coins/${resp.symbol.toLowerCase()}.jpg`);
	} catch (ignored) {}
	try {
		tokenIcon = require(`../../core-pics/coins/${resp.symbol.toLowerCase()}.png`);
	} catch (ignored) {}
	try {
		tokenIcon = require(`../../core-pics/coins/${resp.symbol.toLowerCase()}.svg`);
	} catch (ignored) {}
	try {
		tokenIcon = require(`../../core-pics/coins/${resp.chainId}.jpeg`);
	} catch (ignored) {}
	try {
		tokenIcon = require(`../../core-pics/coins/${resp.chainId}.jpg`);
	} catch (ignored) {}
	try {
		tokenIcon = require(`../../core-pics/coins/${resp.chainId}.png`);
	} catch (ignored) {}
	try {
		tokenIcon = require(`../../core-pics/coins/${resp.chainId}.svg`);
	} catch (ignored) {}

	_setCacheItem(`rpcUrl_${parseInt(`${resp.chainId}`)}`, resp.RPCUrl);

	return {
		networkIcon,
		tokenIcon,
		chainId: parseInt(`${resp.chainId}`),
		decimals: parseInt(`${resp.decimals}`),
		name: resp.name,
		symbol: resp.symbol,
		colorCode: resp.colorCode,
		EIPPrefix: resp.EIPPrefix,
		explorerBaseUrl: resp.explorerBaseUrl,
		explorerName: resp.explorerName,
		isTestNetwork: `${resp.isTestNetwork}`.toLowerCase() === 'true',
		RPCUrl: resp.RPCUrl,
		hasOracle: `${resp.hasOracle}`.toLowerCase() === 'true',
	};
};
export const getChainParamsFromAPI = async (
	chainId: number,
): Promise<ChainType> => {
	const authToken = await createAuthToken();
	if (authToken === '') {
		console.log('Cannot create token');
		throw new Error('Cannot create token');
	}
	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if (!BASE_URL) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(BASE_URL, `/chain_info/${chainId}`);

	let respParsed: ChainType | undefined = undefined;
	try {
		const resp = await fetch(url, {
			headers: {
				Authorization: authToken,
			},
		});

		if (resp && resp.ok) {
			respParsed = await resp.json();
		}
	} catch (e) {
		console.log('Cannot fetch chain info', e);
		throw new Error('Cannot fetch chain info');
	}

	if (!respParsed) {
		console.log('Cannot fetch chain info');
		throw new Error('Cannot fetch chain info');
	}

	return parseChain(respParsed);
};
export const getChainParamsAllFromAPI = async (): Promise<Array<ChainType>> => {
	const cached = _getCacheItem('availableChains');
	if (cached) {
		return cached;
	}

	const authToken = await createAuthToken();
	if (authToken === '') {
		console.log('Cannot create token');
		throw new Error('Cannot create token');
	}
	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if (!BASE_URL) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(BASE_URL, `/chain_info/`);

	let respParsed: Array<ChainType> | undefined = undefined;
	try {
		const resp = await fetch(url, {
			headers: {
				Authorization: authToken,
			},
		});

		if (resp && resp.ok) {
			respParsed = await resp.json();
		}
	} catch (e) {
		console.log('Cannot fetch chain info', e);
		throw new Error('Cannot fetch chain info');
	}

	if (!respParsed) {
		console.log('Cannot fetch chain info');
		throw new Error('Cannot fetch chain info');
	}

	const output = respParsed.map((item) => {
		return parseChain(item);
	});
	_setCacheItem('availableChains', output);

	return output;
};
