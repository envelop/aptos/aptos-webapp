import { BigNumber, combineURLs } from '../_utils';
import default_icon from '../../core-pics/coins/_default.svg';
import { Allowance, ERC20Type, _AssetType } from '../_types';
import { createAuthToken, requestTokenUpdate } from '.';

type APIERC20Item = {
	chain_id: number;
	contract_address: string;
	contract_type: string;
	abi: string;
	name: string;
	symbol: string;
	decimals: string;
};

export const getERC20ParamsFromAPI = async (
	chainId: number,
	contractAddress: string,
): Promise<ERC20Type | undefined> => {
	if (contractAddress === '0x0000000000000000000000000000000000000000') {
		console.log('Cannot fetch native token');
		return undefined;
	}
	const authToken = await createAuthToken();
	if (authToken === '') {
		console.log('Cannot create token');
		throw new Error('Cannot create token');
	}
	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if (!BASE_URL) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(BASE_URL, `/erc20/${chainId}/${contractAddress}`);

	let respParsed: APIERC20Item | undefined = undefined;
	try {
		const resp = await fetch(url, {
			headers: {
				Authorization: authToken,
			},
		});

		if (resp.status === 404) {
			requestTokenUpdate(chainId, _AssetType.ERC20, contractAddress);
			return undefined;
		}

		if (resp && resp.ok) {
			respParsed = await resp.json();
		}
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		throw new Error('Cannot fetch token from oracle');
	}

	if (!respParsed) {
		console.log('Cannot fetch token from oracle');
		throw new Error('Cannot fetch token from oracle');
	}

	let balance = new BigNumber(0);
	let allowance: Array<Allowance> = [];

	let icon = default_icon;
	try {
		icon = require(`../../core-pics/coins/${respParsed.symbol.toLowerCase()}.jpeg`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${respParsed.symbol.toLowerCase()}.jpg`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${respParsed.symbol.toLowerCase()}.png`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${respParsed.symbol.toLowerCase()}.svg`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${contractAddress.toLowerCase()}.jpeg`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${contractAddress.toLowerCase()}.jpg`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${contractAddress.toLowerCase()}.png`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${contractAddress.toLowerCase()}.svg`);
	} catch (ignored) {}
	return {
		contractAddress,
		assetType: _AssetType.ERC20,
		name: respParsed.name,
		symbol: respParsed.symbol,
		decimals: parseInt(respParsed.decimals),
		icon,
		balance,
		allowance,
	};
};
