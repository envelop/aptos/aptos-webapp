import Web3 from 'web3';
import config from '../app.config.json';

import BigNumber from 'bignumber.js';
import { _getCacheItem, _setCacheItem, getDefaultWeb3 } from '..';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100 });
export { BigNumber };

export const combineURLs = (baseURL: string, relativeURL: string) => {
	return relativeURL
		? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
		: baseURL;
};

export const getABI = async (
	chainId: number,
	contractAddress: string,
	typeName?: string,
) => {
	const abisCached: Array<{ name: string; abi: any }> =
		_getCacheItem('abis') || [];
	const abiFound = abisCached.find((item) => {
		return (
			item.name.toLowerCase() === contractAddress.toLowerCase() ||
			(typeName && item.name.toLowerCase() === typeName.toLowerCase())
		);
	});
	if (abiFound) {
		return abiFound.abi;
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if (BASE_URL) {
		// try to get abi by chain/address
		const addressUrl = combineURLs(
			BASE_URL,
			`/abis/${chainId}/${contractAddress.toLowerCase()}.json`,
		);
		try {
			const addressResp = await fetch(addressUrl);

			if (addressResp.ok) {
				try {
					const abiParsed = await addressResp.json();
					_setCacheItem('abis', [
						...(_getCacheItem('abis') || []).filter(
							(item: { name: string; aby: any }) => {
								return (
									item.name.toLowerCase() !== contractAddress.toLowerCase()
								);
							},
						),
						{ name: contractAddress, abi: abiParsed },
					]);
					return abiParsed;
				} catch (ignored) {}
			}
		} catch (e: any) {
			console.log('Cannot fetch abi', contractAddress, typeName, e);
		}

		if (typeName) {
			// try to get abi by type name
			const typeUrl = combineURLs(
				BASE_URL,
				`/abis/${typeName.toLowerCase()}.json`,
			);
			try {
				const typeResp = await fetch(typeUrl);

				if (typeResp.ok) {
					try {
						const abiParsed = await typeResp.json();
						_setCacheItem('abis', [
							...(_getCacheItem('abis') || []).filter(
								(item: { name: string; aby: any }) => {
									return item.name.toLowerCase() !== typeName.toLowerCase();
								},
							),
							{ name: typeName, abi: abiParsed },
						]);
						return abiParsed;
					} catch (ignored) {}
				}
			} catch (e: any) {
				console.log('Cannot fetch abi', contractAddress, typeName, e);
			}
		}
	}

	if (typeName) {
		try {
			const localAbi = require(`../../abis/${typeName.toLowerCase()}.json`);
			_setCacheItem('abis', [
				...(_getCacheItem('abis') || []).filter(
					(item: { name: string; aby: any }) => {
						return item.name.toLowerCase() !== typeName.toLowerCase();
					},
				),
				{ name: typeName, abi: localAbi },
			]);
			return localAbi;
		} catch (ignored) {}
	}

	throw new Error(`Cannot load ${chainId}/${contractAddress} abi`);
};

export const tokenToFloat = (
	value: BigNumber,
	decimals?: number,
): BigNumber => {
	if (!decimals || decimals === 0) {
		return value;
	}
	const decimalsToParse = decimals;
	return value.multipliedBy(10 ** -decimalsToParse);
};
export const tokenToInt = (value: BigNumber, decimals?: number): BigNumber => {
	const decimalsToParse = decimals || 8;
	return value.multipliedBy(10 ** decimalsToParse);
};

export const addThousandSeparator = (numStr: string): string => {
	const parts = numStr.split('.');
	return (
		parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ') +
		(parts[1] ? '.' + parts[1] : '') +
		(numStr.endsWith('.') ? '.' : '')
	);
};
export const removeThousandSeparator = (numStr: string): string => {
	return numStr.replaceAll(',', '.').replaceAll(' ', '');
};

export const compactString = (
	str: string,
	chars?: number,
	saveFirst?: number,
) => {
	if (!str) {
		return str;
	}
	const useChars = chars || 3;

	str = `${str}`;
	if (saveFirst && str.length < useChars * 2 + 2 + saveFirst) {
		return str;
	}
	if (str.length < useChars * 2 + 2) {
		return str;
	}

	if (saveFirst) {
		return `${str.slice(0, useChars + saveFirst)}...${str.slice(-useChars)}`;
	}

	return `${str.slice(0, useChars)}...${str.slice(-useChars)}`;
};

export const convertRemainingTimeToStr = (diff: BigNumber) => {
	const monthsRemaining = diff
		.dividedToIntegerBy(3600)
		.dividedToIntegerBy(24)
		.dividedToIntegerBy(30);
	if (monthsRemaining.gt(0)) {
		if (monthsRemaining.eq(1)) {
			return `${monthsRemaining.toFixed(0)} month`;
		}
		return `${monthsRemaining.toFixed(0)} months`;
	}

	const daysRemaining = diff.dividedToIntegerBy(3600).dividedToIntegerBy(24);
	if (daysRemaining.gt(0)) {
		if (daysRemaining.eq(1)) {
			return `${daysRemaining.toFixed(0)} day`;
		}
		return `${daysRemaining.toFixed(0)} days`;
	}

	const hoursRemaining = diff.dividedToIntegerBy(3600);
	if (hoursRemaining.gt(0)) {
		if (hoursRemaining.eq(1)) {
			return `${hoursRemaining.toFixed(0)} hour`;
		}
		return `${hoursRemaining.toFixed(0)} hours`;
	}

	const minutesRemaining = diff.dividedToIntegerBy(60);
	if (minutesRemaining.gt(0)) {
		if (minutesRemaining.eq(1)) {
			return `${minutesRemaining.toFixed(0)} minute`;
		}
		return `${minutesRemaining.toFixed(0)} minutes`;
	}

	if (diff.eq(1)) {
		return `${diff.toFixed(0)} second`;
	}
	return `${diff.toFixed(0)} seconds`;
};

export const processSwarmUrl = (url: string) => {
	const swarmBaseUrls = config.SWARM_BASE_URLS;
	try {
		for (let idx = 0; idx < swarmBaseUrls.length; idx++) {
			const element = swarmBaseUrls[idx];
			if (url.startsWith(element.prefix)) {
				const tail = url.replaceAll(element.prefix, '');
				return combineURLs(element.baseUrl, tail);
			}
		}

		return url;
	} catch (ignored) {
		return url;
	}
};

export const localStorageGet = (key: string): string => {
	let output = null;
	try {
		output = localStorage.getItem(key);
	} catch (ignored) {}
	return output || '';
};
export const localStorageSet = (key: string, value: string): void => {
	try {
		localStorage.setItem(key, value);
	} catch (ignored) {}
};
export const localStorageRemove = (key: string): void => {
	try {
		localStorage.removeItem(key);
	} catch (ignored) {}
};

export const getStrHash = (str: string): string => {
	const web3 = new Web3();
	const encodedStr = web3.eth.abi.encodeParameter('string', str);
	return web3.utils.keccak256(encodedStr);
};

export const waitUntilAsync = async (
	condition: () => Promise<boolean>,
	interval: number,
	tries: number,
) => {
	return await new Promise((resolve, reject) => {
		let currentTry = 0;
		const timer = setInterval(async () => {
			if (await condition()) {
				resolve('success');
				clearInterval(timer);
			} else {
				currentTry += 1;
			}

			if (currentTry >= tries) {
				clearInterval(timer);
				reject('failed');
			}
		}, interval);
	});
};
export const waitUntil = async (
	condition: () => boolean,
	interval: number,
	tries: number,
) => {
	return await new Promise((resolve, reject) => {
		let currentTry = 0;
		const timer = setInterval(() => {
			if (condition()) {
				resolve('success');
				clearInterval(timer);
			} else {
				currentTry += 1;
			}

			if (currentTry >= tries) {
				clearInterval(timer);
				reject('failed');
			}
		}, interval);
	});
};

export const rationalize = (
	num: BigNumber,
): { numerator: BigNumber; denominator: BigNumber } => {
	const str = num.toString();

	const precision = str.includes('.')
		? str.split('.')[1].length
		: new BigNumber(0);
	const ten = new BigNumber(10);
	return {
		numerator: num.multipliedBy(ten.pow(precision)),
		denominator: ten.pow(precision),
	};
};

export const getUnixtimeFromBlock = async (
	chainId: number,
	blocknumber: BigNumber,
): Promise<BigNumber | undefined> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		return undefined;
	}

	let blockInfo;
	try {
		blockInfo = await web3.eth.getBlock(blocknumber);
		return new BigNumber(blockInfo.timestamp);
	} catch (ignored) {}

	return undefined;
};
