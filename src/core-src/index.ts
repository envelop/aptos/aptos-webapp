
import Web3 from 'web3';
import {

	ChainType,
	_AssetType,

	ERC20Type,
	ERC20Balance,
	NFT,
	WNFT,
	NFTorWNFT,
	castToWNFT,
	NFTToAssetItem,

	decodeAssetTypeFromString,
	decodeAssetTypeFromIndex,
	assetTypeToString,
	encodeCollaterals,
	decodeCollaterals,
	getNativeCollateral,
	calcCollectedFees,
	encodeRoyalties,
	decodeRoyalties,
	getLockType,
	encodeLocks,
	decodeLocks,
	encodeFees,
	decodeFees,
	encodeRules,
	decodeRules,
	decodeWrappedToken,

	_Asset,
	_AssetItem,
	Allowance,
	WNFTsStat,
	CollateralItem,
	_Royalty,
	Royalty,
	RoyaltyInput,
	_Lock,
	Lock,
	LockType,
	_Fee,
	Fee,
	Rules,
	_WNFT,

	SAFTRecipientItem,

	SubscriptionType,
	SubscriptionPayOption,
	SubscriptionTariff,
	SubscriptionRemainings,

	_Display,
	_Price,
	_DenominatedPrice,
	Price,
	encodePrice,
	decodePrice,
	_DiscountType,
	_Discount,
	Discount,
	decodeDiscountTypeFromIndex,
	discountTypeToString,
	encodeDiscount,
	decodeDiscount,
	_DiscountUntil,
	DiscountUntil,
	encodeDiscountUntil,
	decodeDiscountUntil,

	_KioskAssetItem,
	_DisplayParamType,
	DisplayParam,
} from './_types'

import {
	BigNumber,

	combineURLs,

	getABI,
	tokenToFloat,
	tokenToInt,
	addThousandSeparator,
	removeThousandSeparator,
	compactString,
	convertRemainingTimeToStr,
	processSwarmUrl,

	localStorageGet,
	localStorageSet,
	localStorageRemove,

	getStrHash,
	waitUntil,
	waitUntilAsync,
	rationalize,
	getUnixtimeFromBlock,
} from './_utils'

import {
	createContract,
	checkContractType,
	isAddress,
	getUserAddress,
	getNativeBalance,
	getChainId,
	isContractWNFTFromChain,

	getDefaultWeb3,
	connectSilent,
	connect,
	disconnect,
	requestChainChange,
	walletStateListener,

	getERC20ParamsFromChain,
	getERC20BalanceFromChain,
	makeERC20Allowance,
	makeERC20AllowanceMultisig,

	getBalanceERC1155,
	getBalanceBatchERC1155,
	checkApprovalERC1155,
	setApprovalERC1155,
	transferERC1155,
	getERC1155ByIdFromChain,
	getBalanceERC721,
	checkApprovalERC721,
	checkApprovalForAllERC721,
	setApprovalForAllERC721,
	setApprovalERC721,
	UNSAFE_getERC721sOfContractFromChain,
	getERC721ByIdFromChain,
	transferERC721,
	mintERC721,
	getContractNameERC1155,
	getContractNameERC721,
	setApprovalERC1155Multisig,
	transferERC1155Multisig,
	setApprovalForAllERC721Multisig,
	setApprovalERC721Multisig,
	transferERC721Multisig,
	mintERC721Multisig,

	getWNFTsOfContractFromChain721,
	getWNFTsOfContractFromChain721Batch,
	getBalance721FromChain,
	getBalance1155FromChain,
	getWNFTByIdFromChain,
	addValueToWNFT,
	getWNFTv0TransferModel,
	getWNFTWrapperContract,
	getWrapperTechToken,
	transferWNFT,
	getMaxCollateralSlots,
	getWrapperOfBatchWorker,
	transferWNFTMultisig,
	addValueToWNFTMultisig,

	getTariffsForService,
	getPriceWithFee,
	getUserTicketForService,
	getRegistryOfService,
	canUseService,
	buySubscription,
	buySubscriptionMultisig,
} from './chain';

import {
	getERC20Params,
	getNullERC20,
	getKnownERC20Params,

	getNFTById,
	fetchTokenJSON,

	getWNFTById,

	appendWNFTToStat,
	calcWNFTsStat,
	fillCollateralsData,
} from './fetchtokenwrapper';

import {
	createAuthToken,
	requestTokenUpdate,
	requestTokenURIUpdate,

	getChainParamsFromAPI,
	getChainParamsAllFromAPI,

	getERC20ParamsFromAPI,

	getNFTByIdFromAPI,
	getUserNFTsFromAPI,
	getUserNFTsOfContractFromAPI,

	getWNFTByIdFromAPI,
	getUserWNFTsFromAPI,

	getDisplayFromAPI,
	getDisplaysOfUserFromAPI,
	getDisplayParamsFromAPI,
	chainTypeToERC20,
} from './oracle';

export {
	Web3,

	_AssetType,
	LockType,

	castToWNFT,
	NFTToAssetItem,

	decodeAssetTypeFromString,
	decodeAssetTypeFromIndex,
	assetTypeToString,
	encodeCollaterals,
	decodeCollaterals,
	getNativeCollateral,
	calcCollectedFees,
	encodeRoyalties,
	decodeRoyalties,
	getLockType,
	encodeLocks,
	decodeLocks,
	encodeFees,
	decodeFees,
	encodeRules,
	decodeRules,
	decodeWrappedToken, BigNumber,

	combineURLs,

	getABI,
	tokenToFloat,
	tokenToInt,
	addThousandSeparator,
	removeThousandSeparator,
	compactString,
	convertRemainingTimeToStr,
	processSwarmUrl,

	localStorageGet,
	localStorageSet,
	localStorageRemove,

	getStrHash,
	waitUntil,
	waitUntilAsync,
	rationalize,
	getUnixtimeFromBlock,

	createContract,
	checkContractType,
	isAddress,
	getUserAddress,
	getNativeBalance,
	getChainId,
	isContractWNFTFromChain,

	getDefaultWeb3,
	connectSilent,
	connect,
	disconnect,
	requestChainChange,
	walletStateListener,

	getERC20ParamsFromChain,
	getERC20BalanceFromChain,
	makeERC20Allowance,
	makeERC20AllowanceMultisig,

	getBalanceERC1155,
	getBalanceBatchERC1155,
	checkApprovalERC1155,
	setApprovalERC1155,
	transferERC1155,
	getERC1155ByIdFromChain,
	getBalanceERC721,
	checkApprovalERC721,
	checkApprovalForAllERC721,
	setApprovalForAllERC721,
	setApprovalERC721,
	UNSAFE_getERC721sOfContractFromChain,
	getERC721ByIdFromChain,
	transferERC721,
	mintERC721,
	getContractNameERC1155,
	getContractNameERC721,
	setApprovalERC1155Multisig,
	transferERC1155Multisig,
	setApprovalForAllERC721Multisig,
	setApprovalERC721Multisig,
	transferERC721Multisig,
	mintERC721Multisig,

	getWNFTsOfContractFromChain721,
	getWNFTsOfContractFromChain721Batch,
	getBalance721FromChain,
	getBalance1155FromChain,
	getWNFTByIdFromChain,
	addValueToWNFT,
	getWNFTv0TransferModel,
	getWNFTWrapperContract,
	getWrapperTechToken,
	transferWNFT,
	getMaxCollateralSlots,
	getWrapperOfBatchWorker,
	transferWNFTMultisig,
	addValueToWNFTMultisig,

	getTariffsForService,
	getPriceWithFee,
	getUserTicketForService,
	getRegistryOfService,
	canUseService,
	buySubscription,
	buySubscriptionMultisig,

	getERC20Params,
	getNullERC20,
	getKnownERC20Params,

	getNFTById,
	fetchTokenJSON,
	getWNFTById,

	appendWNFTToStat,
	calcWNFTsStat,
	fillCollateralsData,

	createAuthToken,
	requestTokenUpdate,
	requestTokenURIUpdate,

	getChainParamsFromAPI,
	getChainParamsAllFromAPI,

	getERC20ParamsFromAPI,

	getNFTByIdFromAPI,
	getUserNFTsFromAPI,
	getUserNFTsOfContractFromAPI,

	getWNFTByIdFromAPI,
	getUserWNFTsFromAPI,

	getDisplayFromAPI,
	getDisplaysOfUserFromAPI,
	getDisplayParamsFromAPI,
	chainTypeToERC20,

	_DiscountType, encodePrice,
	decodePrice, decodeDiscountTypeFromIndex,
	discountTypeToString,
	encodeDiscount,
	decodeDiscount, encodeDiscountUntil,
	decodeDiscountUntil
};
export type {
	SAFTRecipientItem,

	SubscriptionType,
	SubscriptionPayOption,
	SubscriptionTariff,
	SubscriptionRemainings, Price, Discount, DiscountUntil
};

export type {
	ChainType,

	ERC20Type,
	ERC20Balance,
	NFT,
	WNFT,
	NFTorWNFT,

	_Asset,
	_AssetItem,
	Allowance,
	WNFTsStat,
	CollateralItem,
	_Royalty,
	Royalty,
	RoyaltyInput,
	_Lock,
	Lock,
	_Fee,
	Fee,
	Rules,
	_WNFT,

	_Display,
	_Price,
	_DenominatedPrice,
	_Discount,
	_DiscountUntil,
	_KioskAssetItem,
	_DisplayParamType,
	DisplayParam,
}

let _sessionCache: Array<{ key: string, data: any }> = [];
export const _setCacheItem = (key: string, data: any) => {
	_sessionCache = [
		..._sessionCache.filter((item: { key: string, data: any }) => { return item.key !== key }),
		{ key, data }
	]
}
export const _getCacheItem = (key: string): any | undefined => {
	const foundItem = _sessionCache.find((item: { key: string, data: any }) => { return item.key === key });
	return foundItem?.data || undefined;
}