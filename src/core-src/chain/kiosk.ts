
import { _Display } from "../_types";
import { createContract } from "./common";
import { getDefaultWeb3 } from "./web3provider";

export const getDisplayFromChain = async (chainId: number, kioskAddress: string, displayHash: string): Promise<_Display> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'kiosk', kioskAddress);
    let output = contract.methods.displays(displayHash).call();

	return output;
}