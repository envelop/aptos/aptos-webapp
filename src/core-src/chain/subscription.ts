
import { createContract } from "./common";
import { getDefaultWeb3 } from "./web3provider";
import { BigNumber } from "../_utils";
import {
	getERC20BalanceFromChain,
	makeERC20AllowanceMultisig
} from "./erc20";
import {
	SubscriptionPayOption,
	SubscriptionRemainings,
	SubscriptionTariff,
} from "../_types";
import Web3 from "web3";
import { PromiEvent } from 'web3-core';
import { Contract } from 'web3-eth-contract';
import { getNativeBalance } from "./common";
import { makeERC20Allowance } from "./erc20";

export const getTariffsForService = async (
	chainId: number,
	registryContractAddress: string,
	agentContractAddress: string,
	serviceContractAddress: string
): Promise<Array<SubscriptionTariff>> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'subregistry', registryContractAddress);
	const fetchedTariffs = await contract.methods.getAvailableAgentsTariffForService(
		agentContractAddress,
		serviceContractAddress,
	).call();

	const tariffsParsed: Array<SubscriptionTariff> = fetchedTariffs[1]
		.map((item: SubscriptionTariff, idx: number) => {
			return { ...item, idx: parseInt(fetchedTariffs[0][idx]) }
		})
		.map((item: SubscriptionTariff) => {
			return {
				...item,
				subscription: {
					...item.subscription,
					timelockPeriod: new BigNumber(item.subscription.timelockPeriod),
					ticketValidPeriod:  new BigNumber(item.subscription.ticketValidPeriod),
				},
				payWith: item.payWith.map((iitem: SubscriptionPayOption, idx: number) => {
					return {
						...iitem,
						agentFeePercent: new BigNumber(iitem.agentFeePercent),
						paymentAmountWithFee: new BigNumber(iitem.paymentAmount),
						paymentAmount: new BigNumber(iitem.paymentAmount),
						idx
					}
				})
			}
		});

	return tariffsParsed;
}

// pass tariff object?
export const getPriceWithFee = async (
	chainId: number,
	registryContractAddress: string,
	serviceContractAddress: string,
	tariffIdx: number,
	payOptionIdx: number
): Promise<BigNumber> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'subregistry', registryContractAddress);
	const price = await contract.methods.getTicketPrice(
		serviceContractAddress,
		tariffIdx,
		payOptionIdx
	).call();

	return new BigNumber(price[1])
}

export const getUserTicketForService = async (
	chainId: number,
	registryContractAddress: string,
	serviceContractAddress: string,
	userAddress: string,
): Promise<SubscriptionRemainings | undefined> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'subregistry', registryContractAddress);
	const userTicket = await contract.methods.getUserTicketForService(serviceContractAddress, userAddress).call();

	if (
		userTicket &&
		userTicket.countsLeft &&
		userTicket.validUntil
	) {
		const validUntil = new BigNumber(userTicket.validUntil);
		const countsLeft = new BigNumber(userTicket.countsLeft)
		const now = new BigNumber(new Date().getTime()).dividedBy(1000);

		if ( countsLeft.gt(0) || validUntil.gt(now) ) {
			return {
				validUntil,
				countsLeft
			};
		}
	}

	return undefined;
}

export const getRegistryOfService = async (
	chainId: number,
	serviceContractAddress: string,
): Promise<string | undefined> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}

	const contract = await createContract(web3, 'subservice', serviceContractAddress);
	let subRegistry;
	try {
		subRegistry = await contract.methods.subscriptionRegistry().call();
		if ( subRegistry === '' ) { return undefined; }
		return subRegistry;
	} catch(e) { return undefined; }

}

export const canUseService = async (
	chainId: number,
	registryContractAddress: string,
	serviceContractAddress: string,
	userAddress: string,
): Promise<boolean> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'subregistry', registryContractAddress);
	return (await contract.methods.getUserTicketForService(serviceContractAddress, userAddress).call()).ok;
}

export const buySubscription = async (
	web3: Web3,
	registryContractAddress: string,
	serviceContractAddress: string,
	agentContractAddress: string,
	tariffIdx: number,
	payOption: SubscriptionPayOption,
	userAddress: string,
	buyFor?: string,
): Promise<PromiEvent<Contract>> => {

	const chainId = await web3.eth.getChainId();

	const priceToPay = await getPriceWithFee(
		chainId,
		registryContractAddress,
		serviceContractAddress,
		tariffIdx,
		payOption.idx
	);

	if ( payOption.paymentToken === '0x0000000000000000000000000000000000000000' ) {
		const balance = await getNativeBalance(
			chainId,
			payOption.paymentToken,
		);
		if ( balance.lt(priceToPay) ) {
			throw new Error('Not enough tokens');
		}
	} else {
		const balance = await getERC20BalanceFromChain(
			chainId,
			payOption.paymentToken,
			userAddress,
			registryContractAddress
		);
		if ( balance.balance.lt(priceToPay) ) {
			throw new Error('Not enough tokens');
		}
		if ( balance.allowance?.amount.lt(priceToPay) ) {
			await makeERC20Allowance(
				web3,
				payOption.paymentToken,
				userAddress,
				priceToPay,
				registryContractAddress
			)
		}
	}

	const contract = await createContract(web3, 'subscriptionAgent', agentContractAddress);

	const buyForParsed = buyFor || userAddress;
	const tx = contract.methods.buySubscription(
		serviceContractAddress,
		tariffIdx,
		payOption.idx,
		buyForParsed,
		userAddress
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null })
}
export const buySubscriptionMultisig = async (
	web3: Web3,
	registryContractAddress: string,
	serviceContractAddress: string,
	agentContractAddress: string,
	tariffIdx: number,
	payOption: SubscriptionPayOption,
	userAddress: string,
	buyFor?: string,
): Promise<PromiEvent<Contract>> => {

	const chainId = await web3.eth.getChainId();

	const priceToPay = await getPriceWithFee(
		chainId,
		registryContractAddress,
		serviceContractAddress,
		tariffIdx,
		payOption.idx
	);

	if ( payOption.paymentToken === '0x0000000000000000000000000000000000000000' ) {
		const balance = await getNativeBalance(
			chainId,
			payOption.paymentToken,
		);
		if ( balance.lt(priceToPay) ) {
			throw new Error('Not enough tokens');
		}
	} else {
		const balance = await getERC20BalanceFromChain(
			chainId,
			payOption.paymentToken,
			userAddress,
			registryContractAddress
		);
		if ( balance.balance.lt(priceToPay) ) {
			throw new Error('Not enough tokens');
		}
		if ( balance.allowance?.amount.lt(priceToPay) ) {
			await makeERC20AllowanceMultisig(
				web3,
				payOption.paymentToken,
				userAddress,
				priceToPay,
				registryContractAddress
			)
		}
	}

	const contract = await createContract(web3, 'subscriptionAgent', agentContractAddress);

	const buyForParsed = buyFor || userAddress;
	const tx = contract.methods.buySubscription(
		serviceContractAddress,
		tariffIdx,
		payOption.idx,
		buyForParsed,
		userAddress
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}