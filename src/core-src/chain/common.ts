import Web3 from "web3";
import {
	BigNumber,
	getABI
} from "../_utils";
import { Contract } from "web3-eth-contract";
import { _AssetType } from "../_types";
import { getDefaultWeb3 } from "./web3provider";

/**
 * Function-wrapper to find abi and create contract object
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractType - name of default abi-file
 * @param {string} contractAddress - address of contract
 * @returns {Promise<Contract>}> Contract object
 */
export const createContract = async (web3: Web3, contractType: string, contractAddress: string): Promise<Contract> => {
	const chainId = await web3.eth.getChainId();
	const abi = await getABI(chainId, contractAddress || '', contractType);
	const contract = new web3.eth.Contract(abi as any, contractAddress);
	return contract;
}

/**
 * Check type of contract ERC721/ERC1155 with supportsInterface method
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @returns {Promise<_AssetType>}> Type of contract
 */
export const checkContractType = async (chainId: number, contractAddress: string): Promise<_AssetType> => {
	const IID_IERC1155 = '0xd9b67a26';
	const IID_IERC721  = '0x80ac58cd';

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_wnft721', contractAddress);

	if ( await contract.methods.supportsInterface(IID_IERC721).call() ) {
		return _AssetType.ERC721;
	}

	if ( await contract.methods.supportsInterface(IID_IERC1155).call() ) {
		return _AssetType.ERC1155;
	}

	return _AssetType.empty;
}

/**
 * Get address of user's wallet
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @returns {Promise<string>}> Address of connected wallet
 */
export const getUserAddress = async (web3: Web3): Promise<string | undefined> => {
	const accounts = await web3.eth.getAccounts();
	if ( !accounts.length ) { return undefined; }
	if ( accounts[0] === '' ) { return undefined; }
	return accounts[0];
}
export const getChainId = async (web3: Web3): Promise<number | undefined> => {
	const chainId = await web3.eth.getChainId();
	return chainId;
}
/**
 * Get balance of native token of user
 * @param {number} chainId - id of chain where to call method
 * @param {string} userAddress - address of wallet to check
 * @returns {Promise<BigNumber>}> Amount of native token on connected wallet
 */
export const getNativeBalance = async (chainId: number, userAddress: string): Promise<BigNumber> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	return new BigNumber(0);//web3 (await web3.eth.getBalance(userAddress));
}

/**
 * Check if string is a valid address
 * @param {string} str - string to check
 * @returns {boolean}> if string is valid
 */
export const isAddress = (str: string): boolean => {
	return Web3.utils.isAddress(str);
}

/**
 * Check if string is a valid address
 * @param {number} chainId - id of chain where to call check method
 * @param {string} contractAddress - address to check
 * @param {string} tokenId - id of existed token on contract
 * @returns {string}> type of contract ''/'wnft'/'wnftv0'
 */
export const isContractWNFTFromChain = async ( chainId: number, contractAddress: string, tokenId?: string ): Promise<string> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}

	const tokenIdToUse = tokenId || '0';

	const contractwNFT721 = await createContract(web3, '_wnft721', contractAddress);
	try {
		await contractwNFT721.methods.wnftInfo(tokenIdToUse).call();
		return 'wnft'
	} catch(ignored) {}

	const contractWNFTv0 = await createContract(web3, 'wnftv0', contractAddress);
	try {
		await contractWNFTv0.methods.wrappedTokens(tokenIdToUse).call();
		return 'wnftv0'
	} catch(ignored) {}

	return '';
}