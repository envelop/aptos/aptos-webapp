import Web3 from "web3";
import { PromiEvent } from 'web3-core';
import { Contract } from 'web3-eth-contract';
import { createContract } from "./common";
import {
	BigNumber,
	processSwarmUrl
} from '../_utils';
import {
	NFT,
	_AssetType
} from "../_types";
import { getDefaultWeb3 } from "./web3provider";

//#region ---------- ERC1155 ----------
/**
 * Balance of exact user's ERC1155 token
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - address of user to check
 * @param {string} id - id of ERC1155 token to check
 * @returns {Promise<number>} Count of user's ERC1155 tokens on contract
 */
export const getBalanceERC1155 = async (chainId: number, contractAddress: string, userAddress: string, id: string): Promise<BigNumber> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc1155', contractAddress);
	return new BigNumber(await contract.methods.balanceOf(userAddress, id).call());
}
/**
 * Balance of multiple users multiple ERC1155 tokens
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @param {Array<string>} userAddresses - array of users to check
 * @param {Array<string>} ids - array of ids to check
 * @returns {Promise<Array<number>>}> Ordered array of users' balances
 */
export const getBalanceBatchERC1155 = async (chainId: number, contractAddress: string, userAddresses: Array<string>, ids: Array<string>): Promise<Array<number>> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc1155', contractAddress);
	return await contract.methods.balanceOfBatch(userAddresses, ids).call();
}
/**
 * Check if all user's tokens of contract are allowed to use by addressTo
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user's address to check
 * @param {string} addressTo - address which allowed to use tokens
 * @returns {Promise<boolean>}> is all tokens are allowed to use by addressTo
 */
export const checkApprovalERC1155 = async (chainId: number, contractAddress: string, userAddress: string, addressTo: string): Promise<boolean> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc1155', contractAddress);
	return await contract.methods.isApprovedForAll(userAddress, addressTo).call();
}
/**
 * Allow to use all user's tokens of contract to addressTo
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user who allow to use tokens
 * @param {string} addressTo - address which allowed to use tokens
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const setApprovalERC1155 = async (web3: Web3, contractAddress: string, userAddress: string, addressTo: string): Promise<PromiEvent<Contract>> => {
	const contract = await createContract(web3, '_erc1155', contractAddress);
	const tx = contract.methods.setApprovalForAll(
		addressTo,
		true
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
/**
 * MULTISIG
 * Allow to use all user's tokens of contract to addressTo
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user who allow to use tokens
 * @param {string} addressTo - address which allowed to use tokens
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const setApprovalERC1155Multisig = async (web3: Web3, contractAddress: string, userAddress: string, addressTo: string): Promise<PromiEvent<Contract>> => {
	const contract = await createContract(web3, '_erc1155', contractAddress);
	const tx = contract.methods.setApprovalForAll(
		addressTo,
		true
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}
/**
 * Transfer exact amount of ERC1155 token to other address
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} tokenId - id of token
 * @param {BigNumber} amount - amount of ERC1155 token
 * @param {string} userAddress - user who transfer ERC1155 token
 * @param {string} addressTo - address-receiver of ERC1155 token
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const transferERC1155 = async (
	web3: Web3,
	contractAddress: string,
	tokenId: string,
	amount: BigNumber,
	userAddress: string,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {
	const contract = await createContract(web3, '_erc1155', contractAddress);

	const tx = contract.methods.safeTransferFrom(
		userAddress,
		addressTo,
		tokenId,
		amount.toString(),
		'0x0'
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null })
}
/**
 * MULTISIG
 * Transfer exact amount of ERC1155 token to other address
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} tokenId - id of token
 * @param {BigNumber} amount - amount of ERC1155 token
 * @param {string} userAddress - user who transfer ERC1155 token
 * @param {string} addressTo - address-receiver of ERC1155 token
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const transferERC1155Multisig = async (
	web3: Web3,
	contractAddress: string,
	tokenId: string,
	amount: BigNumber,
	userAddress: string,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {
	const contract = await createContract(web3, '_erc1155', contractAddress);

	const tx = contract.methods.safeTransferFrom(
		userAddress,
		addressTo,
		tokenId,
		amount.toString(),
		'0x0'
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}
/**
 * Get data of exact ERC1155 token
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @param {string} tokenId - id of token
 * @param {string} userAddress - If passed, function will check amount of token for specified user
 * @returns {Promise<NFT>}> NFT-object with data
 */
export const getERC1155ByIdFromChain = async (
	chainId: number,
	contractAddress: string,
	tokenId: string,
	userAddress?: string,
): Promise<NFT> => {

	if ( contractAddress === '' ) { throw new Error('Empty contract address'); }
	if ( tokenId === ''         ) { throw new Error('Empty token id'); }

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}

	const contract = await createContract(web3, '_erc1155', contractAddress);

	let tokenUrl     = '';
	let tokenUrlRaw  = '';
	let tokenUrlBody = '';
	let name         = '';
	let description  = '';
	let image        = '';
	let imageRaw     = '';
	let copies       = new BigNumber(0);
	let totalSupply  = new BigNumber(0);
	let token;
	let tokenParsed;
	try {
		if ( userAddress ) {
			copies  = new BigNumber(await contract.methods.balanceOf(userAddress, tokenId).call());
		}
	} catch(ignored) {}
	try {
		totalSupply = new BigNumber(await contract.methods.totalSupply(tokenId).call());
	} catch(ignored) {}
	try {
		tokenUrlRaw = await contract.methods.uri(tokenId).call();
		tokenUrl    = processSwarmUrl(tokenUrlRaw);
	} catch(e) {
		console.log('Cannot get erc1155 token url: ', e);
		throw e;
	}

	try {
		token           = await fetch(tokenUrl);
		tokenUrlBody    = await token.text();
		tokenParsed     = JSON.parse(tokenUrlBody);
		name            = tokenParsed.name        || '';
		description     = tokenParsed.description || '';
		if ( tokenParsed.image     ) { image = processSwarmUrl(tokenParsed.image    ); imageRaw = tokenParsed.image     }
		if ( tokenParsed.image_url ) { image = processSwarmUrl(tokenParsed.image_url); imageRaw = tokenParsed.image_url }
	} catch(e) {
		console.log('Cannot fetch token data', token);
	}

	return {
		chainId,
		contractAddress,
		tokenId,
		tokenUrlBody,
		tokenUrl,
		tokenUrlRaw,
		name,
		description,
		image,
		imageRaw,
		assetType: _AssetType.ERC1155,
		amount: copies,
		totalSupply,
	}
}
/**
 * Get name of contract
 * @param {string} contractAddress - address of contract
 * @returns {Promise<string>}> Name of contract
 */
export const getContractNameERC1155 = async (chainId: number, contractAddress: string): Promise<string> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc1155', contractAddress);
	return contract.methods.name().call();

}
//#endregion ---------- ERC1155 ----------

//#region ---------- ERC721 ----------
/**
 * Get amount of user's token on contract
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - address of user to check balance
 * @returns {Promise<BigNumber>}> Promise of sent function (can be listened in outer function)
 */
export const getBalanceERC721 = async (chainId: number, contractAddress: string, userAddress: string): Promise<BigNumber> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc721', contractAddress);
	return await contract.methods.balanceOf(userAddress).call();
}
/**
 * Check if all tokens of contract are allowed to use by addressTo
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user's address to check
 * @param {string} addressTo - address which allowed to use tokens
 * @returns {Promise<boolean>}> is token are allowed to use by addressTo
 */
export const checkApprovalForAllERC721 = async (
	chainId: number,
	contractAddress: string,
	userAddress: string,
	addressTo: string,
): Promise<boolean> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc721', contractAddress);

	return await contract.methods.isApprovedForAll(userAddress, addressTo).call();
}
/**
 * Check if exact token or all user's tokens of contract are allowed to use by addressTo
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user's address to check
 * @param {string} addressTo - address which allowed to use tokens
 * @returns {Promise<boolean>}> is token are allowed to use by addressTo
 */
export const checkApprovalERC721 = async (
	chainId: number,
	contractAddress: string,
	tokenId: string,
	userAddress: string,
	addressTo: string,
): Promise<boolean> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc721', contractAddress);

	if ( tokenId ) {
		const approvedAddress = await contract.methods.getApproved(tokenId).call();
		if ( approvedAddress.toLowerCase() === addressTo.toLowerCase() ) { return true; }
	}

	return await contract.methods.isApprovedForAll(userAddress, addressTo).call();
}
/**
 * Allow to use all user's tokens of contract to addressTo
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user who allow to use tokens
 * @param {string} addressTo - address which allowed to use tokens
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const setApprovalForAllERC721 = async (
	web3: Web3,
	contractAddress: string,
	userAddress: string,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {
	const contract = await createContract(web3, '_erc721', contractAddress);

	const tx = contract.methods.setApprovalForAll(
		addressTo,
		true
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
/**
 * MULTISIG
 * Allow to use all user's tokens of contract to addressTo
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user who allow to use tokens
 * @param {string} addressTo - address which allowed to use tokens
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const setApprovalForAllERC721Multisig = async (
	web3: Web3,
	contractAddress: string,
	userAddress: string,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {
	const contract = await createContract(web3, '_erc721', contractAddress);

	const tx = contract.methods.setApprovalForAll(
		addressTo,
		true
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}
/**
 * Allow to use exact user's token of contract to addressTo
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user who allow to use tokens
 * @param {string} addressTo - address which allowed to use tokens
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const setApprovalERC721 = async (
	web3: Web3,
	contractAddress: string,
	tokenId: string,
	userAddress: string,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {

	const contract = await createContract(web3, '_erc721', contractAddress);

	const tx = contract.methods.approve(
		addressTo,
		tokenId
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });
}
/**
 * MULTISIG
 * Allow to use exact user's token of contract to addressTo
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user who allow to use tokens
 * @param {string} addressTo - address which allowed to use tokens
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const setApprovalERC721Multisig = async (
	web3: Web3,
	contractAddress: string,
	tokenId: string,
	userAddress: string,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {

	const contract = await createContract(web3, '_erc721', contractAddress);

	const tx = contract.methods.approve(
		addressTo,
		tokenId
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}

const _fetchERC721ById = async (
	contract: Contract,
	contractAddress: string,
	tokenId: string,
	chainId: number,
) => {
	let tokenUrl     = '';
	let tokenUrlRaw  = '';
	let tokenUrlBody = '';
	let name         = '';
	let description  = '';
	let image        = '';
	let imageRaw     = '';
	let owner        = '';
	let token;
	let tokenParsed;

	try {
		owner       = await contract.methods.ownerOf(tokenId).call();
		tokenUrlRaw = await contract.methods.tokenURI(tokenId).call();
		tokenUrl    = processSwarmUrl(tokenUrlRaw);
	} catch(e) {
		console.log('Cannot get erc721 token: ', e);
		throw e;
	}

	try {
		token        = await fetch(tokenUrl);
		tokenUrlBody = await token.text();
		tokenParsed  = JSON.parse(tokenUrlBody);
		name         = tokenParsed.name        || '';
		description  = tokenParsed.description || '';
		if ( tokenParsed.image     ) { image = processSwarmUrl(tokenParsed.image    ); imageRaw = tokenParsed.image    ; }
		if ( tokenParsed.image_url ) { image = processSwarmUrl(tokenParsed.image_url); imageRaw = tokenParsed.image_url; }
	} catch(e) {
		console.log('Cannot fetch token data', token);
	}

	return {
		owner,
		chainId,
		contractAddress,
		tokenId,
		tokenUrlBody,
		tokenUrl,
		tokenUrlRaw,
		name,
		description,
		image,
		imageRaw,
		assetType: _AssetType.ERC721,
	}
}
/**
 * Get all user's tokens of contract
 *
 * NOTE: Contract should have tokenOfOwnerByIndex method
 *
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user to check
 * @returns {Promise<Array<NFT>>}> Array of user's tokens
 */
export const UNSAFE_getERC721sOfContractFromChain = async (
	chainId: number,
	contractAddress: string,
	userAddress: string,
): Promise<Array<NFT>> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc1155', contractAddress);

	const balance = await contract.methods.balanceOf( userAddress ).call();

	const output = []

	for (let idx = 0; idx < balance; idx++) {
		const contractTokenId = await contract.methods.tokenOfOwnerByIndex(userAddress, idx).call();
		output.push(
			await _fetchERC721ById(
				contract,
				contractAddress,
				contractTokenId,
				chainId
			)
		)
	}

	return output
}
/**
 * Get exact token of contract
 *
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contractk
 * @param {string} tokenId - token id to get
 * @returns {Promise<Array<NFT>>}> NFT-object
 */
export const getERC721ByIdFromChain = async (
	chainId: number,
	contractAddress: string,
	tokenId: string,
): Promise<NFT> => {

	if ( contractAddress === '' ) { throw new Error('Empty contract address'); }
	if ( tokenId === ''         ) { throw new Error('Empty token id'); }

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc721', contractAddress);

	return _fetchERC721ById(contract, contractAddress, tokenId, chainId);
}
/**
 * Transfer ERC721 token to other address
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} tokenId - id of token
 * @param {string} userAddress - user who transfer ERC1155 token
 * @param {string} addressTo - address-receiver of ERC1155 token
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const transferERC721 = async (
	web3: Web3,
	contractAddress: string,
	tokenId: string,
	userAddress: string,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {

	const contract = await createContract(web3, '_erc721', contractAddress);

	const tx = contract.methods.transferFrom(
		userAddress,
		addressTo,
		tokenId
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null })
}
/**
 * MULTISIG
 * Transfer ERC721 token to other address
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} tokenId - id of token
 * @param {string} userAddress - user who transfer ERC1155 token
 * @param {string} addressTo - address-receiver of ERC1155 token
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const transferERC721Multisig = async (
	web3: Web3,
	contractAddress: string,
	tokenId: string,
	userAddress: string,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {

	const contract = await createContract(web3, '_erc721', contractAddress);

	const tx = contract.methods.transferFrom(
		userAddress,
		addressTo,
		tokenId
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}
/**
 * Get name of contract
 * @param {string} contractAddress - address of contract
 * @returns {Promise<string>}> Name of contract
 */
export const getContractNameERC721 = async (chainId: number, contractAddress: string): Promise<string> => {
	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc721', contractAddress);
	return contract.methods.name().call();

}
//#endregion ---------- ERC721 ----------

/**
 * Mint new ERC721 token on minter contract
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user who transfer ERC1155 token
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const mintERC721 = async (
	web3: Web3,
	contractAddress: string,
	userAddress: string
): Promise<PromiEvent<Contract>> => {
	const contract = await createContract(web3, 'minter', contractAddress);

	const tx = contract.methods.mint( userAddress );

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null })
}



/**
 * MULTISIG
 * Mint new ERC721 token on minter contract
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - user who transfer ERC1155 token
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const mintERC721Multisig = async (
	web3: Web3,
	contractAddress: string,
	userAddress: string
): Promise<PromiEvent<Contract>> => {
	const contract = await createContract(web3, 'minter', contractAddress);

	const tx = contract.methods.mint( userAddress );

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}