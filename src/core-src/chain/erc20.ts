import Web3 from 'web3';
import { PromiEvent } from 'web3-core';
import { Contract } from 'web3-eth-contract';
import { createContract } from './common';
import { BigNumber } from '../_utils';
import { Allowance, ERC20Balance, ERC20Type, _AssetType } from '../_types';
import default_icon from '../../core-pics/coins/_default.svg';
import { getDefaultWeb3 } from './web3provider';

/**
 * Get parameters of erc20 contract
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - if passed, function will check balance of specified address
 * @param {string} allowanceTo - if passed, function will check allowance to specified address of userAddress
 * @returns {Promise<ERC20Type>}> ERC20 object
 */
export const getERC20ParamsFromChain = async (
	chainId: number,
	contractAddress: string,
	userAddress?: string,
	allowanceTo?: string,
): Promise<ERC20Type> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_erc20', contractAddress);

	let name = '';
	let symbol = '';
	let decimals = 18;
	let balance = new BigNumber(0);
	let icon = default_icon;

	let allowance: Array<Allowance> = [];

	name = await contract.methods.name().call();
	symbol = await contract.methods.symbol().call();
	decimals = parseInt(await contract.methods.decimals().call());
	if (userAddress) {
		balance = new BigNumber(
			await contract.methods.balanceOf(userAddress).call(),
		);
		if (allowanceTo) {
			allowance = [
				{
					allowanceTo: allowanceTo,
					amount: new BigNumber(
						await contract.methods.allowance(userAddress, allowanceTo).call(),
					),
				},
			];
		}
	}

	try {
		icon = require(`../../core-pics/coins/${symbol.toLowerCase()}.jpeg`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${symbol.toLowerCase()}.jpg`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${symbol.toLowerCase()}.png`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${symbol.toLowerCase()}.svg`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${contractAddress.toLowerCase()}.jpeg`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${contractAddress.toLowerCase()}.jpg`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${contractAddress.toLowerCase()}.png`);
	} catch (ignored) {}
	try {
		icon = require(`../../core-pics/coins/${contractAddress.toLowerCase()}.svg`);
	} catch (ignored) {}

	return {
		contractAddress: contractAddress,
		assetType: _AssetType.ERC20,
		name,
		symbol,
		decimals,
		icon,
		balance,
		allowance,
	};
};
/**
 * Get balance of erc20 contract for exact user
 * @param {number} chainId - id of chain where to call method
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - balance of user to check
 * @param {string} allowanceTo - if passed, function will check allowance to specified address of userAddress
 * @returns {Promise<{address: string,balance: BigNumber, allowance: BigNumber }>} ERC20 object
 */
export const getERC20BalanceFromChain = async (
	chainId: number,
	contractAddress: string,
	userAddress: string,
	allowanceTo?: string,
): Promise<ERC20Balance> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}

	const contract = await createContract(web3, '_erc20', contractAddress);

	let balance = new BigNumber(0);
	let allowance = undefined;

	balance = new BigNumber(await contract.methods.balanceOf(userAddress).call());
	if (allowanceTo) {
		allowance = {
			allowanceTo,
			amount: new BigNumber(
				await contract.methods.allowance(userAddress, allowanceTo).call(),
			),
		};
	}

	return {
		contractAddress,
		balance,
		allowance,
	};
};
/**
 * Allow other address to use user's ERC20 tokens
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - address which allow to use tokens
 * @param {string} addressTo - address which get permissions
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const makeERC20Allowance = async (
	web3: Web3,
	contractAddress: string,
	userAddress: string,
	amount: BigNumber,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {
	const contract = await createContract(web3, '_erc20', contractAddress);

	const parsedAmount =
		new BigNumber(amount).toString() === '-1'
			? new BigNumber(10 ** 50).toString()
			: new BigNumber(amount).toString();
	return contract.methods.approve(addressTo, parsedAmount).send({
		from: userAddress,
		maxPriorityFeePerGas: null,
		maxFeePerGas: null,
	});
};
/**
 * MULTISIG FUNCTION
 * Allow other address to use user's ERC20 tokens
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {string} contractAddress - address of contract
 * @param {string} userAddress - address which allow to use tokens
 * @param {string} addressTo - address which get permissions
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const makeERC20AllowanceMultisig = async (
	web3: Web3,
	contractAddress: string,
	userAddress: string,
	amount: BigNumber,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {
	return new Promise(async (res, rej) => {
		const contract = await createContract(web3, '_erc20', contractAddress);

		const parsedAmount =
			new BigNumber(amount).toString() === '-1'
				? new BigNumber(10 ** 50).toString()
				: new BigNumber(amount).toString();
		return contract.methods
			.approve(addressTo, parsedAmount)
			.send(
				{ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null },
				(err: any, data: any) => {
					if (err) {
						rej(err);
					}
					res(data);
				},
			);
	});
};
