import Web3 from 'web3';
import config from '../app.config.json';

import Onboard from '@web3-onboard/core';
import injectedModule from '@web3-onboard/injected-wallets';
import gnosisModule from '@web3-onboard/gnosis';
import walletConnectModule from '@web3-onboard/walletconnect';
import mewWallet from '@web3-onboard/mew-wallet';
import coinbaseWalletModule from '@web3-onboard/coinbase';
import trustModule from '@web3-onboard/trust';

import { SafeAppProvider } from '@safe-global/safe-apps-provider';
import SafeAppsSDK from '@safe-global/safe-apps-sdk';

import { createAuthToken, getChainParamsAllFromAPI } from '../oracle';
import {
	combineURLs,
	localStorageGet,
	localStorageRemove,
	localStorageSet,
} from '../_utils';
import { ChainType } from '../_types';
import { _getCacheItem, _setCacheItem } from '..';

import niftsyLogo from '../../core-pics/coins/niftsy.svg';

const _getRPCUrlFromAPI = async (chainId: number) => {
	const authToken = await createAuthToken();
	if (authToken === '') {
		return undefined;
	}

	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if (!BASE_URL) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}
	const url = combineURLs(BASE_URL, `/chain_info/${chainId}`);
	try {
		const resp = await fetch(url, {
			headers: {
				Authorization: authToken,
			},
		});

		const respParsed: ChainType = await resp.json();
		if (!resp.ok) {
			return undefined;
		}
		if ('error' in respParsed) {
			return undefined;
		}

		_setCacheItem(`rpcUrl_${chainId}`, respParsed.RPCUrl);
		return respParsed.RPCUrl;
	} catch (e) {
		console.log('Cannot fetch token from oracle', e);
		return undefined;
	}
};
const _getRPCUrlFromConfig = async (chainId: number) => {
	const foundChain = config.CHAIN_PARAMS.find((item: any) => {
		return item.chainId === chainId;
	});
	if (!foundChain) {
		return undefined;
	}
	_setCacheItem(`rpcUrl_${chainId}`, foundChain.chainRPCUrl);
	return foundChain.chainRPCUrl;
};
/**
 * Create web3 object only for reading data
 * @param {number} chainId - id of network to connect
 * @returns {Promise<Web3 | undefined>}> Web3 object which shoгld be passed to any function that interact with blockchain
 */
export const getDefaultWeb3 = async (
	chainId?: number,
): Promise<Web3 | null> => {
	const chainIdToUse = chainId || 1;
	let rpcUrl = await _getCacheItem(`rpcUrl_${chainIdToUse}`);

	if (!rpcUrl) {
		rpcUrl = await _getRPCUrlFromAPI(chainIdToUse);
	}
	if (!rpcUrl) {
		rpcUrl = await _getRPCUrlFromConfig(chainIdToUse);
	}

	return new Web3(rpcUrl);
};

const initOnboard = async () => {
	if ((window as any).onboard) {
		return (window as any).onboard;
	}

	const availableChains = await getChainParamsAllFromAPI();

	const injected = injectedModule();
	const gnosis = gnosisModule();
	const trust = trustModule();
	const mewWalletModule = mewWallet();
	const coinbaseWalletSdk = coinbaseWalletModule({ darkMode: true });
	const walletConnect = walletConnectModule({
		version: 2,
		projectId: '1a6dc9f34d7dce2a117da7f72fc7c559',
	});

	let onboard =
		(window as any).onboard ||
		Onboard({
			wallets: [
				injected,
				gnosis,
				trust,
				mewWalletModule,
				walletConnect,
				coinbaseWalletSdk,
			],
			chains: availableChains.map((item) => {
				return {
					id: `0x${item.chainId.toString(16)}`,
					token: item.symbol,
					label: item.name,
					rpcUrl: item.RPCUrl,
				};
			}),
			appMetadata: {
				name: 'Envelop dApp',
				icon: niftsyLogo,
				logo: niftsyLogo,
				description:
					'Collateralised NFTs. NFT 2.0. You can wrap NFTs by adding coins, tokens, including other NFTs to the collateral',
				recommendedInjectedWallets: [
					{ name: 'MetaMask', url: 'https://metamask.io' },
				],
				explore: 'https://app.envelop.is',
			},
			connect: {
				autoConnectLastWallet: true,
			},
			accountCenter: {
				desktop: { enabled: false },
				mobile: { enabled: false },
			},
		});

	return onboard;
};

/**
 * Check if user already connect wallet and return web3 object without any actions from user
 * If there is never was connection or wallet is locked return undefined
 * @returns {Promise<Web3 | undefined>}> Web3 object which shoгld be passed to any function that interact with blockchain
 */
export const connectSilent = async (): Promise<Web3 | null> => {
	const sdk = new SafeAppsSDK();
	try {
		await Promise.race([
			new Promise((res) => {
				sdk.safe.getInfo().then((data) => {
					res(data);
				});
			}),
			new Promise((_, rej) => {
				setTimeout(() => {
					rej();
				}, 500);
			}),
		]);
		const safe = await sdk.safe.getInfo();
		localStorageSet('authMethod', 'Safe');
		return new Web3(new SafeAppProvider(safe, sdk as any) as any);
	} catch (ignored) {}

	const authMethod = localStorageGet('authMethod');

	if (
		authMethod.toLowerCase() === 'metamask' &&
		(window as any).ethereum &&
		(window as any).ethereum._metamask
	) {
		if (await (window as any).ethereum._metamask.isUnlocked()) {
			const _web3 = new Web3((window as any).ethereum);
			if (!_web3) {
				return null;
			}

			return _web3;
		} else {
			return null;
		}
	}

	const onboard = (window as any).onboard || (await initOnboard());
	(window as any).onboard = onboard;

	if (onboard.state.get().wallets.length) {
		return new Web3(onboard.state.get().wallets[0].provider as any);
	}

	if (!authMethod || authMethod === '') {
		return null;
	}

	const wallets = await onboard.connectWallet({
		autoSelect: {
			label: authMethod,
			disableModals: true,
		},
	});
	if (wallets[0]) {
		try {
			return new Web3(wallets[0].provider as any);
		} catch (e) {
			throw e;
		}
	}

	return null;
};
/**
 * Open menu with various wallets, auth user and create web3 object
 * @returns {Promise<Web3>}> Web3 object which should be passed to any function that interact with blockchain
 */
export const connect = async (): Promise<Web3 | null> => {
	const beenLogged = await connectSilent();
	if (beenLogged) {
		return beenLogged;
	}

	const authMethod = localStorageGet('authMethod');
	let web3 = null;

	if (
		authMethod.toLowerCase() === 'metamask' &&
		(window as any).ethereum &&
		(window as any).ethereum._metamask
	) {
		await (window as any).ethereum.request({ method: 'eth_requestAccounts' });
		return new Web3((window as any).ethereum);
	}

	const onboard = await initOnboard();
	(window as any).onboard = onboard;

	const wallets = await onboard.connectWallet();
	if (wallets[0]) {
		try {
			web3 = new Web3(wallets[0].provider as any);
			localStorageSet('authMethod', wallets[0].label);
		} catch (e) {
			throw e;
		}
	}

	return web3;
};
export const disconnect = () => {
	const authMethod = localStorageGet('authMethod');

	if (authMethod.toLowerCase() === 'metamask') {
		(window as any).ethereum.removeAllListeners();
	} else {
		const onboardStateUnsubscribe = _getCacheItem('onboardStateUnsubscribe');
		if (onboardStateUnsubscribe) {
			onboardStateUnsubscribe();
		}
	}

	for (const item in localStorage) {
		if (item.toLowerCase() === 'authmethod') {
			localStorageRemove(item);
		}
		if (item.toLowerCase() === 'lastuseraddress') {
			localStorageRemove(item);
		}
		if (item.toLowerCase().includes('walletlink')) {
			localStorageRemove(item);
		}
		if (item.toLowerCase().includes('onboard.js')) {
			localStorageRemove(item);
		}
	}

	(window as any).onboard = undefined;
};
/**
 * Request wallet to change network
 * NOTE: works only with metamask extension
 */
export const requestChainChange = async (chainId: number): Promise<boolean> => {
	const authMethod = localStorageGet('authMethod');

	if ((window as any).onboard) {
		await (window as any).onboard.setChain({
			chainId: '0x' + Number(chainId).toString(16),
		});
		return true;
	}

	if (authMethod.toLowerCase() === 'metamask' && (window as any).ethereum) {
		await (window as any).ethereum.request({
			method: 'wallet_switchEthereumChain',
			params: [{ chainId: '0x' + Number(chainId).toString(16) }],
		});

		return true;
	}

	return false;
};

export const walletStateListener = (cb: Function) => {
	if ((window as any).onboard) {
		const { onboardStateUnsubscribe } = (window as any).onboard.state
			.select('notifications')
			.subscribe(cb);
		_setCacheItem('onboardStateUnsubscribe', onboardStateUnsubscribe);
		return;
	}

	const authMethod = localStorageGet('authMethod');

	if (authMethod.toLowerCase() === 'metamask') {
		(window as any).ethereum.on('chainChanged', () => {
			cb();
		});
		(window as any).ethereum.on('accountsChanged', () => {
			cb();
		});
		return;
	}
};
