
import {
	expect,
	test
} from '@jest/globals';
import {
	_AssetType,
	decodeAssetTypeFromIndex,
	decodeAssetTypeFromString,
	assetTypeToString,
	encodeCollaterals,
	CollateralItem,
	_AssetItem,
	decodeCollaterals,
	getNativeCollateral,
	calcCollectedFees, 
	Fee,
	encodeRoyalties,
	decodeRoyalties, 
	Royalty, 
	_Royalty,
	LockType,
	getLockType,
	Lock,
	_Lock,
	encodeLocks,
	decodeLocks,
	_Fee,
	encodeFees,
	decodeFees,
	Rules,
	encodeRules,
	decodeRules
} from '../_types';
import {
	BigNumber,
	processSwarmUrl
}  from '../_utils';

test('decodeAssetTypeFromString', () => {
	expect(decodeAssetTypeFromString('ERC20')).toBe(_AssetType.ERC20);
});

test('decodeAssetTypeFromIndex', () => {
	expect(decodeAssetTypeFromIndex('-1')).toBe(_AssetType.wNFTv0);
});

test('assetTypeToString', () => {
	expect(assetTypeToString(-1, 'ERC')).toBe('wNFTv0');
	expect(assetTypeToString(0, 'ERC')).toBe('empty');
	expect(assetTypeToString(1, 'ERC')).toBe('native');
	expect(assetTypeToString(2, 'ERC')).toBe('ERC-20');
	expect(assetTypeToString(3, 'ERC')).toBe('ERC-721');
	expect(assetTypeToString(4, 'ERC')).toBe('ERC-1155');
	expect(assetTypeToString(4, 'ERC')).toBe('ERC-1155');
});

test('encodeCollaterals with transferFeeToken', () => {
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									};
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'usdc_image.png'
									};
	const collaterals = [ coll1, coll2];
	const transferFeeToken = '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174';
	let result = encodeCollaterals(collaterals, transferFeeToken);
	const res1: _AssetItem = {
								asset: {
											assetType: 2,
											contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174'
										},
								tokenId: '0',
								amount: '4000000000000000000'
							};
	const res2: _AssetItem = {
								asset: {
											assetType: 2,
											contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
										},
								tokenId: '0',
								amount: '1000000000000000000'
							};
	expect(result.length).toBe(2);
	expect(result[0].asset.contractAddress).toBe(transferFeeToken); //check transferFeeToken
	expect(result[0]).toEqual(res1);
	expect(result[1]).toEqual(res2);
});

test('encodeCollaterals without transferFeeToken', () => {
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									};
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'usdc_image.png'
									};
	const collaterals = [ coll1, coll2];
	let result = encodeCollaterals(collaterals);
	const res1: _AssetItem = {
								asset: {
											assetType: 2,
											contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
										},
								tokenId: '0',
								amount: '1000000000000000000'
							};
	const res2: _AssetItem = {
								asset: {
											assetType: 2,
											contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174'
										},
								tokenId: '0',
								amount: '4000000000000000000'
							};
	expect(result.length).toBe(2);
	expect(result[0]).toEqual(res1);
	expect(result[1]).toEqual(res2);
});

test('encodeCollaterals without params', () => {
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
									};
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
									};
	const collaterals = [ coll1, coll2];
	let result = encodeCollaterals(collaterals);
	const res1: _AssetItem = {
								asset: {
											assetType: 2,
											contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
										},
								tokenId: '0',
								amount: '0'
							};
	const res2: _AssetItem = {
								asset: {
											assetType: 2,
											contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174'
										},
								tokenId: '0',
								amount: '0'
							};
	expect(result.length).toBe(2);
	expect(result[0]).toEqual(res1);
	expect(result[1]).toEqual(res2);
});

/*test('encodeCollaterals with empty array', () => {
	const collaterals = [];
	let result = encodeCollaterals(collaterals);
	expect(result.length).toBe(0);
	expect(result).toEqual([]);
});*/

test('decodeCollaterals', () => {
	const coll1: _AssetItem = {
								asset: {
											assetType: 2,
											contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
										},
								tokenId: '0',
								amount: '10000000000000000000000000000'
							};
	const coll2: _AssetItem = {
								asset: {
											assetType: 2,
											contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174'
										},
								tokenId: '0',
								amount: '20000000000000000000000000000'
							};
	const collaterals = [coll1, coll2];
	let result = decodeCollaterals(collaterals);
	const res1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('10000000000000000000000000000')
									};
	const res2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
										tokenId: '0',
										amount: new BigNumber('20000000000000000000000000000')
									};
	expect(result[0]).toEqual(res1);
	expect(result[1]).toEqual(res2);
});

test('decodeCollaterals with empty amount', () => {
	const coll1: _AssetItem = {
								asset: {
											assetType: 2,
											contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
										},
								tokenId: '',
								amount: ''
							};
	const coll2: _AssetItem = {
								asset: {
											assetType: 2,
											contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174'
										},
								tokenId: '',
								amount: ''
							};
	const collaterals = [coll1, coll2];
	let result = decodeCollaterals(collaterals);
	const res1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '',
										amount: new BigNumber('0')
									};
	const res2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
										tokenId: '',
										amount: new BigNumber('0')
									};
	expect(result[0]).toEqual(res1);
	expect(result[1]).toEqual(res2);
});

test('getNativeCollateral with zero address', () => {
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									};
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x0000000000000000000000000000000000000000',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'ether_image.png'
									};
	const collaterals = [ coll1, coll2];
	
	let result = getNativeCollateral(collaterals);
	expect(result).toEqual(new BigNumber('4000000000000000000')); 
});

test('getNativeCollateral with empty address', () => {
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									};
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'ether_image.png'
									};
	const collaterals = [ coll1, coll2];
	
	let result = getNativeCollateral(collaterals);
	expect(result).toEqual(new BigNumber('4000000000000000000')); 
});

test('getNativeCollateral with address 0', () => {
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									};
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'ether_image.png'
									};
	const collaterals = [ coll1, coll2];
	
	let result = getNativeCollateral(collaterals);
	expect(result).toEqual(new BigNumber('4000000000000000000')); 
});

test('getNativeCollateral without native tokens', () => {
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									};
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'ether_image.png'
									};
	const collaterals = [ coll1, coll2];
	
	let result = getNativeCollateral(collaterals);
	expect(result).toEqual(new BigNumber('0')); 
});

test('calcCollectedFees with empty fee array', () => {
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									};
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'ether_image.png'
									};
	const collaterals = [ coll1, coll2];
	let result = calcCollectedFees(collaterals, []);
	expect(result).toEqual(new BigNumber('0'));
});

test('calcCollectedFees with empty fee array', () => {
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									};
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'ether_image.png'
									};
	const collaterals = [ coll1, coll2];
	let result = calcCollectedFees(collaterals, []);
	expect(result).toEqual(new BigNumber('0'));
});

test('calcCollectedFees with one record in fee array', () => {
	const fee: Fee = {
						value: new BigNumber('100'),
						token: "0xc2132d05d31c914a87c6611c10748aeb04b58e8f"
					}
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									}
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'ether_image.png'
									}
	const collaterals = [ coll1, coll2];
	let result = calcCollectedFees(collaterals, [fee]);
	expect(result).toEqual(coll1.amount);
});


test('calcCollectedFees with several records in fee array', () => {
	const fee1: Fee = {
						value: new BigNumber('100'),
						token: "0xc2132d05d31c914a87c6611c10748aeb04b58e8f"
					}
	const fee2: Fee = {
						value: new BigNumber('200'),
						token: "0xc2132d05d31c914a87c6611c10748aeb04b58e8f"
					}
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									}
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'ether_image.png'
									}
	const collaterals = [ coll1, coll2];
	let result = calcCollectedFees(collaterals, [fee1, fee2]);
	expect(result).toEqual(coll1.amount);
});


test('calcCollectedFees with several identical records in collateral array', () => {
	const fee: Fee = {
						value: new BigNumber('100'),
						token: "0xc2132d05d31c914a87c6611c10748aeb04b58e8f"
					}
	const coll1: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('1000000000000000000'),
										tokenImg: 'usdt_image.png'
									}
	const coll2: CollateralItem =  	{
										assetType: 2,
										contractAddress: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
										tokenId: '0',
										amount: new BigNumber('4000000000000000000'),
										tokenImg: 'ether_image.png'
									}
	const collaterals = [ coll1, coll2];
	let result = calcCollectedFees(collaterals, [fee]);
	expect(result).toEqual(new BigNumber('5000000000000000000'));
});

test('encodeRoyalties', () => {
	const royalty1: Royalty = {
								address: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
								percent: new BigNumber("40")
							}
	const royalty2: Royalty = {
								address: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
								percent: new BigNumber("50")
							}
	let result = encodeRoyalties([royalty1, royalty2], '0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE');
	
	const roy1: _Royalty = {
								beneficiary: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
								percent: "4000"
							}
	const roy2: _Royalty = {
								beneficiary: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
								percent: "5000"
							}
	
	expect(result).toEqual([roy1, roy2]);
});

test('encodeRoyalties with undefined', () => {
	const royalty1: Royalty = {
								address: undefined,
								percent: new BigNumber("40")
							}
	const royalty2: Royalty = {
								address: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
								percent: new BigNumber("50")
							}
	let result = encodeRoyalties([royalty1, royalty2], '0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE');
	
	const roy1: _Royalty = {
								beneficiary: '0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE',
								percent: "4000"
							}
	const roy2: _Royalty = {
								beneficiary: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
								percent: "5000"
							}
	expect(result).toEqual([roy1, roy2]);
});

test('decodeRoyalties', () => {
	
	const roy1: _Royalty = {
								beneficiary: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
								percent: "4000"
							}
	const roy2: _Royalty = {
								beneficiary: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
								percent: "5000"
							}

	let result = decodeRoyalties([roy1, roy2]);

	const royalty1: Royalty = {
								address: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f',
								percent: new BigNumber("40")
							}
	const royalty2: Royalty = {
								address: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
								percent: new BigNumber("50")
							}
	
	expect(result).toEqual([royalty1, royalty2]);
});

test('decodeRoyalties with undefined', () => {
	
	
	const roy1: _Royalty = {
								beneficiary: '0x0000000000000000000000000000000000000000',
								percent: "4000"
							}
	const roy2: _Royalty = {
								beneficiary: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
								percent: "5000"
							}
	let result = decodeRoyalties([roy1, roy2]);

	const royalty1: Royalty = {
								address: undefined,
								percent: new BigNumber("40")
							}
	const royalty2: Royalty = {
								address: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174',
								percent: new BigNumber("50")
							}

	expect(result).toEqual([royalty1, royalty2]);
});

test('getLockType', () => {
	
	let result = getLockType('0x00');
	expect(result).toEqual('0x00');

	result = getLockType('0x03');
	expect(result).toEqual(undefined);
});

test('encodeLocks', () => {

	const lock1: Lock = {
							lockType: LockType.value,
							param: new BigNumber('100')
						}
	
	const lock2: Lock = {
							lockType: LockType.time,
							param: new BigNumber('10000000')
						}

	let result = encodeLocks([lock1, lock2]);
	
	const _lock1: _Lock = {
							lockType: LockType.value,
							param: '100'
						}
	
	const _lock2: _Lock = {
							lockType: LockType.time,
							param: '10000000'
						}	

	expect(result).toEqual([_lock1, _lock2]);
});

test('decodeLocks', () => {

	const _lock1: _Lock = {
							lockType: LockType.value,
							param: '100'
						}
	
	const _lock2: _Lock = {
							lockType: LockType.time,
							param: '10000000'
						}

	let result = decodeLocks([_lock1, _lock2]);
	
	const lock1: Lock = {
							lockType: LockType.value,
							param: new BigNumber('100')
						}
	
	const lock2: Lock = {
							lockType: LockType.time,
							param: new BigNumber('10000000000')
						}	

	expect(result).toEqual([lock1, lock2]);
});

test('decodeLocks with NaN', () => {

	const _lock1: _Lock = {
							lockType: LockType.value,
							param: 'NaN'
						}
	
	const _lock2: _Lock = {
							lockType: LockType.time,
							param: ''
						}

	let result = decodeLocks([_lock1, _lock2]);
	
	const lock1: Lock = {
							lockType: LockType.value,
							param: new BigNumber('0')
						}
	
	const lock2: Lock = {
							lockType: LockType.time,
							param: new BigNumber('0')
						}	

	expect(result).toEqual([lock1, lock2]);
});

test('encodeFees', () => {

	const fee1: Fee = {
							value: new BigNumber('100'),
							token: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174'
						}
	
	const fee2: Fee = {
							value: new BigNumber('200'),
							token: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
						}

	let result = encodeFees([fee1, fee2]);
	
	const _fee1: _Fee = {	
							feeType: '0x00',
							param: '100',
							token: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174'
						}
	
	const _fee2: _Fee = {	
							feeType: '0x00',
							param: '200',
							token: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
						}

	expect(result).toEqual([_fee1, _fee2]);
});

test('decodeFees', () => {

	const _fee1: _Fee = {	
							feeType: '0x00',
							param: '100',
							token: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174'
						}
	
	const _fee2: _Fee = {	
							feeType: '0x00',
							param: 'NaN',
							token: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
						}

	let result = decodeFees([_fee1, _fee2]);
	
	const fee1: Fee = {
							value: new BigNumber('100'),
							token: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174'
						}
	
	const fee2: Fee = {
							value: new BigNumber('0'),
							token: '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
						}

	expect(result).toEqual([fee1, fee2]);
});

test('encodeRules', () => {

	const _rules: Rules = {
								noUnwrap: true,
								noAddCollateral: true,
								noWrap: true,
								noTransfer: true
							}

	
	let result = encodeRules(_rules);
	expect(result).toBe('0x000f');
});

test('encodeRules without restriction', () => {

	const _rules: Rules = {
								noUnwrap: false,
								noAddCollateral: false,
								noWrap: false,
								noTransfer: false
							}

	
	let result = encodeRules(_rules);
	expect(result).toBe('0x0000');
});


test('decodeRules', () => {
	let result = decodeRules('0x000f');
	const _rules: Rules = {
								noUnwrap: true,
								noAddCollateral: true,
								noWrap: true,
								noTransfer: true
							}

	
	expect(result).toEqual(_rules);
});

test('decodeRules wrong value', () => {
	let result = decodeRules('0x00af');
	const _rules: Rules = {
								noUnwrap: true,
								noAddCollateral: true,
								noWrap: true,
								noTransfer: true
							}

	
	expect(result).toEqual(_rules);
});

test('decodeRules 0x0002', () => {
	let result = decodeRules('0x0002');
	const _rules: Rules = {
								noUnwrap: false,
								noAddCollateral: false,
								noWrap: true,
								noTransfer: false
							}

	
	expect(result).toEqual(_rules);
});