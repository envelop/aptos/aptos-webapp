
import {
	WNFT,
	_AssetType
} from "../_types";

import { getWNFTByIdFromAPI } from "../oracle";
import { getWNFTByIdFromChain } from "../chain";

export const getWNFTById = async (chainId: number, contractAddress: string, tokenId: string, userAddress?: string): Promise<WNFT | undefined> => {

	if ( contractAddress === '' ) { throw new Error('Empty contract address'); }
	if ( tokenId === ''         ) { throw new Error('Empty token id'); }

	let token = undefined;
	try { token = await getWNFTByIdFromAPI(chainId, contractAddress, tokenId, userAddress); } catch(ignored) {}

	if ( !token ) {
		try { token = await getWNFTByIdFromChain(chainId, contractAddress, tokenId, userAddress); } catch(ignored) {}
	}

	return token;
}