
import {
	getNFTById
} from ".";
import {
	CollateralItem,
	 WNFT,
	 WNFTsStat,
	 _AssetType
} from "../_types";
import {
	BigNumber,
} from "../_utils";

const _sumCollaterals = (statCollaterals: Array<CollateralItem>, tokenCollateral: Array<CollateralItem>): Array<CollateralItem> => {
	let output: Array<CollateralItem> = statCollaterals;

	tokenCollateral.forEach((item: CollateralItem) => {

		if ( item.assetType === _AssetType.empty ) { return; }

		if ( item.assetType === _AssetType.native || item.assetType === _AssetType.ERC20 ) {

			let amountToAdd = item.amount || new BigNumber(0);

			const foundAdded = output.find((iitem: CollateralItem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() });
			if ( foundAdded && foundAdded.amount ) {
				amountToAdd = amountToAdd.plus(foundAdded.amount)
			};

			output = [
				...output.filter((iitem) => { return item.contractAddress.toLowerCase() !== iitem.contractAddress.toLowerCase() }),
				{
					...item,
					amount: amountToAdd,
				}
			];
		}
		if ( item.assetType === _AssetType.ERC721 ) {

			let amountToAdd = new BigNumber(1);

			const foundAdded = output.find((iitem: CollateralItem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() });
			if ( foundAdded && foundAdded.amount ) {
				amountToAdd = amountToAdd.plus(foundAdded.amount)
			};

			output = [
				...output.filter((iitem) => { return item.contractAddress.toLowerCase() !== iitem.contractAddress.toLowerCase() }),
				{
					...item,
					amount: amountToAdd,
				}
			];
		}
		if ( item.assetType === _AssetType.ERC1155 ) {

			let amountToAdd = item.amount || new BigNumber(1);

			const foundAdded = output.find((iitem: CollateralItem) => { return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase() });
			if ( foundAdded && foundAdded.amount ) {
				amountToAdd = amountToAdd.plus(foundAdded.amount)
			};

			output = [
				...output.filter((iitem) => { return item.contractAddress.toLowerCase() !== iitem.contractAddress.toLowerCase() }),
				{
					...item,
					amount: amountToAdd,
				}
			];
		}

	});

	return output;
}
export const appendWNFTToStat = (token: WNFT, stat: WNFTsStat | undefined): WNFTsStat => {
	let statToProcess =  stat || { count: 0, collaterals: [] };
	const summedCollateral: Array<CollateralItem> = _sumCollaterals(statToProcess.collaterals, token.collateral);

	statToProcess = {
		count      : statToProcess.count + 1,
		collaterals: summedCollateral
	}

	return statToProcess;
}
export const calcWNFTsStat = (tokens: Array<WNFT>): WNFTsStat => {
	let statToProcess: WNFTsStat = { count: 0, collaterals: [] };

	for (const item of tokens) {
		statToProcess = appendWNFTToStat(item, statToProcess);
	}

	return statToProcess;
}

export const fillCollateralsData = async (token: WNFT): Promise<WNFT> => {
	const collateralUpdated = await Promise.all(token.collateral.map(async (item) => {
		if ( !item.contractAddress || item.contractAddress === '0' || item.contractAddress === '0x0000000000000000000000000000000000000000' || !item.tokenId ) { return item; }

		if (
			item.assetType === _AssetType.wNFTv0 ||
			item.assetType === _AssetType.ERC721 ||
			item.assetType === _AssetType.ERC1155
		) {
			const tokenFetched = await getNFTById(token.chainId, item.contractAddress, item.tokenId);

			return {
				...item,
				tokenImg: tokenFetched?.image || '',
				amount: tokenFetched?.amount,
			};
		}

		return item;
	}));

	return {
		...token,
		collateral: collateralUpdated,
	}
}