
import {
	NFT,
	_AssetType
} from "../_types";

import { getNFTByIdFromAPI } from "../oracle/nft";
import {
	getERC1155ByIdFromChain,
	getERC721ByIdFromChain
} from "../chain/nft";
import { processSwarmUrl } from "../_utils";

export const fetchTokenJSON = async ( _tokenUrl: string ) => {
	let description = '';
	let image = '';
	let imageRaw = '';
	let name = '';
	let tokenUrlBody = '';

	let tokenUrlRaw = _tokenUrl;
	let tokenUrl = processSwarmUrl(tokenUrlRaw);

	if ( !_tokenUrl ) {
		return undefined;
	} else {
		try {
			const res = await fetch(tokenUrl);
			tokenUrlBody = await res.text();
			const tokenUrlBodyParsed = JSON.parse(tokenUrlBody);
			if ( tokenUrlBodyParsed.image_url ) { imageRaw = tokenUrlBodyParsed.image_url; }
			if ( tokenUrlBodyParsed.image     ) { imageRaw = tokenUrlBodyParsed.image    ; }

			if ( imageRaw ) { image = processSwarmUrl(imageRaw); }
			name = tokenUrlBodyParsed.name || '';
			description = tokenUrlBodyParsed.description || '';
		} catch(e) {
			console.log('Cannot fetch tokenUrl', tokenUrl, e);
		}
	}

	return {
		description,
		image,
		imageRaw,
		name,
		tokenUrl,
		tokenUrlRaw,
		tokenUrlBody,
	}

}

export const getNFTById = async (chainId: number, contractAddress: string, tokenId: string, userAddress?: string): Promise<NFT | undefined> => {

	if ( contractAddress === '' ) { throw new Error('Empty contract address'); }
	if ( tokenId === ''         ) { throw new Error('Empty token id'); }

	let token = undefined;
	try { token = await getNFTByIdFromAPI(chainId, contractAddress, tokenId, userAddress); } catch(ignored) {}

	if ( !token ) {
		try { token = await getERC721ByIdFromChain(chainId, contractAddress, tokenId); } catch(ignored) {}
	}

	if ( !token ) {
		try { token = await getERC1155ByIdFromChain(chainId, contractAddress, tokenId, userAddress); } catch(ignored) {}
	}

	return token;
}