import {
	getERC20BalanceFromChain,
	getERC20ParamsFromChain,
	getNativeBalance,
} from '../chain';
import { getChainParamsFromAPI, getERC20ParamsFromAPI } from '../oracle';
import { Allowance, ERC20Type, _AssetType } from '../_types';

import { BigNumber, compactString } from '../_utils';

import default_icon from '../../core-pics/coins/_default.svg';
import { _getCacheItem, _setCacheItem } from '..';

export const getNullERC20 = (contractAddress: string) => {
	const assetType =
		contractAddress === '0x0000000000000000000000000000000000000000'
			? _AssetType.native
			: _AssetType.ERC20;
	return {
		contractAddress,
		assetType,
		icon: default_icon,
		decimals: 0,
		name: compactString(contractAddress),
		symbol: compactString(contractAddress),
		balance: new BigNumber(0),
		allowance: [],
	};
};

export const getERC20Params = async (
	chainId: number,
	contractAddress: string,
	userAddress?: string,
	allowanceTo?: string,
): Promise<ERC20Type> => {
	// native token fallback
	if (contractAddress === '0x0000000000000000000000000000000000000000') {
		const chainData = await getChainParamsFromAPI(chainId);
		if (!chainData) {
			return getNullERC20(contractAddress);
		}

		let nativeBalance = new BigNumber(0);
		if (userAddress) {
			nativeBalance = await getNativeBalance(chainId, userAddress);
		}

		return {
			contractAddress,
			assetType: _AssetType.native,
			icon: chainData.tokenIcon || default_icon,
			decimals: chainData.decimals,
			name: chainData.name,
			symbol: chainData.symbol,
			balance: nativeBalance,
			allowance: [],
		};
	}

	let erc20Cached: Array<ERC20Type> = _getCacheItem('erc20List') || [];
	const erc20CachedFound: ERC20Type | undefined = erc20Cached.find(
		(item: ERC20Type) => {
			return (
				contractAddress.toLowerCase() === item.contractAddress.toLowerCase()
			);
		},
	);
	if (erc20CachedFound) {
		if (!userAddress) {
			return erc20CachedFound;
		}

		const balanceFetched = await getERC20BalanceFromChain(
			chainId,
			contractAddress,
			userAddress,
			allowanceTo,
		);
		const balance = balanceFetched.balance;
		let allowance: Array<Allowance> = [];
		if (balanceFetched.allowance) {
			allowance = [
				...erc20CachedFound.allowance.filter((item: Allowance) => {
					return (
						item.allowanceTo.toLowerCase() !==
						balanceFetched.allowance?.allowanceTo.toLowerCase()
					);
				}),
				balanceFetched.allowance,
			];
		}

		let token = {
			...erc20CachedFound,
			balance,
			allowance,
		};
		erc20Cached = _getCacheItem('erc20List') || [];
		_setCacheItem('erc20List', [
			...erc20Cached.filter((item: ERC20Type) => {
				return (
					item.contractAddress.toLowerCase() !==
					token?.contractAddress.toLowerCase()
				);
			}),
			token,
		]);

		return token;
	}

	let erc20Requested = _getCacheItem('erc20Requested') || [];
	const erc20RequestedFound = erc20Requested.find((item: string) => {
		return contractAddress.toLowerCase() === item.toLowerCase();
	});
	if (erc20RequestedFound) {
		return getNullERC20(contractAddress);
	}

	_setCacheItem('erc20Requested', [...erc20Requested, contractAddress]);

	let token: ERC20Type | undefined = erc20CachedFound;
	try {
		token = await getERC20ParamsFromAPI(chainId, contractAddress);
	} catch (e) {
		throw e;
	}

	if (!token) {
		token = await getERC20ParamsFromChain(
			chainId,
			contractAddress,
			userAddress,
			allowanceTo,
		);
	}

	if (!token) {
		return getNullERC20(contractAddress);
	}

	erc20Requested = _getCacheItem('erc20Requested') || [];
	_setCacheItem(
		'erc20Requested',
		erc20Requested.filter((item: string) => {
			return item.toLowerCase() !== contractAddress.toLowerCase();
		}),
	);

	erc20Cached = _getCacheItem('erc20List') || [];
	_setCacheItem('erc20List', [
		...erc20Cached.filter((item: ERC20Type) => {
			return (
				item.contractAddress.toLowerCase() !==
				token?.contractAddress.toLowerCase()
			);
		}),
		token,
	]);

	return token;
};

export const getKnownERC20Params = (): Array<ERC20Type> => {
	return _getCacheItem('erc20List') || [];
};
