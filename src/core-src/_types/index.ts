
import {
	getNFTById
} from '../fetchtokenwrapper';
import {
	BigNumber,
	processSwarmUrl
}  from '../_utils';

export type ChainType = {
	chainId         : number,
	name            : string,
	colorCode       : string,
	RPCUrl          : string,
	symbol          : string,
	EIPPrefix       : string,
	decimals        : number,
	tokenIcon?      : string,
	networkIcon?    : string,
	isTestNetwork   : boolean,
	explorerBaseUrl : string,
	explorerName    : string,
	hasOracle       : boolean,
};

export enum _AssetType {
	wNFTv0  = -1,
	empty   =  0,
	native  =  1,
	ERC20   =  2,
	ERC721  =  3,
	ERC1155 =  4
}
export const decodeAssetTypeFromString  = (str: string): _AssetType => {
	const strCleared = str
		.toLowerCase()
		.replaceAll('-', '')
		.replaceAll(' ', '')
		.replaceAll('_', '');
	if ( strCleared.includes('1155')   ) { return _AssetType.ERC1155; }
	if ( strCleared.includes('721')    ) { return _AssetType.ERC721 ; }
	if ( strCleared.includes('20')     ) { return _AssetType.ERC20  ; }
	if ( strCleared.includes('native') ) { return _AssetType.native ; }
	if ( strCleared.includes('wNFT')   ) { return _AssetType.wNFTv0 ; }
	return _AssetType.empty;
}
export const decodeAssetTypeFromIndex  = (str: string): _AssetType => {

	if ( str === '-1' ) { return _AssetType.wNFTv0;  }
	if ( str ===  '0' ) { return _AssetType.empty;   }
	if ( str ===  '1' ) { return _AssetType.native;  }
	if ( str ===  '2' ) { return _AssetType.ERC20;   }
	if ( str ===  '3' ) { return _AssetType.ERC721;  }
	if ( str ===  '4' ) { return _AssetType.ERC1155; }

	return _AssetType.empty;
}
export const assetTypeToString  = (assetType: _AssetType, EIPPrefix: string): string => {

	if ( assetType === _AssetType.wNFTv0  ) { return 'wNFTv0'           ; }
	if ( assetType === _AssetType.empty   ) { return 'empty'            ; }
	if ( assetType === _AssetType.native  ) { return 'native'           ; }
	if ( assetType === _AssetType.ERC20   ) { return `${EIPPrefix}-20`  ; }
	if ( assetType === _AssetType.ERC721  ) { return `${EIPPrefix}-721` ; }
	if ( assetType === _AssetType.ERC1155 ) { return `${EIPPrefix}-1155`; }

	return 'empty';
}
export type _Asset = {
	assetType: _AssetType,
	contractAddress: string
}
export type _AssetItem = {
	asset: _Asset,
	tokenId: string,
	amount: string,
}
export const NFTToAssetItem = (token: NFTorWNFT): _AssetItem => {
	return {
		asset: {
			assetType: token.assetType,
			contractAddress: token.contractAddress
		},
		amount: token.amount?.toString() || '0',
		tokenId: token.tokenId,
	}
}

export type Allowance = {
	allowanceTo: string,
	amount: BigNumber
};
export interface ERC20Type extends _Asset {
	name     : string,
	symbol   : string,
	decimals : number,
	icon     : string,
	balance  : BigNumber,
	allowance: Array<Allowance>,
}
export type ERC20Balance = {
	contractAddress: string,
	balance        : BigNumber,
	allowance?     : Allowance,
}

export interface NFT extends _Asset {
	chainId      : number,
	owner?       : string,
	tokenId      : string,
	amount?      : BigNumber,
	totalSupply? : BigNumber,
	description? : string,
	image?       : string,
	imageRaw?    : string,
	name?        : string,
	tokenUrl?    : string,
	tokenUrlRaw? : string,
	tokenUrlBody?: string,
	blockNumber? : string,
	logIndex?    : string,
}
export interface WNFT extends NFT {
	originalTokenInfo?: NFT,
	collateral        : Array<CollateralItem>,
	fees              : Array<Fee>,
	royalties         : Array<Royalty>,
	locks             : Array<Lock>,
	unWrapDestination : string,
	rules             : Rules,
	collectedFees     : BigNumber,
	createTx?         : string,
}
export type NFTorWNFT = NFT & Partial<WNFT>;

export const castToWNFT = (partial: NFTorWNFT): WNFT => {
	return {
		assetType: partial.assetType                  || _AssetType.empty,
		contractAddress: partial.contractAddress      || '0x0000000000000000000000000000000000000000',
		chainId: partial.chainId                      || 1,
		owner: partial.owner,
		tokenId: partial.tokenId                      || '',
		amount: partial.amount,
		totalSupply: partial.totalSupply,
		description: partial.description,
		image: partial.image,
		imageRaw: partial.imageRaw,
		name: partial.name,
		tokenUrl: partial.tokenUrl,
		tokenUrlRaw: partial.tokenUrlRaw,
		tokenUrlBody: partial.tokenUrlBody,
		originalTokenInfo: partial.originalTokenInfo,
		collateral: partial.collateral                || [],
		fees: partial.fees                            || [],
		royalties: partial.royalties                  || [],
		locks: partial.locks                          || [],
		unWrapDestination: partial.unWrapDestination  || '',
		rules: partial.rules                          || decodeRules('0'),
		collectedFees: partial.collectedFees          || new BigNumber(0),

		blockNumber: partial.blockNumber,
		logIndex: partial.logIndex,
		createTx: partial.createTx,
	}
}

export type WNFTsStat = {
	count      : number,
	collaterals: Array<CollateralItem>,
};

// ---------- COLLATERALS ----------
export interface CollateralItem extends _Asset {
	tokenId?: string,
	amount?: BigNumber,
	tokenImg?: string,
}
export const encodeCollaterals = (collaterals: Array<CollateralItem>, transferFeeAddress?: string): Array<_AssetItem> => {
	let sortedCollaterals = collaterals;

	if ( transferFeeAddress ) {
		sortedCollaterals = sortedCollaterals.sort((item, prev) => {
			return item.contractAddress.toLowerCase() === transferFeeAddress.toLowerCase() ? -1 : 1
		})
	}

	return sortedCollaterals
		.map((item: CollateralItem): _AssetItem => {
			return {
				asset: {
					assetType      : item.assetType,
					contractAddress: item.contractAddress,
				},
				tokenId: item.tokenId || '0',
				amount: item.amount ? item.amount.toString() : '0'
			}
		})
}
export const decodeCollaterals = (collaterals: Array<_AssetItem>): Array<CollateralItem> => {
	if ( !collaterals ) { return []; }
	return collaterals.map((item: _AssetItem): CollateralItem => {

		let amountParsed = new BigNumber(item.amount);
		if ( amountParsed.isNaN() ) { amountParsed = new BigNumber(0); }

		return {
			assetType: Number(item.asset.assetType),
			contractAddress: item.asset.contractAddress,
			tokenId: item.tokenId,
			amount: amountParsed,
		}
	})
}
export const getNativeCollateral = (collaterals: Array<CollateralItem>): BigNumber => {
	let output = new BigNumber(0);
	const foundCollateral = collaterals.filter((item: CollateralItem) => {
		return !item.contractAddress || item.contractAddress === '0' || item.contractAddress === '0x0000000000000000000000000000000000000000'
	});
	if ( foundCollateral.length && foundCollateral[0].amount ) {
		output = foundCollateral[0].amount;
	}
	return output;
}
export const getNativeCollateralNumber = (collaterals: Array<CollateralItem>): number => {
	let output = new BigNumber(0);
	const foundCollateral = collaterals.filter((item: CollateralItem) => {
		return !item.contractAddress || item.contractAddress === '0' || item.contractAddress === '0x0000000000000000000000000000000000000000'
	});
	if ( foundCollateral.length && foundCollateral[0].amount ) {
		output = foundCollateral[0].amount;
	}
	return output.toNumber();
}

export const calcCollectedFees = ( collaterals: Array<CollateralItem>, fees: Array<Fee> ): BigNumber => {
	if ( !fees || !fees.length ) { return new BigNumber(0); }
	const addressToSum = fees[0].token.toLowerCase();
	const output = collaterals.reduce((prev, item) => {
		if ( item.contractAddress.toLowerCase() !== addressToSum ) { return prev; }
		if ( !item.amount ) { return prev; }
		const amount = new BigNumber(item.amount);
		if ( amount.isNaN() ) { return prev; }

		return prev.plus(amount);
	}, new BigNumber(0));

	return output;
}
// ---------- COLLATERALS ----------

// ---------- ROYALTIES ----------
export type _Royalty = {
	beneficiary: string,
	percent: string,
}
export type Royalty = {
	address   : string | undefined,
	percent   : BigNumber, // fraction: 100.00
}
export type RoyaltyInput = {
	address   : string | undefined,
	percent   : string, // fraction: 100.00
	timeAdded?: number, // needs for ui purposes
}
export const encodeRoyalties = (royalties: Array<Royalty>, wrapperContractAddress: string): Array<_Royalty> => {
	return royalties.map((item: Royalty): _Royalty => {
		return {
			beneficiary: item.address || wrapperContractAddress,
			percent    : item.percent.multipliedBy(100).toString(),
		}
	})
}
export const decodeRoyalties = (royalties: Array<_Royalty>): Array<Royalty> => {
	if ( !royalties ) { return []; }
	return royalties.map((item: _Royalty): Royalty => {
		return {
			address: item.beneficiary === '0x0000000000000000000000000000000000000000' ? undefined : item.beneficiary,
			percent: new BigNumber(item.percent).dividedBy(100),
		}
	})
}
// ---------- END ROYALTIES ----------

// ---------- LOCKS ----------
export enum LockType {
	time  = '0x00',
	value = '0x01',
	slots = '0x02',
}
export const getLockType = (str: string): LockType | undefined => {
	if ( str === LockType.time  ) { return LockType.time ; }
	if ( str === LockType.value ) { return LockType.value; }
	if ( str === LockType.slots ) { return LockType.slots; }
	return undefined;
}
export type _Lock = {
	lockType: string,
	param: string,
}
export type Lock = {
	lockType: LockType,
	param: BigNumber,
}
export const encodeLocks = (locks: Array<Lock>): Array<_Lock> => {
	return locks.map((item: Lock): _Lock => {
		return {
			lockType: item.lockType,
			param   : item.param.toString(),
		}
	})
}
export const decodeLocks = (locks: Array<_Lock>): Array<Lock> => {
	if ( !locks ) { return []; }
	return locks.map((item: _Lock): Lock => {
		const lockTypeParsed = getLockType(item.lockType) || LockType.time;

		let amountParsed = new BigNumber(item.param);
		if ( amountParsed.isNaN() ) { amountParsed = new BigNumber(0); }

		if ( lockTypeParsed === LockType.time ) {
			amountParsed = amountParsed;
		}

		return {
			lockType: lockTypeParsed,
			param   : amountParsed
		}
	})
}
// ---------- END LOCKS ----------

// ---------- FEE ----------
export type _Fee = {
	feeType: string,
	param: string,
	token: string,
}
export type Fee = {
	value: BigNumber,
	token: string,
}
export const encodeFees = (fees: Array<Fee>): Array<_Fee> => {
	return fees.map((item: Fee): _Fee => {
		return {
			feeType: '0x00',
			param: item.value.toString(),
			token: item.token,
		}
	})
}
export const decodeFees = (fees: Array<_Fee>): Array<Fee> => {
	if ( !fees ) { return []; }
	return fees.map((item: _Fee): Fee => {
		let amountParsed = new BigNumber(item.param);
		if ( amountParsed.isNaN() ) { amountParsed = new BigNumber(0); }
		return {
			token: item.token,
			value: amountParsed
		}
	})
}
// ---------- END FEE ----------

// ---------- Rules ----------
export type Rules = {
	noUnwrap: boolean,
	noAddCollateral: boolean,
	noWrap: boolean,
	noTransfer: boolean,
}
export enum RulesBitMask {
	'noUnwrap'        = 0b0001,
	'noWrap'          = 0b0010,
	'noTransfer'      = 0b0100,
	'noAddCollateral' = 0b1000,
}
// contract type: bytes2 ('0x000f')
export const encodeRules = (settings: Rules): string => {
	let output = 0;
	if ( settings.noUnwrap        ) { output |= RulesBitMask.noUnwrap;        }
	if ( settings.noWrap          ) { output |= RulesBitMask.noWrap;          }
	if ( settings.noTransfer      ) { output |= RulesBitMask.noTransfer;      }
	if ( settings.noAddCollateral ) { output |= RulesBitMask.noAddCollateral; }

	return '0x' + output.toString(16).padStart(4, '0');
}
export const decodeRules = (settings: string): Rules => {
	const numParsed = parseInt(settings, 16);

	const output = {
		noUnwrap       : false,
		noAddCollateral: false,
		noWrap         : false,
		noTransfer     : false,
	};

	if ( numParsed & RulesBitMask.noUnwrap        ) { output.noUnwrap        = true; }
	if ( numParsed & RulesBitMask.noWrap          ) { output.noWrap          = true; }
	if ( numParsed & RulesBitMask.noTransfer      ) { output.noTransfer      = true; }
	if ( numParsed & RulesBitMask.noAddCollateral ) { output.noAddCollateral = true; }

	return output;
}
// ---------- END Rules ----------

// ---------- TYPES FROM API ----------
// ---------- END TYPES FROM API ----------

export type _WNFT = {
	inAsset: _AssetItem,
	collateral: Array<_AssetItem>,
	unWrapDestinition?: string,
	unWrapDestination?: string,
	fees: Array<_Fee>,
	locks: Array<_Lock>,
	royalties: Array<_Royalty>,
	rules: string,
}
export const decodeWrappedToken = async (params: {
	inWNFT         : _WNFT,
	owner          : string | undefined,
	chainId        : number,
	contractAddress: string,
	tokenId        : string,
	assetType      : _AssetType,
	amount         : BigNumber | undefined,
	totalSupply    : BigNumber | undefined,
	tokenUrl       : string,
	blockNumber?   : string,
	logIndex?      : string,
	createTx?      : string,
}): Promise<WNFT> => {

	const parsedCollaterals   = decodeCollaterals(params.inWNFT.collateral);
	const parsedFees          = decodeFees(params.inWNFT.fees);
	const parsedCollectedFees = calcCollectedFees(parsedCollaterals, parsedFees);

	let originalTokenInfo: NFT | undefined;
	if (
		params.inWNFT.inAsset &&
		params.inWNFT.inAsset.asset.contractAddress !== '0x0000000000000000000000000000000000000000' &&
		params.inWNFT.inAsset.tokenId !== '0'
	) {
		try {
			originalTokenInfo = await getNFTById(
				params.chainId,
				params.inWNFT.inAsset.asset.contractAddress,
				params.inWNFT.inAsset.tokenId,
			);
		} catch(e: any) { console.log('Cannot fetch original token of wnft', params.inWNFT.inAsset.asset.contractAddress, params.inWNFT.inAsset.tokenId); }
	} else {
		originalTokenInfo = {
			chainId: params.chainId,
			assetType: params.inWNFT.inAsset.asset.assetType,
			contractAddress: params.inWNFT.inAsset.asset.contractAddress,
			tokenId: params.inWNFT.inAsset.tokenId,
		}
	}

	return {
		chainId        : params.chainId,
		owner          : params.owner,
		contractAddress: params.contractAddress,
		tokenId        : params.tokenId,
		assetType      : params.assetType,
		amount         : params.amount,
		totalSupply    : params.totalSupply,

		originalTokenInfo,

		tokenUrl: processSwarmUrl(params.tokenUrl),
		tokenUrlRaw: params.tokenUrl,

		collateral       : parsedCollaterals,
		fees             : parsedFees,
		collectedFees    : parsedCollectedFees,
		royalties        : decodeRoyalties(params.inWNFT.royalties),
		locks            : decodeLocks(params.inWNFT.locks),
		rules            : decodeRules(params.inWNFT.rules),
		unWrapDestination: params.inWNFT.unWrapDestination || params.inWNFT.unWrapDestinition || '',

		blockNumber: params.blockNumber,
		logIndex: params.logIndex,
		createTx: params.createTx,
	}
}

// // ---------- SAFT ----------
export type SAFTRecipientItem = {
	timeAdded: number,
	userAddress: string,
	tokenId: string,
}
// // ---------- END SAFT ----------

// ---------- SUBSCRIPTION ----------
export type SubscriptionType = {
	timelockPeriod   : BigNumber,
	ticketValidPeriod: BigNumber,
	counter          : number,
	isAvailable      : boolean,
	beneficiary      : string,
}
export type SubscriptionPayOption = {
	idx            : number,
	paymentToken   : string,
	paymentAmount  : BigNumber,
	agentFeePercent: BigNumber,
}
export type SubscriptionTariff = {
	idx         : number,
	subscription: SubscriptionType,
	payWith     : Array<SubscriptionPayOption>,
}
export type SubscriptionRemainings = {
	validUntil: BigNumber,
	countsLeft: BigNumber,
}
// ---------- END SUBSCRIPTION ----------

//#region ---------- LAUNCHPAD ----------
export type _Display = {
	id: number,
	chain_id: number,
	contract_address: string,
	name_hash: string,
	name: string,
	owner: string,
	beneficiary: string,
	enable_after: number,
	disable_after: number,
	price_model: string,
	default_prices: Array<_Price>,
	discounts: Array<_DiscountUntil>,

	title?: string,
	description?: string,
	show_widget?: boolean

	blocknumber?: number,
	logindex?: number,

	params?: Array<DisplayParam>,
}

export type _Price = {
	payWith: string;
	amount: string;
}
export type _DenominatedPrice = {
	payWith: string;
	amount: string;
	denominator: string;
}
export type Price =  {
	payWith: string;
	amount: BigNumber;
	idx: number,
}
export const encodePrice = (price: Price): _Price => {
	return {
		payWith: price.payWith,
		amount: price.amount.toString(),
	}
}
export const decodePrice = (price: _Price, idx: number): Price => {
	return {
		payWith: price.payWith,
		amount: new BigNumber(price.amount),
		idx
	}
}

export enum _DiscountType {
	PROMO     = 0,
	REFERRAL  = 1,
	BATCH     = 2,
	TIME      = 3,
	WHITELIST = 4,
	CUSTOM1   = 5,
	CUSTOM2   = 6,
	CUSTOM3   = 7,
}
export type _Discount = {
	dsctType: string;
	dsctPercent: string;
}
export type Discount = {
	dsctType: _DiscountType;
	dsctPercent: BigNumber;
}
export const decodeDiscountTypeFromIndex = (str: string | number): _DiscountType => {

	if ( `${str}` === '0'  ) { return _DiscountType.PROMO;   }
	if ( `${str}` === '1'  ) { return _DiscountType.REFERRAL;  }
	if ( `${str}` === '2'  ) { return _DiscountType.BATCH;   }
	if ( `${str}` === '3'  ) { return _DiscountType.TIME;  }
	if ( `${str}` === '4'  ) { return _DiscountType.WHITELIST; }
	if ( `${str}` === '5'  ) { return _DiscountType.CUSTOM1;   }
	if ( `${str}` === '6'  ) { return _DiscountType.CUSTOM2;  }
	if ( `${str}` === '7'  ) { return _DiscountType.CUSTOM3;   }

	return _DiscountType.PROMO;
}
export const discountTypeToString = (discountType: _DiscountType): string => {

	if ( discountType === _DiscountType.PROMO     ) { return 'PROMO'    ; }
	if ( discountType === _DiscountType.REFERRAL  ) { return 'REFERRAL' ; }
	if ( discountType === _DiscountType.BATCH     ) { return 'BATCH'    ; }
	if ( discountType === _DiscountType.TIME      ) { return 'TIME'     ; }
	if ( discountType === _DiscountType.WHITELIST ) { return 'WHITELIST'; }
	if ( discountType === _DiscountType.CUSTOM1   ) { return 'CUSTOM1'  ; }
	if ( discountType === _DiscountType.CUSTOM2   ) { return 'CUSTOM2'  ; }
	if ( discountType === _DiscountType.CUSTOM3   ) { return 'CUSTOM3'  ; }

	return 'PROMO';
}
export const encodeDiscount = (discount: Discount): _Discount => {
	return {
		dsctType: discount.dsctType.toString(),
		dsctPercent: discount.dsctPercent.multipliedBy(100).toString(),
	}
}
export const decodeDiscount = (discount: _Discount): Discount => {
	return {
		dsctType: decodeDiscountTypeFromIndex(discount.dsctType),
		dsctPercent: new BigNumber(discount.dsctPercent).dividedBy(100),
	}
}
export type _DiscountUntil = {
	untilDate: string,
	discount: _Discount
}
export type DiscountUntil = {
	untilDate: BigNumber,
	discount: Discount
}
export const encodeDiscountUntil = (discount: DiscountUntil): _DiscountUntil => {
	return {
		untilDate: discount.untilDate.toString(),
		discount: encodeDiscount(discount.discount),
	}
}
export const decodeDiscountUntil = (discount: _DiscountUntil): DiscountUntil => {
	return {
		untilDate: new BigNumber(discount.untilDate),
		discount: decodeDiscount(discount.discount),
	}
}
export type _KioskAssetItem = {
	nft: _AssetItem,
	owner: string,
	prices: Array<_Price>
}

export enum _DisplayParamType {
	basic    = 'basic',
	project  = 'project',
	links    = 'links',
	socials  = 'socials',
	progress = 'progress',
	popup    = 'popup',
	style    = 'style',
}
export type DisplayParam = {
	key_name : string,
	key_type : string,
	key_value: string,
}
//#endregion ----------LAUNCHPAD ----------

