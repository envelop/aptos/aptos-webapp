import {
	Account,
	AccountAddress,
	Aptos,
	AptosConfig,
	Ed25519PrivateKey,
	Network,
	NetworkToNetworkName,
	NetworkToChainId,
	InputViewRequestData,
	AccountAddressInput 
  } from "@aptos-labs/ts-sdk";
  import {
	NFT,

} from "../core-src/_types";

export  const isTokenOwner = async (owner: string, tokenAddress: string,  chainId:number): Promise<boolean> => {
	let aptosConfig = new AptosConfig({ network: NetworkByID(chainId) }); 
	let aptos = new Aptos(aptosConfig);
	let tokenAptosAddress = AccountAddress.fromString(tokenAddress);
	let tokenOwnershipData  = await aptos.digitalAsset.getCurrentDigitalAssetOwnership({digitalAssetAddress: tokenAptosAddress} );
	return AccountAddress.fromString(tokenOwnershipData.owner_address) === AccountAddress.fromString(owner);
}

export  const getNFTById = async (owner: string, tokenAddress: string, chainId:number): Promise<boolean> => {
	let aptosConfig = new AptosConfig({ network: NetworkByID(chainId) }); 
	let aptos = new Aptos(aptosConfig);
	let tokenAptosAddress = AccountAddress.fromString(tokenAddress);
	let tokenOwnershipData  = await aptos.digitalAsset.getCurrentDigitalAssetOwnership({digitalAssetAddress: tokenAptosAddress} );
	return AccountAddress.fromString(tokenOwnershipData.owner_address) === AccountAddress.fromString(owner);
}


export  const NetworkByID =  (chainId:number): Network => {

//	console.log('chainId',chainId);
//	console.log('NetworkToChainId',NetworkToChainId);
	
	const networkName = Object.keys(NetworkToChainId).filter((item)=>{ return NetworkToChainId[item]==chainId})[0];
//	console.log('networkName',networkName);
//	console.log('NetworkToNetworkName',NetworkToNetworkName);

	return NetworkToNetworkName[networkName];
}


export const localStorageGet = (key: string): string => {
	let output = null;
	try { output = localStorage.getItem( key ) } catch(ignored) {}
	return output || '';
}
export const localStorageSet = (key: string, value: string): void => {
	try { localStorage.setItem( key, value ) } catch(ignored) {}
}
export const localStorageRemove = (key: string): void => {
	try { localStorage.removeItem( key ) } catch(ignored) {}
}



export const isAptosAddress = (address: string): boolean => {
	try {
		  AccountAddress.fromStringStrict(address);
		return  true;
	  } catch (error: any) {
		return false;
	  }
}

export const getAptosNativeBalance = async (chainId:number, accountAddress: string): Promise<number> => {
	let aptosNetwork = NetworkByID(chainId) ;

	let aptosConfig = new AptosConfig({ network: aptosNetwork}); 
	let aptos = new Aptos(aptosConfig);

/*
	console.log('type_q',type_q);
	console.log('chainId',chainId);
	console.log('aptosNetwork',aptosNetwork);
	console.log('aptosConfig',aptosConfig);
	console.log('aptos',aptos);
*/
	const amount = await aptos.getAccountAPTAmount({
		accountAddress,
	  });
/*	  console.log('type_q',type_q);
	  console.log('chainId',chainId);
	  console.log('accountAddress',accountAddress);
	  console.log('amount',amount);
	  
	  const amount1 = await aptos.getAccountAPTAmount({
		accountAddress:'0x5d42e1a298b9f77b42ad1d28098dee4d7e672f3829d7640f1dcc801742b5f4c1'
	  });
	  console.log('amount1',amount1);
*/
	  return amount
}
	


export const getDAFromChain = async (
	tokenAddress: string,
	chainId:number,
	userAddress:string
) : Promise<{token:NFT, contractAddress:string, contractName:string}> => {
	let tokenUrl     = '';
	let tokenUrlRaw  = '';
	let tokenUrlBody = '';
	let name         = '';
	let tokenName    = '';
	let description  = '';
	let tokenDescription  = '';
	let image        = '';
	let imageRaw     = '';
	let owner        = '';
	let token;
	let tokenParsed;
	let aptosNetwork = NetworkByID(chainId) ;
	
		let aptosConfig = new AptosConfig({ network: aptosNetwork}); 
		
		let aptos = new Aptos(aptosConfig);
		let tokenAptosAddress = AccountAddress.fromString(tokenAddress);
		const ledgerInfo = await aptos.getLedgerInfo();
		let ledgerVersion = parseInt(ledgerInfo.ledger_version)-1;
	

		let tokenuserAddress = AccountAddress.fromString(userAddress);
		let getOwnedTokens =await aptos.getAccountOwnedTokens({
			accountAddress: tokenuserAddress, 
			options:{
				tokenStandard:'v2',
			},
			minimumLedgerVersion: ledgerVersion,
		});
		console.log('getOwnedTokens', getOwnedTokens);
//		let getOwnedTokens = await aptos.digitalAsset.getOwnedTokens ({ownerAddress: tokenuserAddress, minimumLedgerVersion:ledgerVersion});
//		console.log('getOwnedTokens', getOwnedTokens);
		
//		let tokenOwnershipData  = await aptos.digitalAsset.getCurrentTokenOwnership({tokenAddress: tokenAptosAddress, minimumLedgerVersion:ledgerVersion} );
		let tokenOwnershipData  = await aptos.digitalAsset.getCurrentDigitalAssetOwnership({
			digitalAssetAddress: tokenAptosAddress,
			minimumLedgerVersion:ledgerVersion
		});
			console.log('tokenOwnershipData', tokenOwnershipData);
		
		const foundToken = getOwnedTokens.filter((item) => {
			return (
				item.token_data_id.toLowerCase() === tokenAddress.toLowerCase()
			);
		});
		console.log('tokenOwnershipData foundToken', foundToken);
	
		
		owner       = (foundToken.length>0)?userAddress:'wrong owner';
//		owner       = tokenOwnershipData.owner_address
		tokenUrlRaw = tokenOwnershipData.current_token_data?.token_uri || '';
		tokenUrl    = tokenUrlRaw;
		tokenName = tokenOwnershipData.current_token_data?.token_name || '';
		tokenDescription = tokenOwnershipData.current_token_data?.description || '';
		let contractAddress = tokenOwnershipData.current_token_data?.current_collection?.collection_id || '';
		let contractName = tokenOwnershipData.current_token_data?.current_collection?.collection_name || '';
/*TODO cath not DA token	
		try {
		owner       = await contract.methods.ownerOf(tokenId).call();
		tokenUrlRaw = await contract.methods.tokenURI(tokenId).call();
		tokenUrl    = processSwarmUrl(tokenUrlRaw);
	} catch(e) {
		console.log('Cannot get erc721 token: ', e);
		throw e;
	}
*/
	try {
		token        = await fetch(tokenUrl);
		tokenUrlBody = await token.text();
		tokenParsed  = JSON.parse(tokenUrlBody);
		name         = tokenParsed.name        || tokenName;
		description  = tokenParsed.description || tokenDescription;
		if ( tokenParsed.image     ) { image = tokenParsed.image; imageRaw = tokenParsed.image    ; }
		if ( tokenParsed.image_url ) { image = tokenParsed.image_url; imageRaw = tokenParsed.image_url; }
	} catch(e) {
		console.log('Cannot fetch token data', token);
		console.log('tokenOwnershipData','Cannot fetch token data', token);
		image = tokenUrl;
		imageRaw = tokenUrl;
	}
console.log('tokenOwnershipData token', {
	token: {owner,
	chainId,
	contractAddress,
	tokenId:tokenAddress,
	tokenUrlBody,
	tokenUrl,
	tokenUrlRaw,
	name,
	description,
	image,
	imageRaw,
	assetType: -1},
	contractAddress:contractAddress, 
	contractName:contractName

});

	return {
		token: {owner,
		chainId,
		contractAddress,
		tokenId:tokenAddress,
		tokenUrlBody,
		tokenUrl,
		tokenUrlRaw,
		name,
		description,
		image,
		imageRaw,
		assetType: -1},
		contractAddress:contractAddress, 
		contractName:contractName

	}
	}
	
	
	export const mintDA = async (
		contractAddress: string,
		userAddress: string,
		chainID:number,
		signAndSubmitTransaction:Function,
	
	): Promise<any> => {
	
	
		let aptosConfig = new AptosConfig({ network: NetworkByID(chainID) }); 
		let aptos = new Aptos(aptosConfig);
		
		const response = await signAndSubmitTransaction({
			sender: userAddress,
			data: {
			  function: contractAddress+"::mint_nft",
			},
		  });

			  console.log('response', response);
		  
			  // if you want to wait for transaction
			  try {
				let transactionResponse = await aptos.waitForTransaction({ transactionHash: response.hash });
				console.log('transactionResponse', transactionResponse);
				return transactionResponse 
			  } catch (error) {
				console.error(error);
			  }
		
		
		
	
		
}