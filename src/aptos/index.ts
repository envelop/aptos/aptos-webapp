import { getDAFromChain, 
    localStorageGet, 
    localStorageSet, 
    localStorageRemove, 
    NetworkByID, 
    getAptosNativeBalance,
    isAptosAddress,
    mintDA
     } from './aptosUtils';

export { getDAFromChain, 
    localStorageGet, 
    localStorageSet, 
    localStorageRemove, 
    NetworkByID, 
    getAptosNativeBalance,
    isAptosAddress,
    mintDA
};
