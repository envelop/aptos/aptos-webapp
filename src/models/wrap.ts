import {
	BigNumber,
	CollateralItem,
	Fee,
	Lock,
	Royalty,
	Rules,
	Web3,
	_Asset,
	_AssetItem,
	_AssetType,
	_Fee,
	_Lock,
	_Royalty,
	createContract,
	decodeAssetTypeFromIndex,
	encodeCollaterals,
	encodeFees,
	encodeLocks,
	encodeRoyalties,
	encodeRules,
	getDefaultWeb3,
	getNativeCollateral,
	getUserAddress,
} from '../core-src';

import {
NetworkByID
} from '../aptos';

import {
	Account,
	AccountAddress,
	Aptos,
	AptosConfig,
	Ed25519PrivateKey,
	Network,
	NetworkToNetworkName,
	InputViewRequestData,
	AccountAddressInput ,
	UserTransactionResponse
  } from "@aptos-labs/ts-sdk";

export const getMaxCollaterals = async (
	chainId: number,
	wrapperAddress: string,
): Promise<number> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'wrapper', wrapperAddress);

	let slots = 25;
	try {
		slots = parseInt(await contract.methods.MAX_COLLATERAL_SLOTS().call());
	} catch (e: any) {
		throw e;
	}

	return slots;
};
export const getWrapperWhitelist = async (
	chainId: number,
	wrapperAddress: string,
): Promise<string | undefined> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'wrapper', wrapperAddress);
	try {
		const whitelist = await contract.methods.protocolWhiteList().call();
		if (
			!whitelist ||
			whitelist === '' ||
			whitelist === '0x0000000000000000000000000000000000000000'
		) {
			return undefined;
		}
		return whitelist;
	} catch (e: any) {
		throw e;
	}
};
export const getWhitelist = async (
	chainId: number,
	whitelistAddress: string,
): Promise<Array<_Asset>> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		const whitelist = await contract.methods.getWLAddresses().call();
		return whitelist.map(
			(item: { contractAddress: string; assetType: string }) => {
				return { ...item, assetType: decodeAssetTypeFromIndex(item.assetType) };
			},
		);
	} catch (e: any) {
		throw e;
	}
};
export const getBlacklist = async (
	chainId: number,
	wrapCotactAddress: string,
): Promise<Array<_Asset>> => {

	let aptosNetwork = NetworkByID(chainId) ;
	
	let aptosConfig = new AptosConfig({ network: aptosNetwork}); 
	
	let aptos = new Aptos(aptosConfig);

	let view_fumction = `${wrapCotactAddress}::${'env_wrapper'}::${'get_partners_token_list'}`
	let  payload = {
	function: (view_fumction as any),
	functionArguments: [],
	};

	
	try {
		let res0 = await aptos.view<[{ inner: string }]>({ payload });
			console.log('getBlacklist res0',res0);

		let res = (res0 as any)[0].data;
	//	original_token=res.;

	return (res as any).map(
		(item: {key: string,
        value: {enabled_for_collateral: boolean,
        	disabled_for_wrap: boolean
		}
		}) => {
			if (item.value.disabled_for_wrap) {
				return { contractAddress: item.key, assetType: _AssetType.ERC721 };
			}
		},
	);

		} catch (e) {
			console.log('getBlacklist error', e);

			throw e;
	}
	/*const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		const blacklist = await contract.methods.getBLAddresses().call();
		return blacklist.map(
			(item: { contractAddress: string; assetType: string }) => {
				return { ...item, assetType: decodeAssetTypeFromIndex(item.assetType) };
			},
		);
	} catch (e: any) {
		throw e;
	}
	*/
};
/*web3
export const getBlacklist = async (
	chainId: number,
	whitelistAddress: string,
): Promise<Array<_Asset>> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		const blacklist = await contract.methods.getBLAddresses().call();
		return blacklist.map(
			(item: { contractAddress: string; assetType: string }) => {
				return { ...item, assetType: decodeAssetTypeFromIndex(item.assetType) };
			},
		);
	} catch (e: any) {
		throw e;
	}
};
*/
export const isEnabledForCollateral = async (
	chainId: number,
	whitelistAddress: string,
	tokenAddress: string,
): Promise<boolean> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.enabledForCollateral(tokenAddress).call();
	} catch (e: any) {
		throw e;
	}
};
export const isEnabledForFee = async (
	chainId: number,
	whitelistAddress: string,
	tokenAddress: string,
): Promise<boolean> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.enabledForFee(tokenAddress).call();
	} catch (e: any) {
		throw e;
	}
};
export const isEnabledRemoveFromCollateral = async (
	chainId: number,
	whitelistAddress: string,
	tokenAddress: string,
): Promise<boolean> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods
			.enabledRemoveFromCollateral(tokenAddress)
			.call();
	} catch (e: any) {
		throw e;
	}
};

export type _INData = {
	inAsset: _AssetItem;
	unWrapDestinition: string; // fallback for old contracts; will be deprecated
	unWrapDestination: string;
	fees: Array<_Fee>;
	locks: Array<_Lock>;
	royalties: Array<_Royalty>;
	outType: _AssetType;
	outBalance: string;
	rules: string;
};
export type WrapTransactionArgs = {
	_inData: _INData;
	_collateral: Array<_AssetItem>;
	_wrappFor: string;
};
export const encodeINData = (params: {
	originalToken: {
		assetType: _AssetType;
		contractAddress: string;
		tokenId: string;
		amount: BigNumber;
	};
	unwrapDestination: string;
	fees: Array<Fee>;
	locks: Array<Lock>;
	royalties: Array<Royalty>;
	rules: Rules;
	outType: _AssetType;
	outBalance: number;
	wrapperContractAddress: string;
}): _INData => {
	return {
		inAsset: {
			asset: {
				assetType: params.originalToken.assetType,
				contractAddress:
					params.originalToken.contractAddress ||
					'0x0000000000000000000000000000000000000000',
			},
			tokenId: params.originalToken.tokenId || '0',
			amount: params.originalToken.amount
				? params.originalToken.amount.toString()
				: '0',
		},
		unWrapDestinition: params.unwrapDestination,
		unWrapDestination: params.unwrapDestination,
		fees: encodeFees(params.fees),
		locks: encodeLocks(params.locks),
		royalties: encodeRoyalties(params.royalties, params.wrapperContractAddress),
		outType: params.outType,
		outBalance: params.outBalance.toString(),
		rules: encodeRules(params.rules),
	};
};
export const encodeWrapArguments = (params: {
	originalToken: {
		assetType: _AssetType;
		contractAddress: string;
		tokenId: string;
		amount: BigNumber;
	};
	unwrapDestination: string;
	fees: Array<Fee>;
	locks: Array<Lock>;
	royalties: Array<Royalty>;
	rules: Rules;
	outType: _AssetType;
	outBalance: number;
	collaterals: Array<CollateralItem>;
	wrapFor: string;
	wrapperContractAddress: string;
}): WrapTransactionArgs => {
	let transferToken = undefined;
	if (params.fees.length) {
		transferToken = params.fees[0].token;
	}

	return {
		_inData: encodeINData({ ...params }),
		_collateral: encodeCollaterals(params.collaterals, transferToken),
		_wrappFor: params.wrapFor,
	};
};

export const wrapToken = async (
	web3: Web3,
	wrapperContract: string,
	params: {
		originalToken: {
			assetType: _AssetType;
			contractAddress: string;
			tokenId: string;
			amount: BigNumber;
		};
		unwrapDestination: string;
		fees: Array<Fee>;
		locks: Array<Lock>;
		royalties: Array<Royalty>;
		rules: Rules;
		outType: _AssetType;
		outBalance: number;
		collaterals: Array<CollateralItem>;
		wrapFor: string;
	},
) => {
	const userAddress = await getUserAddress(web3);
	if (!userAddress) {
		return;
	}

	const contract = await createContract(web3, 'wrapper', wrapperContract);

	const nativeCollateral = getNativeCollateral(params.collaterals);
	const encodedParams = encodeWrapArguments({
		...params,
		wrapperContractAddress: wrapperContract,
	});
	const tx = contract.methods.wrap(
		encodedParams._inData,
		encodedParams._collateral,
		encodedParams._wrappFor,
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativeCollateral });
	} catch (e) {
		throw e;
	}

	return tx.send({
		from: userAddress,
		value: nativeCollateral,
		maxPriorityFeePerGas: null,
		maxFeePerGas: null,
	});
};




export const wrapTokenMultisig = async (
	web3: Web3,
	wrapperContract: string,
	params: {
		originalToken: {
			assetType: _AssetType;
			contractAddress: string;
			tokenId: string;
			amount: BigNumber;
		};
		unwrapDestination: string;
		fees: Array<Fee>;
		locks: Array<Lock>;
		royalties: Array<Royalty>;
		rules: Rules;
		outType: _AssetType;
		outBalance: number;
		collaterals: Array<CollateralItem>;
		wrapFor: string;
	},
) => {
	const userAddress = await getUserAddress(web3);
	if (!userAddress) {
		return;
	}

	const contract = await createContract(web3, 'wrapper', wrapperContract);

	const nativeCollateral = getNativeCollateral(params.collaterals);
	const encodedParams = encodeWrapArguments({
		...params,
		wrapperContractAddress: wrapperContract,
	});
	const tx = contract.methods.wrap(
		encodedParams._inData,
		encodedParams._collateral,
		encodedParams._wrappFor,
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativeCollateral });
	} catch (e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send(
			{
				from: userAddress,
				value: nativeCollateral,
				maxPriorityFeePerGas: null,
				maxFeePerGas: null,
			},
			(err: any, data: any) => {
				if (err) {
					rej(err);
				}
				res(data);
			},
		);
	});
};

export const aptosWrapToken = async (
	wrapperContract: string,
	chainID:number,
	params: {
			tokenId: string;
			locks: number;
			nativeCollaterals: number;
	},
	signAndSubmitTransaction:Function,
	accountAddress:string
) => {
	let aptosConfig = new AptosConfig({ network: NetworkByID(chainID) }); 
	let aptos = new Aptos(aptosConfig);
/*
	const response = await signAndSubmitTransaction({
		type: "entry_function_payload",
		function: wrapperContract+"::env_wrapper::wrap",
		arguments: [params.tokenId, params.locks, params.nativeCollaterals],
		type_arguments: [],

	  });
	  */
	const response = await signAndSubmitTransaction({
		sender: accountAddress,
		data: {
		function: wrapperContract+"::env_wrapper::wrap",
		functionArguments: [params.tokenId, params.locks, params.nativeCollaterals],
		type_arguments: [],
		}
	  });
	  return response;
/*	  sender: userAddress,
	  data: {
		function: `${token.contractAddress}::env_wrapper::add_fa_collateral`,
		typeArguments: [],
		functionArguments: [token.tokenId, collateral_meta_address, value ],
	  },

*/
//	  console.log('response', response);
	  /*
	  try {
		let transactionResponse = await aptos.waitForTransaction({ transactionHash: response.hash }) as UserTransactionResponse;
//		console.log('transactionResponse', transactionResponse);
		return transactionResponse 
	  } catch (error) {
		console.error(error);
	  }*/
};

export const aptosWrapTokenWithMint = async (
	wrapperContract: string,
	chainID:number,
	params: {
			locks: number;
			nativeCollaterals: number;
	},
	signAndSubmitTransaction:Function,
	accountAddress:string
) => {
/*
	console.log('aptosWrapTokenWithMint 1');

	let aptosConfig = new AptosConfig({ network: NetworkByID(chainID) }); 
	let aptos = new Aptos(aptosConfig);
	let res_hash = '';
	console.log('aptosWrapTokenWithMint 2');
*/
	console.log('wrapperContract',wrapperContract);
	

	try {
	/*	const response = await signAndSubmitTransaction({
		type: "entry_function_payload",
		function: wrapperContract+"::env_wrapper::wrap_with_mint",
		arguments: [params.locks, params.nativeCollaterals],
		type_arguments: [],
	  });*/

	  const response = await signAndSubmitTransaction({
		sender: accountAddress,
		data: {
		function: wrapperContract+"::env_wrapper::wrap_with_mint",
		functionArguments: [params.locks, params.nativeCollaterals],
		type_arguments: [],
		},
	  });
	  console.log('response', response);
	return response;
	} catch (error) {
		console.log(error);
		return undefined;
	}
	/*
	  if (res_hash!='') {
	  try {
		let transactionResponse = await aptos.waitForTransaction({ transactionHash: res_hash }) as UserTransactionResponse;
//		console.log('transactionResponse', transactionResponse);
		return transactionResponse 
	  } catch (error) {
		console.error(error);
	  }
	}
	*/
};
