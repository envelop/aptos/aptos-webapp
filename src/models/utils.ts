import { BigNumber } from '../core-src';

export const monthsNames = [
	'jan',
	'feb',
	'mar',
	'apr',
	'may',
	'june',
	'july',
	'aug',
	'sept',
	'oct',
	'nov',
	'dec',
];

export const dateToStr = (date: Date): string => {
	return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;
};
export const dateToStrWithMonth = (date: Date): string => {
	return `${date.getDate()} ${
		monthsNames[date.getMonth()]
	} ${date.getFullYear()}`;
};
export const unixtimeToDate = (unixtime: BigNumber): Date => {
	return new Date(unixtime.multipliedBy(1000).toNumber());
};
export const dateToUnixtimeNumber = (indate: Date): number => {
	return Math.floor(indate.getTime()/1000);
};
export const dateToUnixtime = (indate: Date): BigNumber => {
	return new BigNumber(indate.getTime()).dividedToIntegerBy(1000);
};
export const addDays = (num: number): Date => {
	const output = new Date();
	output.setDate(output.getDate() + num);
	return output;
};

export const unixtimeDiffToMonths = (diff: BigNumber) => {
	const monthsRemaining = diff
		.dividedToIntegerBy(3600)
		.dividedToIntegerBy(24)
		.dividedToIntegerBy(30);
	if (monthsRemaining.gt(0)) {
		if (monthsRemaining.eq(1)) {
			return `${monthsRemaining.toFixed(0)} month`;
		}
		return `${monthsRemaining.toFixed(0)} months`;
	}

	const daysRemaining = diff.dividedToIntegerBy(3600).dividedToIntegerBy(24);
	if (daysRemaining.gt(0)) {
		if (daysRemaining.eq(1)) {
			return `${daysRemaining.toFixed(0)} day`;
		}
		return `${daysRemaining.toFixed(0)} days`;
	}

	const hoursRemaining = diff.dividedToIntegerBy(3600);
	if (hoursRemaining.gt(0)) {
		if (hoursRemaining.eq(1)) {
			return `${hoursRemaining.toFixed(0)} hour`;
		}
		return `${hoursRemaining.toFixed(0)} hours`;
	}

	const minutesRemaining = diff.dividedToIntegerBy(60);
	if (minutesRemaining.gt(0)) {
		if (minutesRemaining.eq(1)) {
			return `${minutesRemaining.toFixed(0)} minute`;
		}
		return `${minutesRemaining.toFixed(0)} minutes`;
	}

	if (diff.eq(1)) {
		return `${diff.toFixed(0)} second`;
	}
	return `${diff.toFixed(0)} seconds`;
};
export const unixtimeDiffToDays = (diff: BigNumber) => {
	const daysRemaining = diff.dividedToIntegerBy(3600).dividedToIntegerBy(24);
	if (daysRemaining.gt(0)) {
		if (daysRemaining.eq(1)) {
			return `${daysRemaining.toFixed(0)} day`;
		}
		return `${daysRemaining.toFixed(0)} days`;
	}

	const hoursRemaining = diff.dividedToIntegerBy(3600);
	if (hoursRemaining.gt(0)) {
		if (hoursRemaining.eq(1)) {
			return `${hoursRemaining.toFixed(0)} hour`;
		}
		return `${hoursRemaining.toFixed(0)} hours`;
	}

	const minutesRemaining = diff.dividedToIntegerBy(60);
	if (minutesRemaining.gt(0)) {
		if (minutesRemaining.eq(1)) {
			return `${minutesRemaining.toFixed(0)} minute`;
		}
		return `${minutesRemaining.toFixed(0)} minutes`;
	}

	if (diff.eq(1)) {
		return `${diff.toFixed(0)} second`;
	}
	return `${diff.toFixed(0)} seconds`;
};
